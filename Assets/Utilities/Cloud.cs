using System;
using UnityEngine;
using ZombieGame.Ecs;

namespace Utilities
{
    public class Cloud : MonoBehaviour
    {
        public float Velocity;

        private void Update()
        {
            if (Contexts.sharedInstance.core.gameplayTickEntity != null &&
                Contexts.sharedInstance.core.gameplayTickEntity.isTickComplete)
            {
                var position = transform.localPosition;
                position.y += Velocity;
                transform.localPosition = position;
            }
        }
    }
}
using System;
using DigitalRuby.Tween;
using TMPro;
using UnityEngine;
using ZombieGame.Ecs;

namespace Utilities
{
    public class NumberText : MonoBehaviour
    {
        private Action<ITween<float>> _moneyTween;
        private float _shownMoney = 0;

        public float TweenDuration = 0.2f;
        private TMP_Text Text;
        public string Format = "{0}";

        private void Awake()
        {
            Text = GetComponent<TMP_Text>();
            _moneyTween = t =>
            {
                Text.text = String.Format(Format, t.CurrentValue.ToGameNumberFormat());
                _shownMoney = t.CurrentValue;
            };
        }

        public void SetValue(float value)
        {
            TweenFactory.Tween("Tween" + name, _shownMoney, value, TweenDuration, TweenScaleFunctions.Linear,
                _moneyTween);
        }

        public void SetFormat(string format)
        {
            Format = format;
        }
    }
}
using UnityEngine;
using ZombieGame.Ecs;
using ZombieGame.Ecs.Commands;

namespace Utilities
{
    public class TapEmitter : MonoBehaviour, IContextUser
    {
        private Contexts _contexts;

        private void Update()
        {
            if (Input.GetMouseButtonUp(0))
            {
                GameInputActions.CreateTapDamage(_contexts,
                    Camera.main.ScreenToWorldPoint(Input.mousePosition).ToVector2());
            }
        }

        public void SupplyContext(Contexts contexts)
        {
            _contexts = contexts;
        }
    }
}
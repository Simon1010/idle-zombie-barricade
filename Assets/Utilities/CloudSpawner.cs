using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using ZombieGame.Ecs;
using Random = UnityEngine.Random;

namespace Utilities
{
    public class CloudSpawner : MonoBehaviour
    {
        private List<Cloud> _spawnedClouds;
        private Transform _lastSpawnPosition;
        public float CloudMinVerticalGap = 381;
        public int Direction = 1;
        public float SpawnTimer;
        public float SpawnTimerMax;
        public float RandomOffsetX = 15f;

        private void Awake()
        {
            _spawnedClouds = new List<Cloud>();
            SpawnTimer = Random.Range(0, SpawnTimerMax);
        }

        private void Update()
        {
            if (Contexts.sharedInstance.core.gameplayTickEntity != null &&
                Contexts.sharedInstance.core.gameplayTickEntity.isTickComplete)
            {
                SpawnTimer -= Time.deltaTime;

                if (SpawnTimer <= 0)
                {
                    if (SpawnCloud())
                    {
                        SpawnTimer = Random.Range(0, SpawnTimerMax);
                    }
                }
            }
        }

        public bool CanSpawnCloud()
        {
            if (SpawnTimer > 0)
            {
                return false;
            }


            var closestCloud = GetClosestCloudToSpawnPoint();

            if (closestCloud == null)
            {
                return true;
            }

            var distance = Mathf.Abs(closestCloud.transform.localPosition.y - GetSpawnPointY());
            return distance >= CloudMinVerticalGap;
        }

        private Cloud GetClosestCloudToSpawnPoint()
        {
            return _spawnedClouds.OrderBy(e =>
            {
                var distance = Mathf.Abs(e.transform.localPosition.y - GetSpawnPointY());
                return distance;
            }).FirstOrDefault();
        }

        private Cloud GetFarthestCloudToSpawnPoint()
        {
            return _spawnedClouds.OrderByDescending(e =>
            {
                var distance = Mathf.Abs(e.transform.localPosition.y - GetSpawnPointY());
                return distance;
            }).FirstOrDefault();
        }

        private float GetSpawnPointY()
        {
            if (Direction == 1)
            {
                return -1698;
            }

            return 1698;
        }

        public void UpdateCloudMovement(float windSpeed, float acceleration)
        {
            if (windSpeed < 0)
            {
                Direction = -1;
            }
            else
            {
                Direction = 1;
            }

            foreach (var spawnedCloud in _spawnedClouds)
            {
                var direction = Mathf.Sign(windSpeed - spawnedCloud.Velocity);

                var speedDifference = Mathf.Abs(windSpeed - spawnedCloud.Velocity);
                if (speedDifference < acceleration)
                {
                    spawnedCloud.Velocity += speedDifference * direction;
                }
                else
                {
                    spawnedCloud.Velocity += acceleration * direction;
                }
            }


            UpdateCloudRemoval();
        }

        public bool SpawnCloud()
        {
            if (CanSpawnCloud())
            {
                var cloud = GameObject.Instantiate(GameResourceManager.Instance.GetPrefab<Cloud>("Decor Cloud"),
                    transform);
                var xOffset = Random.Range(0, RandomOffsetX);
                if (Random.Range(0, 2) == 1)
                {
                    xOffset = -xOffset;
                }

                cloud.transform.localPosition = new Vector3(xOffset, GetSpawnPointY(), 0f);
                _spawnedClouds.Add(cloud);
                return true;
            }

            return false;
        }


        public void UpdateCloudRemoval()
        {
            var farthestCloud = GetFarthestCloudToSpawnPoint();

            if (farthestCloud == null)
            {
                return;
            }

            var distance = Mathf.Abs(farthestCloud.transform.localPosition.y - GetSpawnPointY());
            var maxDistance = Mathf.Abs(GetSpawnPointY()) + Mathf.Abs(GetSpawnPointY());

            if (distance >= maxDistance)
            {
                _spawnedClouds.Remove(farthestCloud);
                GameObject.Destroy(farthestCloud.gameObject);
            }
        }
    }
}
using System;
using UnityEngine;

namespace Utilities
{
    public class FadeIn : MonoBehaviour
    {
        public SpriteRenderer SpriteRenderer;
        public float FadeAcceleration = 0.2f;
        public float AlphaStart;
        public float AlphaEnd = 1f;


        private void Awake()
        {
            SpriteRenderer = GetComponentInChildren<SpriteRenderer>();
            var color = SpriteRenderer.color;
            color.a = AlphaStart;
            SpriteRenderer.color = color;
        }

        private void Update()
        {
            var color = SpriteRenderer.color;
            if (color.a == AlphaEnd)
            {
                return;
            }

            if (color.a > AlphaEnd)
            {
                color.a = AlphaEnd;
            }

            color.a += FadeAcceleration;
            SpriteRenderer.color = color;
        }
    }
}
using System;
using UnityEngine;

namespace Utilities
{
    public class CashPile : MonoBehaviour
    {
        private int _cashAmount = 0;
        private SpriteRenderer _spriteRenderer;
        public int PileSize = 250000;
        public float[] Percentages = new[] {0.25f, 0.5f, 0.75f, 1f};

        private void Awake()
        {
            _spriteRenderer = GetComponentInChildren<SpriteRenderer>();
        }


        public int CashAmount
        {
            get { return _cashAmount; }
            set
            {
                _cashAmount = value;
                UpdateSprite();
            }
        }

        private void UpdateSprite()
        {
            var percentage = (float) _cashAmount / PileSize;

            for (int i = 0; i < Percentages.Length; i++)
            {
                if (i == 0 || percentage >= Percentages[i])
                {
                    _spriteRenderer.sprite = GameResourceManager.Instance.GetSprite("decor_cash_pile_" + (i + 1));
                }
            }
        }

        public bool IsFull()
        {
            return CashAmount == PileSize;
        }

        public void SetSortingOrder(int order)
        {
            _spriteRenderer.sortingOrder = order;
        }
    }
}
using UnityEngine;
using ZombieGame.Ecs.View;
using ZombieGame.Ecs.View.Spawners;

namespace Utilities
{
    public class BloodSpawner : ViewSpawner, IListenerAnyCurrentHealth
    {
        public Vector3 BloodSpawnOffset = new Vector3(0, 25f, 0f);

        public void OnAnyCurrentHealth(CoreEntity coreEntity, float value)
        {
            var unitSpawner = GetSpawner<UnitSpawner>();

            if (unitSpawner.Has(coreEntity.coreId.value))
            {
                var go = unitSpawner.Get(coreEntity.coreId.value);

                var bloodDropsToSpawn = Random.Range(0, 5) + 2;

                for (int i = 0; i < bloodDropsToSpawn; i++)
                {
                    var blood = GameObject.Instantiate(GameResourceManager.Instance.GetPrefab("Blood Drop"), transform);
                    var dropId = Random.Range(0, 3);
                    blood.GetComponentInChildren<SpriteRenderer>().sprite =
                        GameResourceManager.Instance.GetSprite("blood_drop_00" + (dropId + 1));
                    blood.transform.localPosition =
                        go.transform.localPosition + BloodSpawnOffset;
                }
            }
        }


        protected override void OnInitalized()
        {
            Listener.AddListenerAnyCurrentHealth(this);
        }
    }
}
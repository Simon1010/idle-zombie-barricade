using TMPro;
using UnityEngine;

namespace Utilities
{
    public class DamageText : MonoBehaviour
    {
        public TMP_Text DamageTf;

        public void SetColor(Color32 color)
        {
            DamageTf.outlineColor = color;
        }
    }
}
using UnityEngine;
using ZombieGame.Ecs;
using Vector2 = System.Numerics.Vector2;

namespace Utilities
{
    public class Map : MonoBehaviour
    {
        public Transform SafehousePosition;
        public Transform BarricadePositionMin;
        public Transform BarricadePositionMax;
        public Transform CashPosition;

        public Vector2[] GetCashPositions()
        {
            var children = CashPosition.childCount;
            var positions = new Vector2[children];

            for (int i = 0; i < children; i++)
            {
                positions[i] = CashPosition.GetChild(i).position.ToVector2();
            }

            return positions;
        }
    }
}
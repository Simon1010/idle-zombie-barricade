using System;
using DigitalRuby.Tween;
using Entitas;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using ZombieGame;
using ZombieGame.Ecs.Commands;

namespace Utilities.Menu
{
    public class GameOverMenu : Menu, IListenerAnyGameOverDate, IListenerAnyGameOverTimer
    {
        public Button WatchAdButton;
        public Button SkipPremiumButton;
        public Button PlayButton;

        public TMP_Text FallDateText;
        public TMP_Text TimerTime;
        private Action<ITween<Vector3>> _tweenCam;

        public override void Setup()
        {
            WatchAdButton.onClick.AddListener(CloseMe);
            SkipPremiumButton.onClick.AddListener(CloseMe);

            Listener.AddListenerAnyGameOverDate(this);
            Listener.AddListenerAnyGameOverTimer(this);
            base.Setup(GameEnums.GameState.Gameplay, GameEnums.GameState.Gameplay);

            _tweenCam = t =>
            {
                var value = t.CurrentValue;
                value.z = -10f;
                Camera.main.transform.localPosition = value;
            };
            TweenFactory.Tween("Camera", Camera.main.transform.localPosition, new Vector3(0, 785, 0), 2.5f,
                TweenScaleFunctions.CubicEaseOut, _tweenCam);
        }

        private void CloseMe()
        {
            GameMenuManager.Instance.CloseCurrentMenu();
        }


        public void OnAnyGameOverDate(CoreEntity coreEntity, DateTime value)
        {
            FallDateText.text = String.Format("{0}/{1}/{2} {3}:{4}", value.Day, value.Month, value.Year, value.Hour,
                value.Minute);
        }

        public void OnAnyGameOverTimer(CoreEntity coreEntity, float value)
        {
            var hoursInADay = 24;

            var time = (int) value;
            var days = time / (hoursInADay * 60 * 60);
            time -= days * hoursInADay * 60 * 60;
            var hours = time / (60 * 60);
            time -= hours * (60 * 60);
            var minutes = time / 60;
            time -= minutes * 60;
            var seconds = time;


            PlayButton.interactable = value <= 0;
            if (value <= 0)
            {
                TimerTime.text = "Play";
            }
            else
            {
                var textFormat = "";

                if (days > 0)
                {
                    textFormat += "{0}";
                }

                if (hours > 0)
                {
                    if (days > 0)
                    {
                        textFormat += ":";
                    }

                    textFormat += days > 9 ? "{1}" : "0{1}"; 
                }

                if (minutes > 0)
                {
                    if (hours > 0)
                    {
                        textFormat += ":";
                    }

                    textFormat += minutes > 9 ? "{2}" : "0{2}"; 
                }

                if (seconds > 0)
                {
                    if (days == 0)
                    {
                        if (minutes > 0)
                        {
                            textFormat += ":";
                        }
                        else
                        {
                            if (hours > 0)
                            {
                                textFormat += ":";
                            }
                        }

                        textFormat += seconds > 9 ? "{3}" : "0{3}"; 
                    }
                }

                TimerTime.text = String.Format(textFormat, days, hours, minutes, seconds);
            }
        }
    }
}
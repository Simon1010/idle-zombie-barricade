using System;
using UnityEngine;
using ZombieGame;
using ZombieGame.Ecs;
using ZombieGame.Ecs.View;

namespace Utilities.Menu
{
    public class GameMenuManager : MonoBehaviour, IContextUser, IListenerAnyGameOver
    {
        private static GameMenuManager _instance;
        private Contexts _contexts;
        private Menu _currentMenu;
        private ListenerEntity _listener;

        public static GameMenuManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    var go = GameObject.Find("Game Menu Manager");

                    if (go == null)
                    {
                        go = new GameObject("Game Menu Manager");
                        go.transform.SetParent(GameObject.Find("Screen Canvas").transform);
                        go.transform.localPosition = Vector3.zero;
                        go.AddComponent<GameMenuManager>();
                    }

                    _instance = go.GetComponent<GameMenuManager>();
                }

                return _instance;
            }
        }

        public void SupplyContext(Contexts contexts)
        {
            _contexts = contexts;
            _listener = contexts.listener.CreateEntity();
            _listener.AddListenerAnyGameOver(this);
        }

        public void CreateMenu(GameEnums.MenuId menuId)
        {
            CloseCurrentMenu();

            GameObject prefab = null;

            switch (menuId)
            {
                case GameEnums.MenuId.Main:
                    break;
                case GameEnums.MenuId.Settings:
                    break;
                case GameEnums.MenuId.Upgrade:
                    prefab = GameResourceManager.Instance.GetPrefab("Upgrade Menu");
                    break;
                case GameEnums.MenuId.Trophy:
                    break;
                case GameEnums.MenuId.Perk:
                    prefab = GameResourceManager.Instance.GetPrefab("Perk Menu");
                    break;
                case GameEnums.MenuId.IdleIncome:
                    prefab = GameResourceManager.Instance.GetPrefab("Idle Income Menu");
                    break;
                case GameEnums.MenuId.GameOver:
                    prefab = GameResourceManager.Instance.GetPrefab("Game Over Menu");
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(menuId), menuId, null);
            }

            var menuGO = Instantiate(prefab, transform);
            menuGO.transform.localPosition = Vector3.zero;
            var menu = menuGO.GetComponent<Menu>();
            menu.Initialize(_contexts);
            menu.Setup();

            _currentMenu = menu;
        }

        public void CloseCurrentMenu()
        {
            _currentMenu?.DestroyMenu();
            _currentMenu = null;
        }

        public void OnAnyGameOver(CoreEntity coreEntity, bool isGameOver)
        {
            CreateMenu(GameEnums.MenuId.GameOver);
        }
    }
}
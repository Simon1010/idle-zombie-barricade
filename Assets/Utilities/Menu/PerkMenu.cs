using System.Collections.Generic;
using System.Linq;
using Entitas;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using ZombieGame;
using ZombieGame.Ecs;
using ZombieGame.Ecs.Commands;
using ZombieGame.Ecs.View;
using ZombieGame.Ecs.View.Views.UI;

namespace Utilities.Menu
{
    public class PerkMenu : Menu, IListenerAnyPerkOwned
    {
        public Transform TopPerks;
        public Transform BottomPerks;
        public Button CloseMenuBackground;

        public TMP_Text InfoTitleText;
        public TMP_Text InfoDescriptionText;
        public TMP_Text InfoCostText;

        private PerkSlotView _currentSelectedPerk;

        public override void Setup()
        {
            base.Setup(GameEnums.GameState.Menu, GameEnums.GameState.Gameplay);
            RefreshSlots();

            Listener.AddListenerAnyPerkOwned(this);
            CloseMenuBackground.onClick.AddListener(CloseMenu);
            UpdateInfoBox();
        }

        private void CloseMenu()
        {
            GameMenuManager.Instance.CloseCurrentMenu();
        }

        private List<CoreEntity> GetValidPerks()
        {
            var totalPerks = Contexts.core.GetEntities(CoreMatcher.Perk).ToList();

            var validPerks = new List<CoreEntity>();

            foreach (var coreEntity in totalPerks)
            {
                var perkData = GameData.GetPerkData(coreEntity.perkId.value, coreEntity.perkTier.value);

                if (perkData.IsLocked(Contexts) == false)
                {
                    validPerks.Add(coreEntity);
                }
            }


            return validPerks;
        }

        private void RefreshSlots()
        {
            var totalPerks = GetValidPerks();

            var topPerks = new CoreEntity[0];
            var bottomPerks = new CoreEntity[0];
            if (totalPerks.Count > 4)
            {
                topPerks = totalPerks.Take(4).ToArray();
                bottomPerks = totalPerks.GetRange(4, totalPerks.Count - 4).ToArray();
            }
            else
            {
                topPerks = totalPerks.ToArray();
            }

            var slots = CreateSlots(TopPerks, "Perk Slot", topPerks).ToList();
            slots.AddRange(CreateSlots(BottomPerks, "Perk Slot", bottomPerks));

            foreach (var slot in slots)
            {
                slot.GetComponent<PerkSlotView>().OnClick = OnClickSlot;
            }
        }


        public GameObject[] CreateSlots(Transform suppliedTransform, string prefabName, CoreEntity[] entities)
        {
            var childCount = suppliedTransform.childCount;


            for (int i = suppliedTransform.childCount - 1; i >= 0; i--)
            {
                var child = suppliedTransform.GetChild(i).GetComponent<PerkSlotView>();

                if (entities.Any(e => e.coreId.value == child.EntityId) == false)
                {
                    Destroy(suppliedTransform.GetChild(i).gameObject);
                    UnityEngine.Debug.Log("DESTROY");
                    suppliedTransform.GetChild(i).SetParent(null);
                }

                if (i >= entities.Length)
                {
                    Destroy(suppliedTransform.GetChild(i).gameObject);
                    suppliedTransform.GetChild(i).SetParent(null);
                }
            }


            bool IsSlotValid(Transform t, CoreEntity coreEntity)
            {
                var slotView = t.GetComponent<PerkSlotView>();
                return slotView.GetEntity().perkId == coreEntity.perkId &&
                       slotView.GetEntity().perkTier.value == coreEntity.perkTier.value;
            }


            for (int i = 0; i < entities.Length; i++)
            {
                if (i >= suppliedTransform.childCount ||
                    IsSlotValid(suppliedTransform.GetChild(i), entities[i]) == false)
                {
                    var go = Instantiate(GameResourceManager.Instance.GetPrefab<PerkSlotView>(prefabName),
                        suppliedTransform);
                    go.Initialize(Contexts, entities[i]);
                    go.transform.SetSiblingIndex(i);
                }
            }

            for (int i = suppliedTransform.childCount - 1; i >= 0; i--)
            {
                if (i >= entities.Length ||
                    IsSlotValid(suppliedTransform.GetChild(i), entities[i]) == false)
                {
                    suppliedTransform.GetChild(i).SetParent(null);
                    GameObject.Destroy(suppliedTransform.GetChild(i).gameObject);
                }
            }


            var children = new GameObject[entities.Length];
            for (int i = suppliedTransform.childCount - 1; i >= 0; i--)
            {
                children[i] = suppliedTransform.GetChild(i).gameObject;
            }


            return children;
        }

        public void OnClickSlot(PerkSlotView perkSlotView)
        {
            if (_currentSelectedPerk && _currentSelectedPerk != perkSlotView)
            {
                _currentSelectedPerk.SetSelected(false);
            }

            if (perkSlotView.GetEntity().isPerkOwned == false)
            {
                if (_currentSelectedPerk == perkSlotView)
                {
                    GameInputActions.CreatePurchasePerk(Contexts, _currentSelectedPerk.EntityId,
                        _currentSelectedPerk.GetPerkData().PurchaseCost);
                }
            }

            _currentSelectedPerk = perkSlotView;
            _currentSelectedPerk.SetSelected(true);
            UpdateInfoBox();
        }

        private void UpdateInfoBox()
        {
            var title = "";
            var description = "Tap on any perk icon to select it! (Tap when selected to Purchase)";
            var cost = "";
            if (_currentSelectedPerk)
            {
                var perkData = _currentSelectedPerk.GetPerkData();
                var perkTierData = _currentSelectedPerk.GetPerkTierData();

                title = perkData.Name ?? perkTierData.DefaultName;
                description = perkData.Description ?? perkTierData.DefaultDescription;

                description = string.Format(description, perkData.StatModifier);

                cost = perkData.PurchaseCost.ToGameNumberFormat();
            }

            InfoTitleText.text = title;
            InfoDescriptionText.text = description;
            InfoCostText.text = cost;
        }

        public void OnAnyPerkOwned(CoreEntity coreEntity, bool isPerkOwned)
        {
            RefreshSlots();
        }
    }
}
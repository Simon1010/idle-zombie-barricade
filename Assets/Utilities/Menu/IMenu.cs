namespace Utilities.Menu
{
    public interface IMenu
    {
        void Initialize(Contexts contexts);
        void DestroyMenu();
    }
}
using Entitas;
using UnityEngine;
using UnityEngine.UI;
using ZombieGame;

namespace Utilities.Menu
{
    public class UpgradeMenu : Menu
    {
        public Transform UpgradeSlotContainer;
        public Button CloseBackgroundButton;

        public override void Setup()
        {
            CreateSlots();
            CloseBackgroundButton.onClick.AddListener(GameMenuManager.Instance.CloseCurrentMenu);

            base.Setup(GameEnums.GameState.Menu, GameEnums.GameState.Gameplay);
        }


        private void CreateSlots()
        {
            var weapons = Contexts.core.GetEntities(CoreMatcher.Weapon);
            base.CreateSlots(UpgradeSlotContainer, "Upgrade Slot", weapons);
        }
    }
}
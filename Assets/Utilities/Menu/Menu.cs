using UnityEngine;
using ZombieGame;
using ZombieGame.Ecs.Commands;
using ZombieGame.Ecs.View;
using ZombieGame.Ecs.View.Views.UI;

namespace Utilities.Menu
{
    public abstract class Menu : MonoBehaviour, IMenu
    {
        protected Contexts Contexts;
        private GameEnums.GameState _closeState;
        private GameEnums.GameState _openState;

        private bool _initialized = false;
        protected ListenerEntity Listener;
        protected ViewEntity ViewEntity;


        public abstract void Setup();

        protected void Setup(GameEnums.GameState openState, GameEnums.GameState closeState)
        {
            _openState = openState;
            _closeState = closeState;

            GameInputActions.CreateSetGameState(Contexts, _openState);
        }

        public void Initialize(Contexts contexts)
        {
            Contexts = contexts;

            Listener = Contexts.listener.CreateEntity();
            ViewEntity = Contexts.view.CreateEntity();
            _initialized = true;
        }


        private void OnDestroy()
        {
            if (_initialized)
            {
                Listener.Destroy();
                ViewEntity.Destroy();
            }
        }

        public void DestroyMenu()
        {
            GameInputActions.CreateSetGameState(Contexts, _closeState);
            Destroy(gameObject);
        }


        public GameObject[] CreateSlots(Transform suppliedTransform, string prefabName, CoreEntity[] entities)
        {
            var childCount = suppliedTransform.childCount;

            var children = new GameObject[entities.Length];
            if (childCount == entities.Length)
            {
                for (int i = 0; i < childCount; i++)
                {
                    children[i] = suppliedTransform.GetChild(i).gameObject;
                }

                return children;
            }

            var difference = entities.Length - childCount;

            if (difference > 0)
            {
                for (int i = 0; i < difference; i++)
                {
                    GameObject.Instantiate(GameResourceManager.Instance.GetPrefab<View>(prefabName),
                        suppliedTransform);
                }
            }

            if (difference < 0)
            {
                for (int i = childCount - 1; i >= (childCount - 1) + difference; i--)
                {
                    GameObject.Destroy(suppliedTransform.GetChild(i).gameObject);
                }
            }

            for (int i = 0; i < suppliedTransform.childCount; i++)
            {
                var slot = suppliedTransform.GetChild(i).GetComponent<View>();
                slot.Initialize(Contexts, entities[i]);
                children[i] = slot.gameObject;
            }

            return children;
        }
    }
}
using System;
using Entitas;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using ZombieGame;
using ZombieGame.Ecs.Commands;

namespace Utilities.Menu
{
    public class IdleIncomeMenu : Menu, IListenerAnyIdleIncomeData
    {
        public Button CloseBackgroundButton;
        public Button CloseButton;

        public TMP_Text TimePassed;
        public NumberText IdleIncomeText;

        public override void Setup()
        {
            CloseBackgroundButton.onClick.AddListener(CloseMe);
            CloseButton.onClick.AddListener(CloseMe);

            Listener.AddListenerAnyIdleIncomeData(this);
            base.Setup(GameEnums.GameState.Menu, GameEnums.GameState.Gameplay);
        }

        private void CloseMe()
        {
            GameMenuManager.Instance.CloseCurrentMenu();
        }

        private void SetIncomeGenerated(float value)
        {
            IdleIncomeText.SetValue(value);
        }

        private void SetTimePassed(float value)
        {
            var hoursInADay = GameSettings.HoursInADay;

            var time = (int) value;
            var days = time / (hoursInADay * 60 * 60);
            time -= days * hoursInADay * 60 * 60;
            var hours = time / (60 * 60);
            time -= hours * (60 * 60);
            var minutes = time / 60;
            time -= minutes * 60;
            var seconds = time;


            var textFormat = "";

            if (days > 0)
            {
                textFormat += days > 1 ? "{0} Days" : "{0} Day";
            }

            if (hours > 0)
            {
                if (days > 0)
                {
                    textFormat += ", ";
                }

                textFormat += hours > 1 ? "{1} Hours" : "{1} Hour";
            }

            if (minutes > 0)
            {
                if (hours > 0)
                {
                    textFormat += ", ";
                }

                textFormat += minutes > 1 ? "{2} Minutes" : "{2} Minute";
            }

            if (seconds > 0)
            {
                if (days == 0)
                {
                    if (minutes > 0)
                    {
                        textFormat += ", ";
                    }
                    else
                    {
                        if (hours > 0)
                        {
                            textFormat += ", ";
                        }
                    }

                    textFormat += seconds > 1 ? "{3} Seconds" : "{3} Second";
                }
            }

            TimePassed.text = String.Format(textFormat, days, hours, minutes, seconds);
        }

        public void OnAnyIdleIncomeData(CoreEntity coreEntity, GameIdleIncomeManager.IdleStats value)
        {
            SetTimePassed(value.TimePassed);
            SetIncomeGenerated(value.IncomeProduced);
        }
    }
}
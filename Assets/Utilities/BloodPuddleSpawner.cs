using UnityEngine;
using ZombieGame.Ecs.View;
using ZombieGame.Ecs.View.Spawners;

namespace Utilities
{
    public class BloodPuddleSpawner : ViewSpawner, IListenerAnyCurrentHealth
    {
        public Vector2 BloodSpawnOffsetX = new Vector2(20f, 60f);
        public Vector2 BloodSpawnOffsetY = new Vector2(-10f, -65f);

        public void OnAnyCurrentHealth(CoreEntity coreEntity, float value)
        {
            var unitSpawner = GetSpawner<UnitSpawner>();

            if (unitSpawner.Has(coreEntity.coreId.value))
            {
                var go = unitSpawner.Get(coreEntity.coreId.value);

                var puddleChance = 0.04f;

                var puddleSpawnChances = Random.Range(0, 5);

                for (int i = 0; i < puddleSpawnChances; i++)
                {
                    var roll = Random.Range(0f, 1f);

                    if (roll > puddleChance)
                    {
                        continue;
                    }

                    var bloodPuddle = GameObject.Instantiate(GameResourceManager.Instance.GetPrefab("Blood Puddle"),
                        transform);
                    var dropId = Random.Range(0, 4);
                    bloodPuddle.GetComponentInChildren<SpriteRenderer>().sprite =
                        GameResourceManager.Instance.GetSprite("blood_puddle_00" + (dropId + 1));

                    var randomRotation = Random.Range(0, 360);
                    bloodPuddle.transform.localEulerAngles = new Vector3(0, 0, randomRotation);

                    var randomX = Random.Range(BloodSpawnOffsetX.x, BloodSpawnOffsetX.y);

                    if (Random.Range(0, 2) == 1)
                    {
                        randomX = -randomX;
                    }

                    var randomY = Random.Range(BloodSpawnOffsetY.x, BloodSpawnOffsetY.y);
                    bloodPuddle.transform.localPosition =
                        go.transform.localPosition + new Vector3(randomX, randomY);
                }
            }
        }


        protected override void OnInitalized()
        {
            Listener.AddListenerAnyCurrentHealth(this);
        }
    }
}
using System;
using UnityEngine;
using ZombieGame.Ecs;
using Random = UnityEngine.Random;

namespace Utilities
{
    public class CloudManager : MonoBehaviour
    {
        private CloudSpawner[] _cloudSpawners;
        public float WindSpeedMin = 2;
        public float WindSpeedMax = 5;
        public float WindSpeed = 0;
        public float WindAccelerationMin = 0.05f;
        public float WindAccelerationMax = 0.09f;
        public float WindAcceleration = 0;

        public float UpdateWindDirectionTimer = 0;
        public float UpdateWindDirectionTimerReset = 100;

        private void Awake()
        {
            _cloudSpawners = GetComponentsInChildren<CloudSpawner>();
        }


        private void Update()
        {
            if (Contexts.sharedInstance.core.gameplayTickEntity != null &&
                Contexts.sharedInstance.core.gameplayTickEntity.isTickComplete)
            {
                if (UpdateWindDirectionTimer <= 0)
                {
                    WindSpeed = Random.Range(WindSpeedMin, WindSpeedMax);
                    WindAcceleration = Random.Range(WindAccelerationMin, WindAccelerationMax);

                    if (Random.Range(0, 2) == 1)
                    {
                        WindSpeed = -WindSpeed;
                    }

                    UpdateWindDirectionTimer = UpdateWindDirectionTimerReset;
                }

                UpdateWindDirectionTimer -= Time.deltaTime;
                foreach (var cloudSpawner in _cloudSpawners)
                {
                    cloudSpawner.UpdateCloudMovement(WindSpeed, WindAcceleration);
                }
            }
        }


    }
}
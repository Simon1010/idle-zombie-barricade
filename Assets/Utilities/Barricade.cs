using UnityEngine;
using ZombieGame.Ecs;
using Vector2 = System.Numerics.Vector2;

namespace Utilities
{
    public class Barricade : MonoBehaviour
    {
        public Transform AllyPositionTransform;

        public Vector2[] GetAllyPositions()
        {
            var positions = new Vector2[AllyPositionTransform.childCount];

            for (int i = 0; i < positions.Length; i++)
            {
                positions[i] = AllyPositionTransform.GetChild(i).transform.localPosition.ToVector2();
            }

            return positions;
        }
    }
}
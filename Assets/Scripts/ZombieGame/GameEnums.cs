namespace ZombieGame
{
    public static class GameEnums
    {
        public enum UnitId
        {
            Civilian_Grunt_001 = 0,
            Undead_Grunt_001 = 1,
            Undead_Fatty_001 = 2,
            Undead_Slender_001 = 3,
        }

        public enum UnitState
        {
            Idle = 0,
            Attacking = 1,
            Advancing = 2,
            Dead = 3,
            Reloading = 4,
            Flee = 5,
        }

        //TODO : Assign Attack Entities to Tower/Units then Choose from Attacks available, not direct Enum Usage
        public enum AttackId
        {
            Melee_001 = 0,
            Fire_001 = 1,
        }

        public enum MapId
        {
            City_001 = 0,
        }

        public enum WeaponId
        {
            Pistol = 0,
            PistolPlus = 1,
            Shotgun = 2,
            ShotgunSawnOff = 3,
            Uzi = 4,
            AssaultRifle = 5,
            SniperRifle = 6,
            Bazooka = 7,
        }

        public enum GameState
        {
            Menu = 0,
            MainMenu = 1,
            Gameplay = 2,
        }

        public enum MenuId
        {
            Main = 0,
            Settings = 1,
            Upgrade = 2,
            Trophy = 3,
            Perk = 4,
            IdleIncome = 5,
            GameOver = 6,
        }

        public enum PerkId
        {
            ClipSize = 0,
            ReloadSpeed = 1,
            Damage = 2,
            Income = 3,
            BarricadeHealth = 4,
            TapDamage = 5,
        }

        public enum ModifierTargetEntityType
        {
            Ally = 0,
            Enemy = 1,
            Barricade = 2,
            Tick = 3,
        }

        public enum ModifierStatId
        {
            Damage = 0,
            DeathCoinReward = 1,
            Health = 2,
            ReloadTime = 3,
            EnemySpawnRate = 4,
            Ammo = 5,
        }

        public enum PerkTier
        {
            Copper = 0,
            Silver = 1,
            Gold = 2,
        }
    }
}
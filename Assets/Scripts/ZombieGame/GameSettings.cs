using UnityEngine;
using Vector2 = System.Numerics.Vector2;

namespace ZombieGame
{
    public static class GameSettings
    {
        public const float Friction = 0.92f;
        public const float Gravity = 0.98f;
        public static readonly Vector2 EnemyAttackOffset = new Vector2(-35f, 15f);
        public static readonly Vector2 AllyAttackOffset = new Vector2(35f, -15f);
        public const float BulletSpeed = 40f;
        public static float AOEDamageFallOff = 0.8f;
        public static float DefaultMultiplier = 1.07f;

        public static float EnemySpawnY = -600f;

        //60 * 60 * hours
        public static int HoursInADay = 4;
        public static int DayLengthInSeconds = 60 * 60 * HoursInADay;
    }
}
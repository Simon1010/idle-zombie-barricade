using System.Linq;
using ZombieGame.Data;
using ZombieGame.Data.Perk;

namespace ZombieGame
{
    public static class GameData
    {
        public static WeaponData[] GetAllWeaponData()
        {
            return new[]
            {
                new WeaponData
                {
                    WeaponId = GameEnums.WeaponId.Pistol,
                    WeaponUpgradeShopIcon = "gui_upgrades_icon_ pistol",
                    Damage = new Stat {Base = 5},
                    ReloadTime = new Stat {Base = 120, Multiplier = 1.00f},
                    MagazineSize = new Stat {Base = 10, Multiplier = 1.00f},
                    Range = new Stat {Base = 400f, Multiplier = 1.00f},
                    FireRate = new Stat {Base = 40f, Multiplier = 1.00f},
                    UpgradePrice = new Stat {Base = 5}
                },
                new WeaponData
                {
                    WeaponId = GameEnums.WeaponId.PistolPlus,
                    WeaponUpgradeShopIcon = "gui_upgrades_icon_ pistol+",
                    Damage = new Stat {Base = 8},
                    ReloadTime = new Stat {Base = 160, Multiplier = 1.00f},
                    MagazineSize = new Stat {Base = 13, Multiplier = 1.00f},
                    Range = new Stat {Base = 500f, Multiplier = 1.00f},
                    FireRate = new Stat {Base = 35f, Multiplier = 1.00f},
                    UpgradePrice = new Stat {Base = 150},
                },
                new WeaponData
                {
                    WeaponId = GameEnums.WeaponId.ShotgunSawnOff,
                    WeaponUpgradeShopIcon = "gui_upgrades_icon_sawedoff",
                    Damage = new Stat {Base = 3},
                    ReloadTime = new Stat {Base = 450, Multiplier = 1.00f},
                    MagazineSize = new Stat {Base = 2, Multiplier = 1.00f},
                    Range = new Stat {Base = 600f, Multiplier = 1.00f},
                    FireRate = new Stat {Base = 25, Multiplier = 1.00f},
                    UpgradePrice = new Stat {Base = 1000},
                },
                new WeaponData
                {
                    WeaponId = GameEnums.WeaponId.Shotgun,
                    WeaponUpgradeShopIcon = "gui_upgrades_icon_ shotgun",
                    Damage = new Stat {Base = 5},
                    ReloadTime = new Stat {Base = 300, Multiplier = 1.00f},
                    MagazineSize = new Stat {Base = 6, Multiplier = 1.00f},
                    Range = new Stat {Base = 450f, Multiplier = 1.00f},
                    FireRate = new Stat {Base = 80f, Multiplier = 1.00f},
                    UpgradePrice = new Stat {Base = 3000},
                },
                new WeaponData
                {
                    WeaponId = GameEnums.WeaponId.Uzi,
                    WeaponUpgradeShopIcon = "gui_upgrades_icon_ uzi",
                    Damage = new Stat {Base = 1f},
                    ReloadTime = new Stat {Base = 300, Multiplier = 0.99f},
                    MagazineSize = new Stat {Base = 40, Multiplier = 1.00f},
                    Range = new Stat {Base = 300f, Multiplier = 1.00f},
                    FireRate = new Stat {Base = 6f, Multiplier = 1f},
                    UpgradePrice = new Stat {Base = 7000},
                },
                new WeaponData
                {
                    WeaponId = GameEnums.WeaponId.AssaultRifle,
                    WeaponUpgradeShopIcon = "gui_upgrades_icon_a4",
                    Damage = new Stat {Base = 4},
                    ReloadTime = new Stat {Base = 300, Multiplier = 1.00f},
                    MagazineSize = new Stat {Base = 30, Multiplier = 1.00f},
                    Range = new Stat {Base = 850f, Multiplier = 1.00f},
                    FireRate = new Stat {Base = 25f, Multiplier = 1.00f},
                    UpgradePrice = new Stat {Base = 25000},
                },
                new WeaponData
                {
                    WeaponId = GameEnums.WeaponId.SniperRifle,
                    WeaponUpgradeShopIcon = "gui_upgrades_icon_m400",
                    Damage = new Stat {Base = 25},
                    ReloadTime = new Stat {Base = 500, Multiplier = 1.00f},
                    MagazineSize = new Stat {Base = 5, Multiplier = 1.00f},
                    Range = new Stat {Base = 1040f, Multiplier = 1.00f},
                    FireRate = new Stat {Base = 100f, Multiplier = 1.00f},
                    UpgradePrice = new Stat {Base = 100000},
                },
                new BazookaData
                {
                    WeaponId = GameEnums.WeaponId.Bazooka,
                    WeaponUpgradeShopIcon = "gui_upgrades_icon_bazooka",
                    Damage = new Stat {Base = 70},
                    ReloadTime = new Stat {Base = 800, Multiplier = 1.00f},
                    MagazineSize = new Stat {Base = 1, Multiplier = 1.00f},
                    Range = new Stat {Base = 800f, Multiplier = 1.00f},
                    FireRate = new Stat {Base = 350f, Multiplier = 1.00f},
                    ExplosionRange = new Stat {Base = 350},
                    UpgradePrice = new Stat {Base = 500000},
                }
            };
        }

        public static WeaponData GetWeaponData(GameEnums.WeaponId weaponId)
        {
            return GetAllWeaponData().SingleOrDefault(e => e.WeaponId == weaponId);
        }

        public static T GetWeaponData<T>(GameEnums.WeaponId weaponId) where T : WeaponData
        {
            return (T) GetWeaponData(weaponId);
        }

        public static AttackData[] GetAllAttackData()
        {
            return new[]
            {
                new AttackData()
                {
                    attackId = GameEnums.AttackId.Melee_001,
                    Events = new[]
                    {
                        new AttackEventData()
                        {
                            AttackId = GameEnums.AttackId.Melee_001,
                            Frame = 9
                        },
                    },
                    AnimationTime = 20,
                    AttackCooldown = 180,
                },
                new AttackData()
                {
                    attackId = GameEnums.AttackId.Fire_001,
                    Events = new[]
                    {
                        new AttackEventData()
                        {
                            AttackId = GameEnums.AttackId.Fire_001,
                            Frame = 1
                        },
                    },
                    AnimationTime = 1,
                    AttackCooldown = 0,
                },
            };
        }

        public static AttackData GetAttackData(GameEnums.AttackId attackId)
        {
            return GetAllAttackData().SingleOrDefault(e => e.attackId == attackId);
        }

        public static UnitData[] GetAllUnitData()
        {
            return new[]
            {
                new UnitData
                {
                    UnitId = GameEnums.UnitId.Civilian_Grunt_001,
                },
            };
        }


        public static UnitData GetUnitData(GameEnums.UnitId unitId)
        {
            return GetAllUnitData().SingleOrDefault(e => e.UnitId == unitId);
        }


        public static EnemyData[] GetAllEnemyData()
        {
            return new[]
            {
                new EnemyData()
                {
                    UnitId = GameEnums.UnitId.Undead_Grunt_001,
                    Health = new Stat {Base = 70, Multiplier = 1.07f},
                    Damage = new Stat {Base = 3, Multiplier = 1.07f},
                    Speed = new Stat {Base = 3f, Multiplier = 1.07f},
                    KillReward = new Stat {Base = 5, Multiplier = 1.07f},
                },
                new EnemyData()
                {
                    UnitId = GameEnums.UnitId.Undead_Fatty_001,
                    Health = new Stat {Base = 300, Multiplier = 1.07f},
                    Damage = new Stat {Base = 10, Multiplier = 1.07f},
                    Speed = new Stat {Base = 0.4f, Multiplier = 1.07f},
                    KillReward = new Stat {Base = 15, Multiplier = 1.07f},
                    DeathAOERange = new Stat {Base = 250}
                },
                new EnemyData()
                {
                    UnitId = GameEnums.UnitId.Undead_Slender_001,
                    Health = new Stat {Base = 30, Multiplier = 1.07f},
                    Damage = new Stat {Base = 2, Multiplier = 1.07f},
                    Speed = new Stat {Base = 2f, Multiplier = 1.07f},
                    KillReward = new Stat {Base = 5, Multiplier = 1.07f},
                }
            };
        }


        public static EnemyData GetEnemyData(GameEnums.UnitId unitId)
        {
            return GetAllEnemyData().SingleOrDefault(e => e.UnitId == unitId);
        }


        public static MapData GetMapData(GameEnums.MapId mapId)
        {
            return GetAllMapData().SingleOrDefault(e => e.MapId == mapId);
        }

        public static MapData[] GetAllMapData()
        {
            return new[]
            {
                new MapData
                {
                    PrefabName = "Cowboy_Map_001",
                },
            };
        }

        public static PlayerData GetPlayerData()
        {
            return new PlayerData
            {
                TapDamage = new Stat {Base = 1},
                TapRange = new Stat {Base = 350}
            };
        }


        public static PerkTierData GetPerkTierData(GameEnums.PerkId perkId)
        {
            return GetAllPerkTierData().SingleOrDefault(e => e.PerkId == perkId);
        }

        public static PerkData GetPerkData(GameEnums.PerkId perkId, GameEnums.PerkTier perkTier)
        {
            return GetAllPerkTierData().SingleOrDefault(e => e.PerkId == perkId)
                ?.PerkData
                .SingleOrDefault(e => e.PerkTier == perkTier);
        }

        public static PerkTierData[] GetAllPerkTierData()
        {
            return new[]
            {
                new PerkTierData()
                {
                    PerkId = GameEnums.PerkId.Damage,
                    DefaultName = "Damage",
                    DefaultDescription = "Increases all weapon damage by {0}%",
                    DefaultPerkIconName = "gui_perk_damage_1",
                    PerkData = new[]
                    {
                        new PerkData
                        {
                            PerkTier = GameEnums.PerkTier.Copper,
                            PerkIconName = "gui_perk_damage_1",
                            Requirements = new IRequirement[] {new RequirementKills(1),},
                            StatModifier = 1.9f,
                            ModifierTargetEntityType = GameEnums.ModifierTargetEntityType.Ally,
                            ModifierStatId = GameEnums.ModifierStatId.Damage,
                            PurchaseCost = 100,
                        },
                    }
                },
                new PerkTierData()
                {
                    PerkId = GameEnums.PerkId.ClipSize,
                    DefaultName = "Clip Size",
                    DefaultDescription = "Increase clip size by {0}%",
                    DefaultPerkIconName = "gui_perk_clip_1",
                    PerkData = new[]
                    {
                        new PerkData
                        {
                            PerkTier = GameEnums.PerkTier.Copper,
                            PerkIconName = "gui_perk_clip_1",
                            Requirements = new IRequirement[] {new RequirementKills(2),},
                            StatModifier = 1.05f,
                            ModifierTargetEntityType = GameEnums.ModifierTargetEntityType.Ally,
                            ModifierStatId = GameEnums.ModifierStatId.Ammo,
                            PurchaseCost = 500,
                        },
                        new PerkData
                        {
                            PerkTier = GameEnums.PerkTier.Silver,
                            PerkIconName = "gui_perk_clip_2",
                            Requirements = new IRequirement[]
                            {
                                new RequirementKills(5),
                                new RequirementPreviousPerk(GameEnums.PerkId.ClipSize, GameEnums.PerkTier.Copper)
                            },
                            StatModifier = 1.2f,
                            ModifierTargetEntityType = GameEnums.ModifierTargetEntityType.Ally,
                            ModifierStatId = GameEnums.ModifierStatId.Ammo,
                            PurchaseCost = 1500,
                        },
                        new PerkData
                        {
                            PerkTier = GameEnums.PerkTier.Gold,
                            PerkIconName = "gui_perk_clip_3",
                            Requirements = new IRequirement[]
                            {
                                new RequirementPreviousPerk(GameEnums.PerkId.ClipSize, GameEnums.PerkTier.Silver)
                            },
                            StatModifier = 1.5f,
                            ModifierTargetEntityType = GameEnums.ModifierTargetEntityType.Ally,
                            ModifierStatId = GameEnums.ModifierStatId.Ammo,
                            PurchaseCost = 3000,
                        },
                    }
                },
                new PerkTierData()
                {
                    PerkId = GameEnums.PerkId.BarricadeHealth,
                    DefaultName = "Barricade Health",
                    DefaultDescription = "Increases total barricade health by {0}%",
                    DefaultPerkIconName = "gui_perk_barricade_hp_1",
                    PerkData = new[]
                    {
                        new PerkData
                        {
                            PerkTier = GameEnums.PerkTier.Copper,
                            PerkIconName = "gui_perk_barricade_hp_1",
                            Requirements = new IRequirement[] {new RequirementKills(5),},
                            StatModifier = 1.2f,
                            ModifierTargetEntityType = GameEnums.ModifierTargetEntityType.Barricade,
                            ModifierStatId = GameEnums.ModifierStatId.Health,
                        },
                        new PerkData
                        {
                            PerkTier = GameEnums.PerkTier.Silver,
                            PerkIconName = "gui_perk_barricade_hp_1",
                            Requirements = new IRequirement[]
                            {
                                new RequirementKills(5),
                                new RequirementWeaponLevel(GameEnums.WeaponId.Pistol, 2),
                            },
                            StatModifier = 1.4f,
                            ModifierTargetEntityType = GameEnums.ModifierTargetEntityType.Barricade,
                            ModifierStatId = GameEnums.ModifierStatId.Health,
                        },
                    }
                },
            };
        }

        public static EnemyData[] GetAllEnemyDataAvailable(int currentDay)
        {
            //TODO : Enemies based on day
            return GetAllEnemyData();
        }

        public static int GetEnemySpawnRate(int currentDay)
        {
            //TODO : Spawn Rate based on day
            return 3;
        }
    }
}
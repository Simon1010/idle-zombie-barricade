using System;
using System.Collections.Generic;
using System.Linq;
using Entitas;
using UnityEngine;

namespace ZombieGame
{
    public static class GameIdleIncomeManager
    {
        public static IdleStats GetIdleStats(Contexts contexts, DateTime lastLoginDate)
        {
            var secondsPassed = (float) (DateTime.Now - lastLoginDate).TotalSeconds;
            var currentSecondsPassed = secondsPassed;

            var currentDay = contexts.core.gameEntity.gameCurrentDay.value;

            var daysPassed = (float) contexts.core.gameEntity.gameTimePassed.value / GameSettings.DayLengthInSeconds;
            var timePercentageLeft = (float) contexts.core.gameEntity.gameTimePassed.value /
                                     GameSettings.DayLengthInSeconds;

            if (daysPassed > 0)
            {
                timePercentageLeft %= daysPassed;
            }

            timePercentageLeft = 1 - timePercentageLeft;

            var timeLeftInCurrentDay = timePercentageLeft * GameSettings.DayLengthInSeconds;

            var timeSpentInCurrentDay =
                (int) Math.Min(currentSecondsPassed, timeLeftInCurrentDay);

            var dayStats = new List<IdleStats>();
            dayStats.Add(GetDayStats(contexts, currentDay, timeSpentInCurrentDay));
            currentSecondsPassed -= timeSpentInCurrentDay;
            currentDay++;

            if (currentSecondsPassed < 0)
            {
                currentSecondsPassed = 0;
            }

            daysPassed = (int) (currentSecondsPassed / GameSettings.DayLengthInSeconds);

            for (int i = 0; i < daysPassed; i++)
            {
                var timeSpentInDay = Math.Min(currentSecondsPassed / GameSettings.DayLengthInSeconds, 1) *
                                     GameSettings.DayLengthInSeconds;
                dayStats.Add(GetDayStats(contexts, currentDay + i, timeSpentInDay));
                currentSecondsPassed -= timeSpentInDay;
            }

            var damageTallied = dayStats.Sum(e => e.DamageTaken);
            var incomeTallied = dayStats.Sum(e => e.IncomeProduced);
            var enemiesKilled = dayStats.Sum(e => e.EnemiesKilled);

            return new IdleStats
            {
                DamageTaken = damageTallied,
                IncomeProduced = incomeTallied,
                EnemiesKilled = enemiesKilled,
                TimePassed = (int) secondsPassed,
                DaysPassed = (int) daysPassed,
            };
        }

        public class IdleStats
        {
            public int DamageTaken;
            public int EnemiesKilled;
            public int IncomeProduced;
            public int TimePassed;
            public int DaysPassed;
        }

        public static IdleStats GetDayStats(Contexts contexts, int day, float secondsSpentInDay)
        {
            var enemyDataAvailable = GameData.GetAllEnemyDataAvailable(day);
            var weaponsAvailable = contexts.core.GetEntities(CoreMatcher.Weapon).Where(e => e.level.value > 0);

            var averageAttackDamage = (int) enemyDataAvailable.Average(e => GameCalculations.CalculateInt(e.Damage, 1));
            var averageAttackHealth = (int) enemyDataAvailable.Average(e => GameCalculations.CalculateInt(e.Health, 1));
            var averageAttackIncome =
                (int) enemyDataAvailable.Average(e => GameCalculations.CalculateInt(e.KillReward, 1));
            var enemiesSpawnedWithinTime = secondsSpentInDay / GameData.GetEnemySpawnRate(day);


            var defenseDamage = weaponsAvailable.Sum(e =>
            {
                var weaponData = GameData.GetWeaponData(e.weaponId.value);
                return GameCalculations.Calculate(weaponData.Damage, e.level.value);
            });

            var defenseFireRate = weaponsAvailable.Sum(e =>
            {
                var weaponData = GameData.GetWeaponData(e.weaponId.value);
                return GameCalculations.Calculate(weaponData.FireRate, e.level.value);
            });

            var defenseAverageDamage = defenseDamage * defenseFireRate * secondsSpentInDay;

            var attackHealth = averageAttackHealth * enemiesSpawnedWithinTime;
            var enemiesKilled = (int) (Math.Min(defenseAverageDamage / attackHealth, 1) * enemiesSpawnedWithinTime);
            var enemiesSurvived = enemiesSpawnedWithinTime - enemiesKilled;
            var damageTaken = averageAttackDamage * enemiesSurvived;
            var incomeAccumulated = enemiesKilled * averageAttackIncome;

            var dayStats = new IdleStats()
            {
                DamageTaken = (int) damageTaken,
                IncomeProduced = incomeAccumulated,
                EnemiesKilled = enemiesKilled
            };

            return dayStats;
        }
    }
}
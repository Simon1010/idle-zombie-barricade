using System.Numerics;

namespace ZombieGame
{
    public static class GameAiHelpers
    {
        public static bool Move(CoreEntity coreEntity, Vector2 position, float threshold = 5f)
        {
            var distance = Vector2.Distance(coreEntity.position.value, position);
            if (distance > threshold)
            {
                if (coreEntity.hasMoveTo == false)
                {
                    coreEntity.ReplaceMoveToArriveDistance(threshold);
                    coreEntity.ReplaceMoveTo(position);
                }

                return false;
            }

            return true;
        }

        public static bool IsAt(CoreEntity coreEntity, Vector2 position)
        {
            var distance = Vector2.Distance(coreEntity.position.value, position);
            return distance < 5f;
        }

        public static void CancelPath(CoreEntity coreEntity)
        {
            if (coreEntity.hasMovementPath)
                coreEntity.RemoveMovementPath();
            if (coreEntity.hasMoveTo)
                coreEntity.RemoveMoveTo();
        }

        public static void StartPath(CoreEntity coreEntity, Vector2[] pathNodes, float threshold = 5f)
        {
            coreEntity.ReplaceMoveToArriveDistance(threshold);
            coreEntity.AddMovementPath(pathNodes);
        }

        public static void DoAttack(CoreEntity coreEntity, GameEnums.AttackId attackId)
        {
            var timeSinceLastAttack = coreEntity.hasTimeSinceLastAttack ? coreEntity.timeSinceLastAttack.value : -1;

            //TODO : Add different attacks
            var isAttackCooldownOver =
                timeSinceLastAttack >= GameData.GetAttackData(attackId).AttackCooldown;

            if (timeSinceLastAttack == -1 || isAttackCooldownOver)
            {
                if (coreEntity.hasAttackId == false)
                {
                    var attackData = GameData.GetAttackData(attackId);
                    coreEntity.ReplaceAttackId(attackId);
                    coreEntity.ReplaceAttackCurrentTime(0);
                    coreEntity.ReplaceAttackTotalTime(attackData.AnimationTime);
                    if (coreEntity.hasTimeSinceLastAttack)
                    {
                        coreEntity.RemoveTimeSinceLastAttack();
                    }
                }
            }
        }

        public static void EndAttack(CoreEntity coreEntity)
        {
            if (coreEntity.hasAttackId)
            {
                coreEntity.RemoveAttackId();
                coreEntity.RemoveAttackCurrentTime();
                coreEntity.RemoveAttackTotalTime();
            }
        }

        public static void StopMoving(CoreEntity coreEntity)
        {
            if (coreEntity.hasMoveTo)
            {
                coreEntity.RemoveMoveTo();
            }
        }

        public static bool IsMoving(CoreEntity coreEntity)
        {
            return coreEntity.hasMoveTo;
        }
    }
}
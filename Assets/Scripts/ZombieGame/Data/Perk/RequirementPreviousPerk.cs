using System.Linq;

namespace ZombieGame.Data.Perk
{
    public class RequirementPreviousPerk : IRequirement
    {
        private readonly GameEnums.PerkId _perkId;
        private readonly GameEnums.PerkTier _tier;

        public RequirementPreviousPerk(GameEnums.PerkId perkId, GameEnums.PerkTier tier)
        {
            _perkId = perkId;
            _tier = tier;
        }

        public bool IsValid(Contexts contexts)
        {
            return contexts.core.GetEntitiesWithPerkId(_perkId).Any(e => e.perkTier.value == _tier && e.isPerkOwned);
        }
    }
}
using System.Linq;

namespace ZombieGame.Data.Perk
{
    public class RequirementWeaponLevel : IRequirement
    {
        private readonly GameEnums.WeaponId _weaponId;
        private readonly int _weaponLevel;

        public RequirementWeaponLevel(GameEnums.WeaponId weaponId, int weaponLevel)
        {
            _weaponId = weaponId;
            _weaponLevel = weaponLevel;
        }

        public bool IsValid(Contexts contexts)
        {
            var weapon = contexts.core.GetEntityWithWeaponId(_weaponId);
            return weapon.level.value >= _weaponLevel;
        }
    }
}
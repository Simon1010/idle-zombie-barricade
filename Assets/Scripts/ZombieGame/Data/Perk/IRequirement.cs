namespace ZombieGame.Data.Perk
{
    public interface IRequirement
    {
        bool IsValid(Contexts contexts);
    }
}
namespace ZombieGame.Data.Perk
{
    public class RequirementKills : IRequirement
    {
        private readonly int _totalKills;

        public RequirementKills(int totalKills)
        {
            _totalKills = totalKills;
        }

        public bool IsValid(Contexts contexts)
        {
            return contexts.core.gameEntity.gameTotalKills.value >= _totalKills;
        }
    }
}
using System;

namespace ZombieGame.Data
{
    [Serializable]
    public class AttackEventData
    {
        public int Frame;
        public GameEnums.AttackId AttackId;
    }
}
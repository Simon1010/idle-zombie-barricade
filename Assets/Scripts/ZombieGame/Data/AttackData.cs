using System;
using System.Linq;

namespace ZombieGame.Data
{
    [Serializable]
    public class AttackData
    {
        public GameEnums.AttackId attackId;
        public AttackEventData[] Events;
        public int AnimationTime;
        public int AttackCooldown = 120;

        public bool IsFrame(int value)
        {
            return Events.Any(e => e.Frame == value);
        }
    }
}
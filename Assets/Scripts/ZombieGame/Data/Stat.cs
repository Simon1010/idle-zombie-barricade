namespace ZombieGame.Data
{
    public class Stat
    {
        public float Base;
        public float Multiplier = GameSettings.DefaultMultiplier;
    }
}
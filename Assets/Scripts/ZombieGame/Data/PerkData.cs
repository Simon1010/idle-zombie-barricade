using ZombieGame.Data.Perk;

namespace ZombieGame.Data
{
    public class PerkData
    {
        public string Name = null;
        public string Description;
        public string PerkIconName;

        public IRequirement[] Requirements;
        public GameEnums.PerkTier PerkTier;

        public float StatModifier;
        public GameEnums.ModifierTargetEntityType ModifierTargetEntityType;
        public GameEnums.ModifierStatId ModifierStatId;
        public int PurchaseCost;
    }

    public class PerkTierData
    {
        public GameEnums.PerkId PerkId;
        public PerkData[] PerkData;
        public string DefaultName;
        public string DefaultDescription;
        public string DefaultPerkIconName;
    }
}
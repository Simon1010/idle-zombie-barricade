namespace ZombieGame.Data
{
    public class EnemyData : UnitData
    {
        public Stat Health;
        public Stat Speed;
        public Stat Damage;
        public Stat KillReward;
        public Stat DeathAOERange;
    }
}
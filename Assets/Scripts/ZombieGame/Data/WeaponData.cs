namespace ZombieGame.Data
{
    public class WeaponData
    {
        public GameEnums.WeaponId WeaponId;
        public Stat Damage;
        public Stat ReloadTime;
        public Stat MagazineSize;
        public Stat Range;
        public Stat FireRate;
        public Stat UpgradePrice { get; set; }
        public string WeaponUpgradeShopIcon;
    }
}
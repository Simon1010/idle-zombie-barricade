using System;

namespace ZombieGame.Data
{
    [Serializable]
    public class UnitData
    {
        public GameEnums.UnitId UnitId;
    }
}
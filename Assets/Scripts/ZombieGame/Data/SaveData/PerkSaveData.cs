using System;

namespace ZombieGame.Data.SaveData
{
    [Serializable]
    public class PerkSaveData
    {
        public GameEnums.PerkId PerkId;
        public bool IsPerkOwned = true;
        public GameEnums.PerkTier PerkTier;
    }
}
using System;

namespace ZombieGame.Data.SaveData
{
    [Serializable]
    public class GameSaveData
    {
        public DateTime GameOverDate;
        public float GameOverTimeLeft = 0f;
        public bool IsGameOver = false;
        public float BarricadeHealth = 0;
        public DateTime LastLoginDate;
        public long LastLoginGameTime = 0;
        public int TotalKills = 0;
        public int LastLoginGameDay = 0;
        public int PlayerMoney = 0;
        public PerkSaveData[] PerkSaveDatas = null;
        public WeaponSaveData[] WeaponSaveDatas = null;
    }
}
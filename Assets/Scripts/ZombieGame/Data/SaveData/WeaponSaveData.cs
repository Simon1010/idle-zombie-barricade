using System;

namespace ZombieGame.Data.SaveData
{
    [Serializable]
    public class WeaponSaveData
    {
        public GameEnums.WeaponId WeaponId;
        public int Level;
    }
}
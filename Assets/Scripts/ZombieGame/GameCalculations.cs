using System;
using System.Linq;
using Entitas;
using ZombieGame.Data;

namespace ZombieGame
{
    public static class GameCalculations
    {
        public static float Calculate(Stat stat, CoreEntity coreEntity)
        {
            var level = coreEntity.level.value;
            return Calculate(stat, level);
        }

        public static float Calculate(Stat stat, int level)
        {
            return stat.Base * (float) Math.Pow(stat.Multiplier, level - 1);
        }

        public static float CalculateInt(Stat stat, int level)
        {
            return (int) Calculate(stat, level);
        }

        public static int CalculateInt(Stat stat, CoreEntity coreEntity)
        {
            return (int) Calculate(stat, coreEntity);
        }

        public static float ApplyModifiers(Contexts contexts, float value,
            GameEnums.ModifierTargetEntityType entityType,
            GameEnums.ModifierStatId statId)
        {
            var modifiers = contexts.core.GetEntities(CoreMatcher.Modifier).Where(e =>
                e.modifierTargetEntityType.value == entityType &&
                e.modifierStatId.value == statId);

            foreach (var modifier in modifiers)
            {
                value *= modifier.modifierValue.value;
            }

            return value;
        }
    }
}
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using Entitas;
using UnityEngine;
using ZombieGame.Data;
using ZombieGame.Data.SaveData;

namespace ZombieGame
{
    public static class GameSaveManager
    {
        public static GameSaveData CreateSaveGame(Contexts contexts)
        {
            var saveData = new GameSaveData();
            //Save Perks
            var perks = contexts.core.GetEntities(CoreMatcher.PerkOwned);
            var perkSaveDatas = new List<PerkSaveData>();
            foreach (var perk in perks)
            {
                var perkSaveData = new PerkSaveData
                {
                    PerkId = perk.perkId.value,
                    IsPerkOwned = perk.isPerkOwned,
                    PerkTier = perk.perkTier.value
                };

                perkSaveDatas.Add(perkSaveData);
            }

            saveData.PerkSaveDatas = perkSaveDatas.ToArray();

            //Save Allies
            var weapons = contexts.core.GetEntities(CoreMatcher.Weapon);
            var weaponSaveDatas = new List<WeaponSaveData>();
            foreach (var coreEntity in weapons)
            {
                var weaponSaveData = new WeaponSaveData()
                {
                    Level = coreEntity.level.value,
                    WeaponId = coreEntity.weaponId.value
                };

                weaponSaveDatas.Add(weaponSaveData);
            }

            saveData.WeaponSaveDatas = weaponSaveDatas.ToArray();

            //Save Day
            //Save Trophies
            //Save Settings

            saveData.TotalKills = contexts.core.gameEntity.gameTotalKills.value;
            saveData.LastLoginGameTime = contexts.core.gameEntity.gameTimePassed.value;
            saveData.LastLoginDate = DateTime.Now;
            saveData.LastLoginGameDay = contexts.core.gameEntity.gameCurrentDay.value;
            saveData.PlayerMoney = contexts.core.playerEntity.playerMoney.value;
            saveData.BarricadeHealth = contexts.core.barricadeEntity.currentHealth.value;
            saveData.IsGameOver = contexts.core.gameEntity.isGameOver;
            if (saveData.IsGameOver)
            {
                saveData.GameOverTimeLeft = contexts.core.gameEntity.gameOverTimer.value;
                saveData.GameOverDate = contexts.core.gameEntity.gameOverDate.value;
            }

            return saveData;
        }


        public static void SaveGame(Contexts contexts)
        {
            var saveGame = CreateSaveGame(contexts);
            var bf = new BinaryFormatter();
            var fs = File.Open(Application.persistentDataPath + "/GameSave.dat", FileMode.OpenOrCreate);
            bf.Serialize(fs, saveGame);
            fs.Close();
        }

        public static GameSaveData GetSaveData(Contexts contexts)
        {
            if (File.Exists(Application.persistentDataPath + "/GameSave.dat"))
            {
                var fs = File.Open(Application.persistentDataPath + "/GameSave.dat", FileMode.Open);
                var bf = new BinaryFormatter();
                var saveData = (GameSaveData) bf.Deserialize(fs);
                fs.Close();
                return saveData;
            }

            return null;
        }

        public static void LoadGame(Contexts contexts, GameSaveData gameSave)
        {
            foreach (var perkSaveData in gameSave.PerkSaveDatas)
            {
                var perk = contexts.core
                    .GetEntitiesWithPerkId(perkSaveData.PerkId)
                    .SingleOrDefault(e => e.perkTier.value == perkSaveData.PerkTier);
                if (perk != null)
                {
                    perk.isPerkOwned =
                        perkSaveData.IsPerkOwned;
                }
            }

            foreach (var weaponSaveData in gameSave.WeaponSaveDatas)
            {
                contexts.core.GetEntityWithWeaponId(weaponSaveData.WeaponId).ReplaceLevel(weaponSaveData.Level);
            }

            contexts.core.playerEntity.ReplacePlayerMoney(gameSave.PlayerMoney);
            contexts.core.gameEntity.ReplaceGameCurrentDay(gameSave.LastLoginGameDay);
            contexts.core.gameEntity.ReplaceGameTotalKills(gameSave.TotalKills);
            contexts.core.barricadeEntity.ReplaceCurrentHealth(gameSave.BarricadeHealth);
            contexts.core.gameEntity.isGameOver = gameSave.IsGameOver;


            var secondsPassed = (int) (DateTime.Now - gameSave.LastLoginDate).TotalSeconds;
            contexts.core.gameEntity.ReplaceGameTimePassed(gameSave.LastLoginGameTime + secondsPassed);


            if (gameSave.IsGameOver)
            {
                var timer = gameSave.GameOverTimeLeft - secondsPassed;
                contexts.core.gameEntity.ReplaceGameOverTimer(timer);
                contexts.core.gameEntity.ReplaceGameOverDate(gameSave.GameOverDate);
            }
        }
    }
}
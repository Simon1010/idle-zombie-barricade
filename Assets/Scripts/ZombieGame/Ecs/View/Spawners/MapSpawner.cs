using Utilities;

namespace ZombieGame.Ecs.View.Spawners
{
    public class MapSpawner : ViewSpawner, IListenerAnyMap
    {
        protected override void OnInitalized()
        {
            Listener.AddListenerAnyMap(this);
        }

        public void OnAnyMap(CoreEntity coreEntity, bool isMap)
        {
            Spawn(GameResourceManager.Instance.GetPrefab("Map"), coreEntity);
        }
    }
}
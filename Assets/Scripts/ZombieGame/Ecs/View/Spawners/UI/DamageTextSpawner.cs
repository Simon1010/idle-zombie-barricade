using UnityEngine;
using Utilities;

namespace ZombieGame.Ecs.View.Spawners.UI
{
    public class DamageTextSpawner : ViewSpawner, IListenerAnyApplyDamage
    {
        protected override void OnInitalized()
        {
            Listener.AddListenerAnyApplyDamage(this);
        }

        public void OnAnyApplyDamage(CoreEntity coreEntity, bool isApplyDamage)
        {
            if (coreEntity.hasDamageAmount && coreEntity.damageAmount.value > 0)
            {
                var go = Spawn(GameResourceManager.Instance.GetPrefab("Damage Text"), false, true);
                var damageText = go.GetComponent<DamageText>();
                var target = Contexts.core.GetEntityWithCoreId(coreEntity.applyDamageTarget.value);
                damageText.transform.localPosition = target.position.value.ToUnityVector2();
                damageText.DamageTf.text = "-" + coreEntity.damageAmount.value.ToGameNumberFormat();
               
//                var randomX = Random.Range(300f, 600f);
//                if (Random.Range(0, 2) == 1)
//                {
//                    randomX = -randomX;
//                }
//                var randomY = Random.Range(3000f, 4500f);
//
//                go.GetComponent<Rigidbody2D>().AddForce(new Vector2(randomX, randomY), ForceMode2D.Impulse);
             
                go.AddComponent<DestroyAfter>().Initialize(5);
            }
        }
    }
}
using Utilities;

namespace ZombieGame.Ecs.View.Spawners.UI
{
    public class EnemyHealthBarSpawner : ViewSpawner, IListenerAnyCurrentHealth
    {
        protected override void OnInitalized()
        {
            Listener.AddListenerAnyCurrentHealth(this);
        }

        public void OnAnyCurrentHealth(CoreEntity coreEntity, float value)
        {
            if (coreEntity.isEnemy)
            {
                if (value < coreEntity.totalHealth.value)
                {
                    if (coreEntity.isDead == false && Has(coreEntity.coreId.value) == false)
                    {
                        Spawn(GameResourceManager.Instance.GetPrefab("Enemy Health Bar"), coreEntity, false, true);
                        coreEntity.ReplacePosition(coreEntity.position.value);
                    }
                    else
                    {
                        if (coreEntity.isDead || value <= 0)
                        {
                            if (Has(coreEntity.coreId.value))
                            {
                                Destroy(coreEntity.coreId.value);
                            }
                        }
                    }
                }
            }
        }
    }
}
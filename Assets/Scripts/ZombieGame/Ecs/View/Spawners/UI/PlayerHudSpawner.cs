using Utilities;

namespace ZombieGame.Ecs.View.Spawners.UI
{
    public class PlayerHudSpawner : ViewSpawner, IListenerAnyPlayer
    {
        protected override void OnInitalized()
        {
            Listener.AddListenerAnyPlayer(this);
        }

        public void OnAnyPlayer(CoreEntity coreEntity, bool isPlayer)
        {
            Spawn(GameResourceManager.Instance.GetPrefab("Player HUD"), coreEntity, true);
        }
    }
}
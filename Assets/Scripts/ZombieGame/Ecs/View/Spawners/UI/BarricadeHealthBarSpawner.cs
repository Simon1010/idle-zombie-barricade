using Utilities;

namespace ZombieGame.Ecs.View.Spawners.UI
{
    public class BarricadeHealthBarSpawner : ViewSpawner, IListenerAnyCurrentHealth
    {
        protected override void OnInitalized()
        {
            Listener.AddListenerAnyCurrentHealth(this);
        }

        public void OnAnyCurrentHealth(CoreEntity coreEntity, float value)
        {
            if (coreEntity.isBarricade)
            {
                if (Has(coreEntity.coreId.value) == false)
                {
                    Spawn(GameResourceManager.Instance.GetPrefab("Barricade Health Bar"), coreEntity, false, true);
                }
                else
                {
                    if (value <= 0)
                    {
                        Destroy(coreEntity.coreId.value);
                    }
                }
            }
        }
    }
}
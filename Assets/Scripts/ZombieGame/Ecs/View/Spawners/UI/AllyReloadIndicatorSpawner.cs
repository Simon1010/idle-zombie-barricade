using Utilities;

namespace ZombieGame.Ecs.View.Spawners.UI
{
    public class AllyReloadIndicatorSpawner : ViewSpawner, IListenerAnyAllyCurrentReloadTime
    {
        protected override void OnInitalized()
        {
            Listener.AddListenerAnyAllyCurrentReloadTime(this);
        }

        public void OnAnyAllyCurrentReloadTime(CoreEntity coreEntity, float value)
        {
            if (value <= 1)
            {
                if (Has(coreEntity.coreId.value))
                {
                    Destroy(coreEntity.coreId.value);
                }
            }
            else
            {
                if (Has(coreEntity.coreId.value) == false)
                {
                    Spawn(GameResourceManager.Instance.GetPrefab("Reload Indicator"), coreEntity, false, true);
                    coreEntity.ReplacePosition(coreEntity.position.value);
                }
            }
        }
    }
}
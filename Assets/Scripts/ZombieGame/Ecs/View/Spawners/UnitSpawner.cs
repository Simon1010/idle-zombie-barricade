using Utilities;

namespace ZombieGame.Ecs.View.Spawners
{
    public class UnitSpawner : ViewSpawner, IListenerAnyUnitId
    {
        protected override void OnInitalized()
        {
            Listener.AddListenerAnyUnitId(this);
        }

        public void OnAnyUnitId(CoreEntity coreEntity, GameEnums.UnitId value)
        {
            Spawn(GameResourceManager.Instance.GetPrefab(value.ToString()), coreEntity);
        }
    }
}
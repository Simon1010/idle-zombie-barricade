using Utilities;

namespace ZombieGame.Ecs.View.Spawners
{
    public class BulletSpawner : ViewSpawner, IListenerAnyBullet
    {
        protected override void OnInitalized()
        {
            Listener.AddListenerAnyBullet(this);
        }

        public void OnAnyBullet(CoreEntity coreEntity, bool isBullet)
        {
            Spawn(GameResourceManager.Instance.GetPrefab("Bullet"), coreEntity);
        }
    }
}
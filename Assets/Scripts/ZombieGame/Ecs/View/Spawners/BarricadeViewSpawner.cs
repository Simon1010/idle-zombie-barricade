using Utilities;

namespace ZombieGame.Ecs.View.Spawners
{
    public class BarricadeViewSpawner : ViewSpawner, IListenerAnyBarricade
    {
        protected override void OnInitalized()
        {
            Listener.AddListenerAnyBarricade(this);
        }

        public void OnAnyBarricade(CoreEntity coreEntity, bool isBarricade)
        {
            Spawn(GameResourceManager.Instance.GetPrefab("Barricade"), coreEntity);
        }
    }
}
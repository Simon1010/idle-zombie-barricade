using System.Collections.Generic;
using UnityEngine;
using Utilities;

namespace ZombieGame.Ecs.View.Spawners
{
    public class CashPileSpawner : ViewSpawner, IListenerAnyPlayerMoney
    {
        private List<CashPile> _piles;

        protected override void OnInitalized()
        {
            _piles = new List<CashPile>();
            Listener.AddListenerAnyPlayerMoney(this);
        }

        public void OnAnyPlayerMoney(CoreEntity coreEntity, int value)
        {
            var cashToPlace = (int) value;

            foreach (var cashPile in _piles)
            {
                cashPile.CashAmount = 0;
            }

            foreach (var cashPile in _piles)
            {
                cashToPlace = PlaceCashInPile(cashPile, cashToPlace);
            }

            while (cashToPlace > 0)
            {
                var cashPile = CreateNewPile();
                cashToPlace = PlaceCashInPile(cashPile, cashToPlace);
            }
        }

        private CashPile CreateNewPile()
        {
            var map = GameResourceManager.Instance.GetPrefab<Map>("Map");
            var cashPile =
                GameObject.Instantiate(GameResourceManager.Instance.GetPrefab<CashPile>("Cash Pile"), transform);
            cashPile.name = "Pile " + _piles.Count;
            int totalFloorPiles = map.GetCashPositions().Length;

            var pileIndex = _piles.Count;
            var pileStackCount = pileIndex / totalFloorPiles;

            if (pileIndex >= totalFloorPiles)
            {
                pileIndex -= pileStackCount * totalFloorPiles;
            }

            var position = cashPile.transform.position;
            position = map.GetCashPositions()[pileIndex].ToUnityVector2();

            //37 offset
            position.y += 37 * pileStackCount;
            cashPile.transform.position = position;


            var yRow = (int) ((float) _piles.Count % 3) + pileStackCount;
            yRow += pileIndex;
            cashPile.SetSortingOrder(yRow);


            _piles.Add(cashPile);
            return cashPile;
        }

        private int PlaceCashInPile(CashPile cashPile, int amount)
        {
            var space = cashPile.PileSize - cashPile.CashAmount;
            var amountPlaced = (int) Mathf.Min(space, amount);
            cashPile.CashAmount += amountPlaced;
            amount -= amountPlaced;
            return amount;
        }
    }
}
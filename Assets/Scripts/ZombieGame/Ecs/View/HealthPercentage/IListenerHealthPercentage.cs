namespace ZombieGame.Ecs.View.HealthPercentage
{
    public interface IListenerHealthPercentage
    {
        void OnHealthPercentage(float percentage);
    }
}
using System.Collections.Generic;
using Entitas;

namespace ZombieGame.Ecs.View.HealthPercentage
{
    public class HealthPercentageSystem : ReactiveSystem<CoreEntity>
    {
        private readonly Contexts _contexts;

        public HealthPercentageSystem(Contexts contexts) : base(contexts.core)
        {
            _contexts = contexts;
        }

        protected override ICollector<CoreEntity> GetTrigger(IContext<CoreEntity> context)
        {
            return context.CreateCollector(CoreMatcher.AllOf(CoreMatcher.CurrentHealth, CoreMatcher.TotalHealth));
        }

        protected override bool Filter(CoreEntity entity)
        {
            return entity.hasCurrentHealth;
        }

        protected override void Execute(List<CoreEntity> entities)
        {
            var listeners = _contexts.view.GetEntities(ViewMatcher.ListenerHealthPercentage);
            foreach (var coreEntity in entities)
            {
                foreach (var viewEntity in listeners)
                {
                    if (viewEntity.viewCoreId.value == coreEntity.coreId.value)
                    {
                        viewEntity.listenerHealthPercentage.value.OnHealthPercentage(
                            coreEntity.currentHealth.value / coreEntity.totalHealth.value);
                    }
                }
            }
        }
    }
}
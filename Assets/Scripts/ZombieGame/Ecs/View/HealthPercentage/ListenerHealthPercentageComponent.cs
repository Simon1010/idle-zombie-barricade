using Entitas;

namespace ZombieGame.Ecs.View.HealthPercentage
{
    [View]
    public class ListenerHealthPercentageComponent : IComponent
    {
        public IListenerHealthPercentage value;
    }
}
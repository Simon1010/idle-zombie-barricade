using UnityEngine;
using Utilities;
using Vector2 = System.Numerics.Vector2;

namespace ZombieGame.Ecs.View.Views
{
    public class BarricadeView : View, IListenerPosition, IListenerDead
    {
        protected override void OnInitalized()
        {
            Listener.AddListenerPosition(this);
            Listener.AddListenerDead(this);
        }

        protected override void OnDestroyed()
        {
        }

        public void OnPosition(Vector2 value)
        {
            transform.localPosition = value.ToUnityVector2();
        }

        public void OnDead(bool isDead)
        {
            var fade = gameObject.AddComponent<Fade>();
            fade.AlphaEnd = 0.7f;
            fade.AlphaStart = 1f;
            fade.FadeAcceleration = 0.3f;
        }
    }
}
using System;
using UnityEngine;
using UnityEngine.UI;
using Utilities;
using ZombieGame.Data;

namespace ZombieGame.Ecs.View.Views.UI
{
    public class PerkSlotView : View, IListenerPerkId, IListenerPerkOwned
    {
        public Image SlotIcon;
        public Image PerkIcon;
        public Image CheckMark;
        public Button PerkButton;

        public delegate void OnClickMe(PerkSlotView slotView);

        public OnClickMe OnClick;

        protected override void OnInitalized()
        {
            Listener.AddListenerPerkId(this);
            Listener.AddListenerPerkOwned(this);
            PerkButton.onClick.AddListener(Click);

            UpdateView();
        }

        private void Click()
        {
            OnClick(this);
        }

        private void UpdateView()
        {
            var perkData = GetPerkData();

            var iconName = perkData.PerkIconName ?? GetPerkTierData().DefaultPerkIconName;
            PerkIcon.sprite = GameResourceManager.Instance.GetSprite(iconName);
            CheckMark.enabled = GetEntity().isPerkOwned;
            PerkIcon.SetNativeSize();

            string slotIconName;
            switch (GetEntity().perkTier.value)
            {
                case GameEnums.PerkTier.Copper:
                    slotIconName = "gui_perks_icon_bronze";
                    break;
                case GameEnums.PerkTier.Silver:
                    slotIconName = "gui_perks_icon_silver";
                    break;
                case GameEnums.PerkTier.Gold:
                    slotIconName = "gui_perks_icon_gold";
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            SlotIcon.sprite = GameResourceManager.Instance.GetSprite(slotIconName);
        }

        public void SetSelected(bool selected)
        {
            SlotIcon.color = selected ? Color.green : Color.white;
        }


        protected override void OnDestroyed()
        {
        }

        public void OnPerkId(GameEnums.PerkId value)
        {
            UpdateView();
        }

        public void OnPerkOwned(bool isPerkOwned)
        {
            UpdateView();
        }

        public PerkData GetPerkData()
        {
            var perkData = GameData.GetPerkData(GetEntity().perkId.value, GetEntity().perkTier.value);
            return perkData;
        }

        public PerkTierData GetPerkTierData()
        {
            var perkTierData = GameData.GetPerkTierData(GetEntity().perkId.value);
            return perkTierData;
        }
    }
}
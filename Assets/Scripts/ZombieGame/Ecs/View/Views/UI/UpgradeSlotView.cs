using TMPro;
using UnityEngine.UI;
using Utilities;
using ZombieGame.Ecs.Commands;

namespace ZombieGame.Ecs.View.Views.UI
{
    public class UpgradeSlotView : View, IListenerLevel, IListenerAnyPlayerMoney
    {
        public Image Icon;
        public TMP_Text CostText;
        public TMP_Text LevelText;
        public Button BuyButton;

        protected override void OnInitalized()
        {
            Listener.AddListenerLevel(this);
            Listener.AddListenerAnyPlayerMoney(this);

            BuyButton.onClick.AddListener(AttemptPurchase);
            RefreshView();
            CheckIfCanBuy();

        }

        private void AttemptPurchase()
        {
            var weaponData = GameData.GetWeaponData(GetEntity().weaponId.value);
            var upgradeCost = GameCalculations.CalculateInt(weaponData.UpgradePrice, GetEntity());
            GameInputActions.CreatePurchaseUpgrade(Contexts, EntityId, upgradeCost);
        }

        protected override void OnDestroyed()
        {
        }

        public void OnLevel(int value)
        {
            RefreshView();
            CheckIfCanBuy();
        }

        public void OnAnyPlayerMoney(CoreEntity coreEntity, int value)
        {
            CheckIfCanBuy();
        }

        private void RefreshView()
        {
            var weaponData = GameData.GetWeaponData(GetEntity().weaponId.value);
            Icon.sprite = GameResourceManager.Instance.GetSprite(weaponData.WeaponUpgradeShopIcon);
            Icon.SetNativeSize();
            CostText.text = GameCalculations.Calculate(weaponData.UpgradePrice, GetEntity()).ToGameNumberFormat();
            LevelText.text = GetEntity().level.value.ToString();
        }

        private void CheckIfCanBuy()
        {
            var playerMoney = Contexts.core.playerEntity.playerMoney.value;
            var weaponData = GameData.GetWeaponData(GetEntity().weaponId.value);
            var upgradeCost = GameCalculations.Calculate(weaponData.UpgradePrice, GetEntity());
            BuyButton.interactable = playerMoney >= upgradeCost;
        }
    }
}
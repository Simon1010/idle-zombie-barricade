using Entitas;
using UnityEngine;
using Utilities;

namespace ZombieGame.Ecs.View.Views.UI
{
    public class UpgradeMenuView : View
    {
        public Transform UpgradeSlotContainer;

        protected override void OnInitalized()
        {
            CreateSlots();
        }

        private void CreateSlots()
        {
            var weapons = Contexts.core.GetEntities(CoreMatcher.Weapon);
            var childCount = UpgradeSlotContainer.childCount;

            if (childCount == weapons.Length)
            {
                return;
            }

            for (int i = childCount - 1; i >= 0; i--)
            {
                GameObject.Destroy(UpgradeSlotContainer.GetChild(i).gameObject);
            }

            for (int i = 0; i < weapons.Length; i++)
            {
                var slot = GameObject.Instantiate(GameResourceManager.Instance.GetPrefab<View>("Upgrade Slot"),
                    UpgradeSlotContainer);
                slot.Initialize(Contexts, weapons[i]);
            }
        }

        protected override void OnDestroyed()
        {
        }
    }
}
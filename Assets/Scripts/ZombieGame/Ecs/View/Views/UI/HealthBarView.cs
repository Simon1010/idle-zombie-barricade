using System;
using DigitalRuby.Tween;
using UnityEngine;
using UnityEngine.UI;
using ZombieGame.Ecs.View.HealthPercentage;

namespace ZombieGame.Ecs.View.Views.UI
{
    public class HealthBarView : View, IListenerPosition, IListenerHealthPercentage, IListenerDead
    {
        public Vector2 PositionOffset;
        public Image Fill;
        public Image Highlight;
        private Action<ITween<float>> _fillTween;
        private Action<ITween<float>> _highlightTween;
        public float TweenTime = 0.3f;
        public float StartFill = 1f;

        protected override void OnInitalized()
        {
            Listener.AddListenerPosition(this);
            Listener.AddListenerDead(this);
            ViewEntity.AddListenerHealthPercentage(this);

            Fill.fillAmount = StartFill;
            Highlight.fillAmount = StartFill;

            _fillTween = t =>
            {
                if (this != null)
                {
                    Fill.fillAmount = t.CurrentValue;
                }
            };
            _highlightTween = t =>
            {
                if (this != null)
                {
                    Highlight.fillAmount = t.CurrentValue;
                }
            };
        }

        protected override void OnDestroyed()
        {
        }

        public void OnPosition(System.Numerics.Vector2 value)
        {
            transform.localPosition = value.ToUnityVector2() + PositionOffset;
        }

        public void OnHealthPercentage(float percentage)
        {
            if (percentage > Fill.fillAmount)
            {
                Action<ITween<float>> fillTween = (t) =>
                {
                    TweenFactory.Tween("FillAmount" + EntityId, Fill.fillAmount, percentage, TweenTime,
                        TweenScaleFunctions.Linear, _fillTween).Delay = 0.3f;
                };

                TweenFactory.Tween("HighlightTween" + EntityId, Highlight.fillAmount, percentage,
                    TweenTime,
                    TweenScaleFunctions.Linear, _fillTween);
            }
            else

            {
                Action<ITween<float>> highlightTween = (t) =>
                {
                    TweenFactory.Tween("HighlightTween" + EntityId, Highlight.fillAmount, percentage,
                        TweenTime,
                        TweenScaleFunctions.Linear, _highlightTween).Delay = 0.3f;
                };

                TweenFactory.Tween("FillAmount" + EntityId, Fill.fillAmount, percentage, TweenTime,
                    TweenScaleFunctions.Linear, _fillTween, highlightTween);
            }
        }

        public void OnDead(bool isDead)
        {
            TweenFactory.RemoveTweenKey("FillAmount" + EntityId, TweenStopBehavior.Complete);
            TweenFactory.RemoveTweenKey("HighlightTween" + EntityId, TweenStopBehavior.Complete);
        }
    }
}
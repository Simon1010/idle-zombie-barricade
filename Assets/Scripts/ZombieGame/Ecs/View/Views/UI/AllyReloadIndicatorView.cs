using System;
using System.Numerics;

namespace ZombieGame.Ecs.View.Views.UI
{
    public class AllyReloadIndicatorView : View, IListenerPosition
    {
        public UnityEngine.Vector2 PositionOffset;


        protected override void OnInitalized()
        {
            Listener.AddListenerPosition(this);
        }

        protected override void OnDestroyed()
        {
        }

        private void Update()
        {
            transform.Rotate(0, 0, 3f);
        }

        public void OnPosition(Vector2 value)
        {
            transform.localPosition = value.ToUnityVector2() + PositionOffset;
        }
    }
}
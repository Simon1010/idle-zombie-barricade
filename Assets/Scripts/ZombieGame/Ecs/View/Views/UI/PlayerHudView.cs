using System;
using DigitalRuby.Tween;
using TMPro;
using UnityEngine.UI;
using Utilities;
using Utilities.Menu;

namespace ZombieGame.Ecs.View.Views.UI
{
    public class PlayerHudView : View, IListenerPlayerMoney, IListenerAnyGameTotalKills, IListenerAnyGameCurrentDay
    {
        public Button UpgradeMenuButton;
        public Button PerksMenuButton;

        public NumberText KillCountTF;
        public NumberText DayText;
        public NumberText MoneyText;

        protected override void OnInitalized()
        {
            UpgradeMenuButton.onClick.AddListener(OpenUpgradeMenu);
            PerksMenuButton.onClick.AddListener(OpenPerksMenu);
            Listener.AddListenerPlayerMoney(this);
            Listener.AddListenerAnyGameTotalKills(this);
            Listener.AddListenerAnyGameCurrentDay(this);
        }

        private void OpenUpgradeMenu()
        {
            GameMenuManager.Instance.CreateMenu(GameEnums.MenuId.Upgrade);
        }

        private void OpenPerksMenu()
        {
            GameMenuManager.Instance.CreateMenu(GameEnums.MenuId.Perk);
        }

        protected override void OnDestroyed()
        {
        }

        public void OnPlayerMoney(int value)
        {
            MoneyText.SetValue(value);
        }

        public void OnAnyGameTotalKills(CoreEntity coreEntity, int value)
        {
            KillCountTF.SetValue(value);
        }

        public void OnAnyGameCurrentDay(CoreEntity coreEntity, int value)
        {
            DayText.Format = "Day {0}";
            DayText.SetValue(value);
        }

        private void Update()
        {
            ToggleBottomBar(Contexts.core.gameEntity.isGameOver == false);
        }

        private void ToggleBottomBar(bool value)
        {
            UpgradeMenuButton.gameObject.SetActive(value);
            PerksMenuButton.gameObject.SetActive(value);
        }
    }
}
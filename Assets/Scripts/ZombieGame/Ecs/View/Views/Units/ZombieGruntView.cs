using System;
using UnityEngine;
using UnityEngine.Rendering;
using Utilities;
using Vector2 = System.Numerics.Vector2;

namespace ZombieGame.Ecs.View.Views.Units
{
    public class ZombieGruntView : View, IListenerAttackId, IListenerAttackComplete, IListenerPosition, IListenerDead,
        IListenerUnitCurrentState, IListenerCurrentHealth, IListenerDestroyed
    {
        private Animator _animator;
        private Shake _shaker;
        private float _previousHealth = -1f;

        private void Awake()
        {
            _animator = GetComponent<Animator>();
            _shaker = GetComponentInChildren<Shake>();
        }

        protected override void OnInitalized()
        {
            IsAutoDestroy = false;

            Listener.AddListenerDestroyed(this);
            Listener.AddListenerPosition(this);
            Listener.AddListenerAttackId(this);
            Listener.AddListenerAttackComplete(this);
            Listener.AddListenerDead(this);
            Listener.AddListenerCurrentHealth(this);
            Listener.AddListenerUnitCurrentState(this);
        }

        protected override void OnDestroyed()
        {
        }


        public void OnAttackId(GameEnums.AttackId value)
        {
            switch (value)
            {
                case GameEnums.AttackId.Melee_001:
                    _animator.Play("Attack_001");
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(value), value, null);
            }
        }

        public void OnAttackComplete(bool isAttackComplete)
        {
            _animator.Play("Idle");
        }

        public void OnPosition(Vector2 value)
        {
            transform.localPosition = value.ToUnityVector2();
        }

        public void OnDead(bool isDead)
        {
            _animator.Play("Dead");

            var renderers = GetComponentsInChildren<SpriteRenderer>();
            foreach (var spriteRenderer in renderers)
            {
                spriteRenderer.sortingOrder = 0;
            }

            var sortingGroups = GetComponentsInChildren<SortingGroup>();
            foreach (var sortingGroup in sortingGroups)
            {
                sortingGroup.enabled = false;
            }
        }

        public void OnUnitCurrentState(GameEnums.UnitState value)
        {
            switch (value)
            {
                case GameEnums.UnitState.Idle:
                    _animator.Play("Idle");
                    break;
                case GameEnums.UnitState.Attacking:
                    break;
                case GameEnums.UnitState.Advancing:
                    _animator.Play("Walk");
                    break;
                case GameEnums.UnitState.Dead:
                    _animator.Play("Dead");
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(value), value, null);
            }
        }


        public void OnCurrentHealth(float value)
        {
            if (_previousHealth != -1 && _previousHealth > value)
            {
                _shaker.DoShake(10f, 2f);
            }

            _previousHealth = value;
        }

        public void OnDestroyed(bool isDestroyed)
        {
            var fade = gameObject.AddComponent<Fade>();
            fade.SpriteRenderers = GetComponentsInChildren<SpriteRenderer>();
            fade.DestroyWhenDone = true;
            fade.WaitTime = 0;
            fade.AlphaStart = 1;
            fade.AlphaEnd = 0;
            fade.FadeAcceleration = 0.09f;
        }
    }
}
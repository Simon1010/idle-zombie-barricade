using System;
using UnityEngine;
using Utilities;
using Random = UnityEngine.Random;
using Vector2 = System.Numerics.Vector2;

namespace ZombieGame.Ecs.View.Views.Units
{
    public class AllyView : View, IListenerAllyWeaponEquipped, IListenerPosition, IListenerAttackId, IListenerAnyBullet,
        IListenerUnitCurrentState
    {
        public Transform WeaponContainer;
        private Transform _muzzleFlashContainer;
        private Animator _animator;

        public SpriteRenderer HeadRenderer;
        public SpriteRenderer TorsoRenderer;

        private void Awake()
        {
            _animator = GetComponent<Animator>();
        }

        protected override void OnInitalized()
        {
            Listener.AddListenerAllyWeaponEquipped(this);
            Listener.AddListenerPosition(this);
            Listener.AddListenerAnyBullet(this);
            Listener.AddListenerAttackId(this);
            Listener.AddListenerUnitCurrentState(this);
        }

        protected override void OnDestroyed()
        {
        }

        public void OnAllyWeaponEquipped(CoreEntityId coreEntityId)
        {
            var weapon = Contexts.core.GetEntityWithCoreId(coreEntityId);
            var prefabName = weapon.weaponId.value.ToString();
            for (int i = 0; i < WeaponContainer.childCount; i++)
            {
                GameObject.Destroy(WeaponContainer.transform.GetChild(i).gameObject);
            }

            var prefab = GameResourceManager.Instance.GetPrefab<Weapon>(prefabName);
            if (prefab != null)
            {
                var go = GameObject.Instantiate(prefab, WeaponContainer);
                go.transform.localPosition = Vector3.zero;

                if (_muzzleFlashContainer == null)
                {
                    _muzzleFlashContainer = new GameObject("Muzzle Flash Container").transform;
                }

                _muzzleFlashContainer.transform.SetParent(go.transform);
                _muzzleFlashContainer.transform.localPosition = go.MuzzlePosition.localPosition;
            }


            var weaponId = (int) weapon.weaponId.value;

            if (weaponId > 4)
            {
                weaponId = 4;
            }

            //Temp fix because we have 5 head sprites and 4 torso sprites
            var torsoId = weaponId + 1;
            torsoId = Mathf.Min(4, torsoId);

            HeadRenderer.sprite = GameResourceManager.Instance.GetSprite("char_ally_head_000" + (weaponId + 1));
            TorsoRenderer.sprite = GameResourceManager.Instance.GetSprite("char_ally_torso_000" + torsoId);
        }

        public void OnPosition(Vector2 value)
        {
            transform.localPosition = value.ToUnityVector2();
        }

        public void OnAttackId(GameEnums.AttackId value)
        {
        }

        public void OnAnyBullet(CoreEntity coreEntity, bool isBullet)
        {
            if (coreEntity.bulletSource.value == EntityId)
            {
                var muzzleFlashNames = new[] {"muzzle_flash_001", "muzzle_flash_002", "muzzle_flash_003"};
                var muzzleFlashName = muzzleFlashNames[Random.Range(0, muzzleFlashNames.Length)];
                var go = new GameObject("Muzzle Flash");
                go.transform.SetParent(_muzzleFlashContainer);
                go.transform.localPosition = Vector3.zero;
                go.AddComponent<DestroyAfter>().Initialize(0.05f);
                var sr = go.AddComponent<SpriteRenderer>();
                sr.sprite = GameResourceManager.Instance.GetSprite(muzzleFlashName);
                sr.sortingLayerName = "Stage";
                sr.sortingOrder = 3;
            }
        }

        public void OnUnitCurrentState(GameEnums.UnitState value)
        {
            return;

            switch (value)
            {
                case GameEnums.UnitState.Idle:
                    _animator.Play("Idle");
                    break;
                case GameEnums.UnitState.Attacking:
                    _animator.Play("Fire");
                    break;
                case GameEnums.UnitState.Advancing:
                    _animator.Play("Move");
                    break;
                case GameEnums.UnitState.Flee:
                    _animator.Play("Move");
                    break;
                case GameEnums.UnitState.Dead:
                    _animator.Play("Dead");
                    break;
                case GameEnums.UnitState.Reloading:
                    _animator.Play("Reload");
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(value), value, null);
            }
        }
    }
}
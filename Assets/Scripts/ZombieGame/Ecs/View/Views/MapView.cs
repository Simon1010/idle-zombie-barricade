using System.Numerics;

namespace ZombieGame.Ecs.View.Views
{
    public class MapView : View, IListenerPosition
    {
        protected override void OnInitalized()
        {
            Listener.AddListenerPosition(this);
        }

        protected override void OnDestroyed()
        {
        }

        public void OnPosition(Vector2 value)
        {
            transform.localPosition = value.ToUnityVector2();
        }
    }
}
using System;
using UnityEngine;
using ZombieGame.Ecs.Commands;
using Vector2 = System.Numerics.Vector2;

namespace ZombieGame.Ecs.View.Views
{
    public class BulletView : View, IListenerPosition
    {
        protected override void OnInitalized()
        {
            Listener.AddListenerPosition(this);
        }

        protected override void OnDestroyed()
        {
        }

        public void OnPosition(Vector2 value)
        {
            transform.localPosition = value.ToUnityVector2();
        }

        private void Update()
        {
            if (GetEntity().hasVelocity)
            {
                float angle = Mathf.Atan2(GetEntity().velocity.value.Y, GetEntity().velocity.value.X) * Mathf.Rad2Deg;

                transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
            }
        }

        public override void OnCollision(CoreEntity other)
        {
            if (other.isEnemy)
            {
                GameInputActions.CreateInputActionCollision(Contexts, EntityId, other.coreId.value);
            }
        }
    }
}
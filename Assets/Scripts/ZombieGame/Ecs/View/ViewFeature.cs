using ZombieGame.Ecs.View.HealthPercentage;

namespace ZombieGame.Ecs.View
{
    public class ViewFeature : Feature
    {
        public ViewFeature(Contexts contexts)
        {
            Add(new HealthPercentageSystem(contexts));
        }
    }
}
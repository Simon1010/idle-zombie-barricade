using System;
using System.Collections.Generic;
using Entitas.Unity;
using UnityEngine;

namespace ZombieGame.Ecs.View
{
    public abstract class ViewSpawner : MonoBehaviour, IViewSpawner, IListenerAnyDestroyed
    {
        private GameObject _canvasPrefab;
        private GameObject _worldCanvasPrefab;

        protected Dictionary<CoreEntityId, GameObject> _gameObjects;
        private Dictionary<System.Type, IViewSpawner> _otherSpawners;
        protected Transform _container;
        protected Transform _canvasContainer;
        protected Transform _worldCanvasContainer;
        protected Contexts Contexts;
        protected ListenerEntity Listener;

        private void Awake()
        {
            _canvasPrefab = Resources.Load<GameObject>("Canvas");
            _worldCanvasPrefab = Resources.Load<GameObject>("World Canvas");
        }

        protected T GetSpawner<T>() where T : IViewSpawner
        {
            return (T) _otherSpawners[typeof(T)];
        }

        public void SupplyContext(Contexts contexts)
        {
            Contexts = contexts;
        }

        public void Initialize(IViewSpawner[] otherSpawners)
        {
            _otherSpawners = new Dictionary<Type, IViewSpawner>();
            foreach (var otherSpawner in otherSpawners)
            {
                _otherSpawners.Add(otherSpawner.GetType(), otherSpawner);
            }

            _container = new GameObject("Entity Container").transform;
            _container.transform.SetParent(transform);

            _canvasContainer = GameObject.Instantiate(_canvasPrefab, transform).transform;
            _canvasContainer = GameObject.Find("Screen Canvas").transform;
            _worldCanvasContainer = GameObject.Instantiate(_worldCanvasPrefab, transform).transform;

            Clear();
            CreateListener();
            OnInitalized();
        }

        protected abstract void OnInitalized();

        private void CreateListener()
        {
            Listener = Contexts.listener.CreateEntity();
            Listener.AddListenerAnyDestroyed(this);
        }

        public void Clear()
        {
            if (_gameObjects != null)
            {
                foreach (var element in _gameObjects)
                {
                    Destroy(element.Value);
                }
            }

            _gameObjects = new Dictionary<CoreEntityId, GameObject>();
        }

        protected GameObject Spawn(GameObject prefab, bool isScreenSpaceUi = false, bool isWorldSpaceUi = false)
        {
            GameObject go = null;

            if (isWorldSpaceUi == false)
            {
                if (isScreenSpaceUi == false)
                {
                    go = Instantiate(prefab, _container.transform);
                }
                else
                {
                    go = Instantiate(prefab, _canvasContainer.transform);
                }
            }
            else
            {
                go = Instantiate(prefab, _worldCanvasContainer.transform);
            }

            return go;
        }

        protected void Spawn(GameObject prefab, CoreEntity coreEntity, bool isScreenSpaceUi = false,
            bool isWorldSpaceUi = false)
        {
            if (prefab.GetComponent<View>() == null)
            {
                Debug.LogError("Prefab does not have ViewComponent");
                return;
            }

            var go = Spawn(prefab, isScreenSpaceUi, isWorldSpaceUi);

            var id = coreEntity.coreId.value;
            if (_gameObjects.ContainsKey(id))
            {
                Debug.LogError("Key already found " + id);
                return;
            }


            var viewController = go.GetComponent<View>();
            viewController.Initialize(Contexts, coreEntity);
            go.Link(coreEntity);
            _gameObjects.Add(id, go);
        }


        public void Destroy(CoreEntityId id)
        {
            if (_gameObjects.ContainsKey(id) == false)
            {
                Debug.LogError("Key not found " + id);
                return;
            }

            var go = _gameObjects[id];
            go.Unlink();

            var view = go.GetComponent<View>();
            Destroy(go);
            _gameObjects.Remove(id);
        }

        public bool Has(CoreEntityId id)
        {
            return _gameObjects.ContainsKey(id);
        }

        public GameObject Get(CoreEntityId id)
        {
            return _gameObjects[id];
        }

        public GameObject GetFirst()
        {
            foreach (var o in _gameObjects)
            {
                return o.Value;
            }

            return null;
        }


        public void OnAnyDestroyed(CoreEntity coreEntity, bool isDestroyed)
        {
            if (Has(coreEntity.coreId.value))
            {
                if (Get(coreEntity.coreId.value).GetComponent<View>().IsAutoDestroy)
                {
                    Destroy(coreEntity.coreId.value);
                }
            }
        }
    }
}
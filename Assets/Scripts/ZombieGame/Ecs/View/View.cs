using UnityEngine;

namespace ZombieGame.Ecs.View
{
    public abstract class View : MonoBehaviour
    {
        protected Contexts Contexts;
        protected ListenerEntity Listener;
        protected ViewEntity ViewEntity;
        public CoreEntityId EntityId;
        private bool _initialized = false;
        public bool IsAutoDestroy = true;

        protected abstract void OnInitalized();
        protected abstract void OnDestroyed();

        public CoreEntity GetEntity()
        {
            if (EntityId == CoreEntityId.Empty)
            {
                return null;
            }

            return Contexts.core.GetEntityWithCoreId(EntityId);
        }

        public void Initialize(Contexts contexts, CoreEntity coreEntity)
        {
            if (_initialized)
            {
                Listener.Destroy();
                ViewEntity.Destroy();
            }

            Contexts = contexts;
            EntityId = coreEntity.coreId.value;
            Listener = Contexts.listener.CreateEntity();
            Listener.ReplaceListenerCoreId(EntityId);
            ViewEntity = Contexts.view.CreateEntity();
            ViewEntity.ReplaceViewCoreId(EntityId);

            _initialized = true;
            OnInitalized();
        }

        private void OnDestroy()
        {
            if (_initialized)
            {
                Listener.Destroy();
                ViewEntity.Destroy();
                OnDestroyed();
            }
        }

        private void OnCollisionEnter2D(Collision2D other)
        {
            if (other.gameObject != null)
            {
                var view = other.gameObject.GetComponent<View>();
                if (view != null)
                {
                    OnCollision(view.GetEntity());
                }
            }
        }

        public virtual void OnCollision(CoreEntity other)
        {
        }
    }
}
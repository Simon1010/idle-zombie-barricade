using Entitas;

namespace ZombieGame.Ecs.View
{
    [View]
    public class ViewCoreIdComponent : IComponent
    {
        public CoreEntityId value;
    }
}
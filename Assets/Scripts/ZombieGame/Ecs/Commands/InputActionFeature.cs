using ZombieGame.Ecs.Commands.Collision;
using ZombieGame.Ecs.Commands.PurchasePerk;
using ZombieGame.Ecs.Commands.PurchaseUpgrade;
using ZombieGame.Ecs.Commands.SetGameState;
using ZombieGame.Ecs.Commands.Tap;

namespace ZombieGame.Ecs.Commands
{
    public class InputActionFeature : Feature
    {
        public InputActionFeature(Contexts contexts)
        {
            Add(new CreateCollisionInputActionSystem(contexts));
            Add(new PurchaseUpgradeInputActionSystem(contexts));
            Add(new SetGameStateInputActionSystem(contexts));
            Add(new TapInputActionSystem(contexts));
            Add(new PurchasePerkInputActionSystem(contexts));

            Add(new DestroyInputActionsSystem(contexts));
        }
    }
}
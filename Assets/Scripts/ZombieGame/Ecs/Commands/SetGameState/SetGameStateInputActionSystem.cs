using System.Collections.Generic;
using Entitas;

namespace ZombieGame.Ecs.Commands.SetGameState
{
    public class SetGameStateInputActionSystem : ReactiveSystem<InputActionEntity>
    {
        private readonly Contexts _contexts;

        public SetGameStateInputActionSystem(Contexts contexts) : base(contexts.inputAction)
        {
            _contexts = contexts;
        }

        protected override ICollector<InputActionEntity> GetTrigger(IContext<InputActionEntity> context)
        {
            return context.CreateCollector(InputActionMatcher.SetGameStateInputAction);
        }

        protected override bool Filter(InputActionEntity entity)
        {
            return entity.hasSetGameStateInputAction;
        }

        protected override void Execute(List<InputActionEntity> entities)
        {
            foreach (var inputActionEntity in entities)
            {
                GameCommands.CreateCommandSetGameState(_contexts, inputActionEntity.setGameStateInputAction.GameState);
            }
        }
    }
}
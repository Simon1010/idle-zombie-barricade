using Entitas;

namespace ZombieGame.Ecs.Commands.SetGameState
{
    [Command]
    public class SetGameStateCommandComponent : IComponent
    {
        public GameEnums.GameState GameState;
    }
}
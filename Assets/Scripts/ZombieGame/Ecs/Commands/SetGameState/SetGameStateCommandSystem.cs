using System.Collections.Generic;
using Entitas;

namespace ZombieGame.Ecs.Commands.SetGameState
{
    public class SetGameStateCommandSystem : ReactiveSystem<CommandEntity>
    {
        private readonly Contexts _contexts;

        public SetGameStateCommandSystem(Contexts contexts) : base(contexts.command)
        {
            _contexts = contexts;
        }

        protected override ICollector<CommandEntity> GetTrigger(IContext<CommandEntity> context)
        {
            return context.CreateCollector(CommandMatcher.SetGameStateCommand);
        }

        protected override bool Filter(CommandEntity entity)
        {
            return entity.hasSetGameStateCommand;
        }

        protected override void Execute(List<CommandEntity> entities)
        {
            foreach (var commandEntity in entities)
            {
                _contexts.core.gameEntity.ReplaceGameState(commandEntity.setGameStateCommand.GameState);
            }
        }
    }
}
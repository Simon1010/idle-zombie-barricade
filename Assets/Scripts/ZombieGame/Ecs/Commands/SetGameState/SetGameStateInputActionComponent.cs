using Entitas;

namespace ZombieGame.Ecs.Commands.SetGameState
{
    [InputAction]
    public class SetGameStateInputActionComponent : IComponent
    {
        public GameEnums.GameState GameState;
    }
}
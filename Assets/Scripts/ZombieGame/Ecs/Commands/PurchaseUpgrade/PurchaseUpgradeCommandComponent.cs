using Entitas;

namespace ZombieGame.Ecs.Commands.PurchaseUpgrade
{
    [Command]
    public class PurchaseUpgradeCommandComponent : IComponent
    {
        public CoreEntityId EntityId;
        public int UpgradeCost;
    }
}
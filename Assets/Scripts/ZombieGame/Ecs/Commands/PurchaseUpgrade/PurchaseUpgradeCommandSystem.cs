using System.Collections.Generic;
using Entitas;

namespace ZombieGame.Ecs.Commands.PurchaseUpgrade
{
    public class PurchaseUpgradeCommandSystem : ReactiveSystem<CommandEntity>
    {
        private readonly Contexts _contexts;

        public PurchaseUpgradeCommandSystem(Contexts contexts) : base(contexts.command)
        {
            _contexts = contexts;
        }

        protected override ICollector<CommandEntity> GetTrigger(IContext<CommandEntity> context)
        {
            return context.CreateCollector(CommandMatcher.PurchaseUpgradeCommand);
        }

        protected override bool Filter(CommandEntity entity)
        {
            return entity.hasPurchaseUpgradeCommand;
        }

        protected override void Execute(List<CommandEntity> entities)
        {
            foreach (var commandEntity in entities)
            {
                var playerMoney = _contexts.core.playerEntity.playerMoney.value;
                playerMoney -= commandEntity.purchaseUpgradeCommand.UpgradeCost;
                _contexts.core.playerEntity.ReplacePlayerMoney(playerMoney);

                var weapon = _contexts.core.GetEntityWithCoreId(commandEntity.purchaseUpgradeCommand.EntityId);
                weapon.ReplaceLevel(++weapon.level.value);
            }
        }
    }
}
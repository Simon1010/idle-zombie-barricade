using Entitas;

namespace ZombieGame.Ecs.Commands.PurchaseUpgrade
{
    [InputAction]
    public class PurchaseUpgradeInputActionComponent : IComponent
    {
        public CoreEntityId EntityId;
        public int UpgradeCost;
    }
}
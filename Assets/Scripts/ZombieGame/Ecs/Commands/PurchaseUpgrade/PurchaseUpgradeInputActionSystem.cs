using System.Collections.Generic;
using Entitas;

namespace ZombieGame.Ecs.Commands.PurchaseUpgrade
{
    public class PurchaseUpgradeInputActionSystem : ReactiveSystem<InputActionEntity>
    {
        private readonly Contexts _contexts;

        public PurchaseUpgradeInputActionSystem(Contexts contexts) : base(contexts.inputAction)
        {
            _contexts = contexts;
        }

        protected override ICollector<InputActionEntity> GetTrigger(IContext<InputActionEntity> context)
        {
            return context.CreateCollector(InputActionMatcher.PurchaseUpgradeInputAction);
        }

        protected override bool Filter(InputActionEntity entity)
        {
            return entity.hasPurchaseUpgradeInputAction;
        }

        protected override void Execute(List<InputActionEntity> entities)
        {
            foreach (var inputActionEntity in entities)
            {
                if (inputActionEntity.purchaseUpgradeInputAction.UpgradeCost >
                    _contexts.core.playerEntity.playerMoney.value)
                {
                    continue;
                }

                GameCommands.CreateCommandPurchaseUpgrade(_contexts,
                    inputActionEntity.purchaseUpgradeInputAction.EntityId,
                    inputActionEntity.purchaseUpgradeInputAction.UpgradeCost);
                
            }
        }
    }
}
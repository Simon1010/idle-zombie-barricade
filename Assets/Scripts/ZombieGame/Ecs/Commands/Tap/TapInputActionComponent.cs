using System.Numerics;
using Entitas;

namespace ZombieGame.Ecs.Commands.Tap
{
    [InputAction]
    public class TapInputActionComponent : IComponent
    {
        public Vector2 position;
    }
}
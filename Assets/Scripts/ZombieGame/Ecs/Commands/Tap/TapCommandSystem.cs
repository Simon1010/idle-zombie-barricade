using System.Collections.Generic;
using Entitas;

namespace ZombieGame.Ecs.Commands.Tap
{
    public class TapDamageCommandSystem : ReactiveSystem<CommandEntity>
    {
        private readonly Contexts _contexts;

        public TapDamageCommandSystem(Contexts contexts) : base(contexts.command)
        {
            _contexts = contexts;
        }

        protected override ICollector<CommandEntity> GetTrigger(IContext<CommandEntity> context)
        {
            return context.CreateCollector(CommandMatcher.TapCommand);
        }

        protected override bool Filter(CommandEntity entity)
        {
            return entity.hasTapCommand;
        }

        protected override void Execute(List<CommandEntity> entities)
        {
            foreach (var commandEntity in entities)
            {
                GameFactory.CreateTap(_contexts, commandEntity.tapCommand.position);
            }
        }
    }
}
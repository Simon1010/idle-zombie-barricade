using System.Numerics;
using Entitas;

namespace ZombieGame.Ecs.Commands.Tap
{
    [Command]
    public class TapCommandComponent : IComponent
    {
        public Vector2 position;
    }
}
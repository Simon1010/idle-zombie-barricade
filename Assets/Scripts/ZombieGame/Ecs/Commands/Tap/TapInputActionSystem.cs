using System.Collections.Generic;
using Entitas;

namespace ZombieGame.Ecs.Commands.Tap
{
    public class TapInputActionSystem : ReactiveSystem<InputActionEntity>
    {
        private readonly Contexts _contexts;

        public TapInputActionSystem(Contexts contexts) : base(contexts.inputAction)
        {
            _contexts = contexts;
        }

        protected override ICollector<InputActionEntity> GetTrigger(IContext<InputActionEntity> context)
        {
            return context.CreateCollector(InputActionMatcher.TapInputAction);
        }

        protected override bool Filter(InputActionEntity entity)
        {
            return entity.hasTapInputAction;
        }

        protected override void Execute(List<InputActionEntity> entities)
        {
            foreach (var inputActionEntity in entities)
            {
                GameCommands.CreateTapOnMap(_contexts, inputActionEntity.tapInputAction.position);
            }
        }
    }
}
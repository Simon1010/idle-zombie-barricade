using Entitas;

namespace ZombieGame.Ecs.Commands
{
    public class DestroyInputActionsSystem : IExecuteSystem
    {
        private readonly Contexts _contexts;
        private IGroup<InputActionEntity> _actions;

        public DestroyInputActionsSystem(Contexts contexts)
        {
            _contexts = contexts;
            _actions = _contexts.inputAction.GetGroup(InputActionMatcher.InputAction);
        }
        
        public void Execute()
        {
            foreach (var action in _actions)
            {
                action.isDestroyed = true;
            }
        }
    }
}
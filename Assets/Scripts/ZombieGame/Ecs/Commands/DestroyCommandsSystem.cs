using Entitas;

namespace ZombieGame.Ecs.Commands
{
    public class DestroyCommandsSystem : IExecuteSystem
    {
        private readonly Contexts _contexts;
        private IGroup<CommandEntity> _commands;

        public DestroyCommandsSystem(Contexts contexts)
        {
            _contexts = contexts;
            _commands = _contexts.command.GetGroup(CommandMatcher.Command);
        }
        
        public void Execute()
        {
            foreach (var command in _commands)
            {
                command.isDestroyed = true;
            }
        }
    }
}
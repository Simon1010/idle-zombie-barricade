using ZombieGame.Ecs.Commands.Collision;
using ZombieGame.Ecs.Commands.PurchasePerk;
using ZombieGame.Ecs.Commands.PurchaseUpgrade;
using ZombieGame.Ecs.Commands.SetGameState;
using ZombieGame.Ecs.Commands.Tap;

namespace ZombieGame.Ecs.Commands
{
    public class CommandFeature : Feature
    {
        public CommandFeature(Contexts contexts)
        {
            Add(new CreateCollisionCommandSystem(contexts));
            Add(new PurchaseUpgradeCommandSystem(contexts));
            Add(new SetGameStateCommandSystem(contexts));
            Add(new TapDamageCommandSystem(contexts));
            Add(new PurchasePerkCommandSystem(contexts));
            
            Add(new DestroyCommandsSystem(contexts));
        }
    }
}
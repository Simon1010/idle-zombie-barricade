using Entitas;

namespace ZombieGame.Ecs.Commands.PurchasePerk
{
    [InputAction]
    public class PurchasePerkInputActionComponent : IComponent
    {
        public CoreEntityId EntityId;
        public int PurchaseAmount;
    }
}
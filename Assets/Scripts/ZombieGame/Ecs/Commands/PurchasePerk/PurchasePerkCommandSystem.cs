using System.Collections.Generic;
using Entitas;

namespace ZombieGame.Ecs.Commands.PurchasePerk
{
    public class PurchasePerkCommandSystem : ReactiveSystem<CommandEntity>
    {
        private readonly Contexts _contexts;

        public PurchasePerkCommandSystem(Contexts contexts) : base(contexts.command)
        {
            _contexts = contexts;
        }

        protected override ICollector<CommandEntity> GetTrigger(IContext<CommandEntity> context)
        {
            return context.CreateCollector(CommandMatcher.PurchasePerkCommand);
        }

        protected override bool Filter(CommandEntity entity)
        {
            return entity.hasPurchasePerkCommand;
        }

        protected override void Execute(List<CommandEntity> entities)
        {
            foreach (var commandEntity in entities)
            {
                var playerMoney = _contexts.core.playerEntity.playerMoney.value;
                playerMoney -= commandEntity.purchasePerkCommand.PurchaseAmount;
                _contexts.core.playerEntity.ReplacePlayerMoney(playerMoney);

                var perk = _contexts.core.GetEntityWithCoreId(commandEntity.purchasePerkCommand.EntityId);
                perk.isPerkOwned = true;
            }
        }
    }
}
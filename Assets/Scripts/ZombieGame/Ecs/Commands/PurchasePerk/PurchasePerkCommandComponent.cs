using Entitas;

namespace ZombieGame.Ecs.Commands.PurchasePerk
{
    [Command]
    public class PurchasePerkCommandComponent : IComponent
    {
        public CoreEntityId EntityId;
        public int PurchaseAmount;
    }
}
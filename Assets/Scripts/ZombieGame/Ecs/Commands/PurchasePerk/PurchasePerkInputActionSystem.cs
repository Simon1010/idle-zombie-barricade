using System.Collections.Generic;
using Entitas;

namespace ZombieGame.Ecs.Commands.PurchasePerk
{
    public class PurchasePerkInputActionSystem : ReactiveSystem<InputActionEntity>
    {
        private readonly Contexts _contexts;

        public PurchasePerkInputActionSystem(Contexts contexts) : base(contexts.inputAction)
        {
            _contexts = contexts;
        }

        protected override ICollector<InputActionEntity> GetTrigger(IContext<InputActionEntity> context)
        {
            return context.CreateCollector(InputActionMatcher.PurchasePerkInputAction);
        }

        protected override bool Filter(InputActionEntity entity)
        {
            return entity.hasPurchasePerkInputAction;
        }

        protected override void Execute(List<InputActionEntity> entities)
        {
            foreach (var inputActionEntity in entities)
            {
                if (_contexts.core.playerEntity.playerMoney.value <
                    inputActionEntity.purchasePerkInputAction.PurchaseAmount)
                {
                    continue;
                }

                var perk = _contexts.core.GetEntityWithCoreId(inputActionEntity.purchasePerkInputAction.EntityId);
                if (perk == null || perk.isPerkOwned)
                {
                    continue;
                }


                GameCommands.CreatePurchasePerk(_contexts, inputActionEntity.purchasePerkInputAction.EntityId,
                    inputActionEntity.purchasePerkInputAction.PurchaseAmount);
            }
        }
    }
}
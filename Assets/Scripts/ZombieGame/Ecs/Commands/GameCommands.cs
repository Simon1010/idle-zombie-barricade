using System.Numerics;

namespace ZombieGame.Ecs.Commands
{
    public static class GameCommands
    {
        private static CommandEntity CreateCommand(Contexts contexts)
        {
            var command = contexts.command.CreateEntity();
            command.isCommand = true;
            return command;
        }

        public static void CreateCommandCreateCollision(Contexts contexts, CoreEntityId entityA, CoreEntityId entityB)
        {
            var command = CreateCommand(contexts);
            command.AddCreateCollisionCommand(entityA, entityB);
        }

        public static void CreateTapOnMap(Contexts contexts, Vector2 position)
        {
            var command = CreateCommand(contexts);
            command.AddTapCommand(position);
        }

        public static void CreateCommandPurchaseUpgrade(Contexts contexts, CoreEntityId entityId, int upgradeCost)
        {
            var command = CreateCommand(contexts);
            command.AddPurchaseUpgradeCommand(entityId, upgradeCost);
        }

        public static void CreateCommandSetGameState(Contexts contexts, GameEnums.GameState gameState)
        {
            var command = CreateCommand(contexts);
            command.AddSetGameStateCommand(gameState);
        }

        public static void CreatePurchasePerk(Contexts contexts, CoreEntityId entityId, int upgradeCost)
        {
            var command = CreateCommand(contexts);
            command.AddPurchasePerkCommand(entityId, upgradeCost);
        }
    }
}
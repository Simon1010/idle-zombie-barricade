using System.Numerics;

namespace ZombieGame.Ecs.Commands
{
    public static class GameInputActions
    {
        private static InputActionEntity CreateInputAction(Contexts contexts)
        {
            var inputAction = contexts.inputAction.CreateEntity();
            inputAction.isInputAction = true;
            return inputAction;
        }

        public static void CreateInputActionCollision(Contexts contexts, CoreEntityId entityA, CoreEntityId entityB)
        {
            var inputAction = CreateInputAction(contexts);
            inputAction.AddCreateCollisionInputAction(entityA, entityB);
        }

        public static void CreateTapDamage(Contexts contexts, Vector2 position)
        {
            var inputAction = CreateInputAction(contexts);
            inputAction.AddTapInputAction(position);
        }

        public static void CreatePurchaseUpgrade(Contexts contexts, CoreEntityId entityId, int upgradeCost)
        {
            var inputAction = CreateInputAction(contexts);
            inputAction.AddPurchaseUpgradeInputAction(entityId, upgradeCost);
        }

        public static void CreateSetGameState(Contexts contexts, GameEnums.GameState state)
        {
            var inputAction = CreateInputAction(contexts);
            inputAction.AddSetGameStateInputAction(state);
        }

        public static void CreatePurchasePerk(Contexts contexts, CoreEntityId entityId, int purchaseAmount)
        {
            var inputAction = CreateInputAction(contexts);
            inputAction.AddPurchasePerkInputAction(entityId, purchaseAmount);
        }
    }
}
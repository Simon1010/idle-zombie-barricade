using Entitas;

namespace ZombieGame.Ecs.Commands.Collision
{
    [Command]
    public class CreateCollisionCommandComponent : IComponent
    {
        public CoreEntityId entityA;
        public CoreEntityId entityB;
    }
}
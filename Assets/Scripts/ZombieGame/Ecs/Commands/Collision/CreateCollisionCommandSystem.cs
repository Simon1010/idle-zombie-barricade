using System.Collections.Generic;
using Entitas;

namespace ZombieGame.Ecs.Commands.Collision
{
    public class CreateCollisionCommandSystem : ReactiveSystem<CommandEntity>
    {
        private readonly Contexts _contexts;

        public CreateCollisionCommandSystem(Contexts contexts) : base(contexts.command)
        {
            _contexts = contexts;
        }

        protected override ICollector<CommandEntity> GetTrigger(IContext<CommandEntity> context)
        {
            return context.CreateCollector(CommandMatcher.CreateCollisionCommand);
        }

        protected override bool Filter(CommandEntity entity)
        {
            return entity.hasCreateCollisionCommand;
        }

        protected override void Execute(List<CommandEntity> entities)
        {
            foreach (var commandEntity in entities)
            {
                GameFactory.CreateCollision(_contexts, commandEntity.createCollisionCommand.entityA,
                    commandEntity.createCollisionCommand.entityB);
            }
        }
    }
}
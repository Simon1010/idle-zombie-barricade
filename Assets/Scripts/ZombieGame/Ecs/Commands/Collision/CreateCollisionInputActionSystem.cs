using System.Collections.Generic;
using Entitas;

namespace ZombieGame.Ecs.Commands.Collision
{
    public class CreateCollisionInputActionSystem : ReactiveSystem<InputActionEntity>
    {
        private readonly Contexts _contexts;

        public CreateCollisionInputActionSystem(Contexts contexts) : base(contexts.inputAction)
        {
            _contexts = contexts;
        }

        protected override ICollector<InputActionEntity> GetTrigger(IContext<InputActionEntity> context)
        {
            return context.CreateCollector(InputActionMatcher.CreateCollisionInputAction);
        }

        protected override bool Filter(InputActionEntity entity)
        {
            return entity.hasCreateCollisionInputAction;
        }

        protected override void Execute(List<InputActionEntity> entities)
        {
            foreach (var inputActionEntity in entities)
            {
                GameCommands.CreateCommandCreateCollision(_contexts,
                    inputActionEntity.createCollisionInputAction.entityA,
                    inputActionEntity.createCollisionInputAction.entityB);
            }
        }
    }
}
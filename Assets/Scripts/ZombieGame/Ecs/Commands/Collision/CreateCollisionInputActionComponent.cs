using Entitas;

namespace ZombieGame.Ecs.Commands.Collision
{
    [InputAction]
    public class CreateCollisionInputActionComponent : IComponent
    {
        public CoreEntityId entityA;
        public CoreEntityId entityB;
    }
}
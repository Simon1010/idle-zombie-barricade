﻿using System;

namespace ZombieGame.Ecs
{
    public struct CoreEntityId : IEquatable<CoreEntityId>
    {
        private int value;

        public CoreEntityId(int value)
        {
            this.value = value;
        }

        public static bool operator !=(CoreEntityId a, CoreEntityId b)
        {
            return a.value != b.value;
        }

        public static bool operator ==(CoreEntityId a, CoreEntityId b)
        {
            return a.value == b.value;
        }

        public bool Equals(CoreEntityId other)
        {
            return other.value == value;
        }

        public override bool Equals(object obj)
        {
            if (obj == null || obj.GetType() != GetType() || obj.GetHashCode() != GetHashCode())
            {
                return false;
            }

            var other = (CoreEntityId) obj;
            return other.value == value;
        }

        public override int GetHashCode()
        {
            return value;
        }

        public static CoreEntityId Empty => new CoreEntityId();

        public override string ToString()
        {
            return "(CoreEntityId:" + value + ")";
        }
        
    }
}
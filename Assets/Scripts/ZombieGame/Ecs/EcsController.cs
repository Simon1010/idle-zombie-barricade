using System;
using Entitas;
using UnityEngine;
using Utilities;
using Utilities.Menu;
using ZombieGame.Ecs.Commands;
using ZombieGame.Ecs.Core;
using ZombieGame.Ecs.View;
using Random = System.Random;
using Vector2 = System.Numerics.Vector2;

namespace ZombieGame.Ecs
{
    public class EcsController
    {
        public Contexts Contexts
        {
            get => _contexts;
        }

        private Contexts _contexts;
        private Systems _systems;

        public EcsController()
        {
            _contexts = Contexts.sharedInstance;
            _contexts.SubscribeId();

            _systems = new Feature()
                    .Add(new InputActionFeature(_contexts))
                    .Add(new CommandFeature(_contexts))
                    .Add(new CoreFeature(_contexts))
                    .Add(new ViewFeature(_contexts))
                ;
        }

        public void Initialize()
        {
            _systems.Initialize();

            var saveGame = GameSaveManager.GetSaveData(_contexts);

            GameFactory.CreateGame(_contexts);
            GameFactory.CreatePlayer(_contexts);
            GameFactory.CreateMap(_contexts);
            GameFactory.CreateTick(_contexts, 0.01f).isGameplayTick = true;

            var map = GameResourceManager.Instance.GetPrefab<Utilities.Map>("Map");
            GameFactory.CreateBarricade(_contexts, new Vector2(0, 300), 100);

            var safehouseEntrance = _contexts.core.CreateEntity();
            safehouseEntrance.isSafehouseEntrance = true;
            safehouseEntrance.ReplacePosition(
                map.SafehousePosition.localPosition.ToVector2());

            var weaponIds = Enum.GetValues(typeof(GameEnums.WeaponId));
            foreach (var weaponId in weaponIds)
            {
                GameFactory.CreateWeapon(_contexts, (GameEnums.WeaponId) weaponId);
            }


            var perks = (GameEnums.PerkId[]) Enum.GetValues(typeof(GameEnums.PerkId));

            foreach (var perk in perks)
            {
                GameFactory.CreatePerk(_contexts, perk);
            }

            if (saveGame != null)
            {
                GameSaveManager.LoadGame(_contexts, saveGame);


                var idleStats =
                    GameIdleIncomeManager.GetIdleStats(_contexts, saveGame.LastLoginDate);

                var idleIncome = _contexts.core.CreateEntity();
                idleIncome.isIdleIncome = true;
                idleIncome.AddIdleIncomeData(idleStats);
            }

            StartGame();
        }


        public void Update()
        {
            _systems.Execute();
            _systems.Cleanup();

            if (UnityEngine.Input.GetKeyUp(UnityEngine.KeyCode.S))
            {
                _contexts.core.enemySpawnTickEntity.isTickDisabled = false;
            }

            if (UnityEngine.Input.GetKeyUp(UnityEngine.KeyCode.E))
            {
                _contexts.core.barricadeEntity.isDestroyed = true;
            }

            if (UnityEngine.Input.GetKeyUp(UnityEngine.KeyCode.A))
            {
                TempCreateAlly();
            }

            if (UnityEngine.Input.GetKeyUp(UnityEngine.KeyCode.V))
            {
                var playerMoney = _contexts.core.playerEntity.playerMoney.value;
                playerMoney += 250000;
                _contexts.core.playerEntity.ReplacePlayerMoney(playerMoney);
            }

            if (UnityEngine.Input.GetKeyUp(UnityEngine.KeyCode.D))
            {
                var random = new Random();
                var randomX = random.Next(-200, 200);
                var enemyValues = Enum.GetValues(typeof(GameEnums.UnitId));
                var randomEnemy = random.Next(1, enemyValues.Length);
                GameFactory.CreateEnemy(_contexts, new Vector2(randomX, -600f), (GameEnums.UnitId) randomEnemy);
            }

            if (UnityEngine.Input.GetKeyUp(KeyCode.R))
            {
            }

            if (UnityEngine.Input.GetKeyUp(KeyCode.H))
            {
                GameSaveManager.SaveGame(_contexts);
            }


            if (UnityEngine.Input.GetKeyUp(KeyCode.J))
            {
                GameSaveManager.LoadGame(_contexts, GameSaveManager.GetSaveData(_contexts));
            }
        }

        private void TempCreateAlly()
        {
            var allies = _contexts.core.GetEntities(CoreMatcher.Ally).Length;
            if (allies < Enum.GetValues(typeof(GameEnums.WeaponId)).Length)
            {
                var weaponId = (GameEnums.WeaponId) allies;
                var barricade = GameResourceManager.Instance.GetPrefab<Barricade>("Barricade");
                var ally = GameFactory.CreateAlly(_contexts,
                    _contexts.core.barricadeEntity.position.value + barricade.GetAllyPositions()[allies],
                    GameEnums.UnitId.Civilian_Grunt_001);

                var weapon = _contexts.core.GetEntityWithWeaponId(weaponId);
                ally.AddAllyWeaponEquipped(weapon.coreId.value);
            }
        }

        private void StartGame()
        {
            var enemySpawnTick = GameFactory.CreateTick(_contexts, 3, 0);
            enemySpawnTick.isEnemySpawnTick = true;
            enemySpawnTick.isTickDisabled = true;

            var autoSaveTick = GameFactory.CreateTick(_contexts, 5, 0);
            autoSaveTick.isAutoSaveTick = true;
        }

        public void TearDown()
        {
            _systems.TearDown();
        }
    }
}
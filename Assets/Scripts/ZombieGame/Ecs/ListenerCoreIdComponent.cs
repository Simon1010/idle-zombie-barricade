﻿using Entitas;

namespace ZombieGame.Ecs
{
    [Listener]
    public class ListenerCoreIdComponent : IComponent
    {
        public CoreEntityId value;
    }
}

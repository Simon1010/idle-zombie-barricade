using ZombieGame.Ecs.Core.Game.GameOver;
using ZombieGame.Ecs.Core.Game.Save;
using ZombieGame.Ecs.Core.IdleIncome;

namespace ZombieGame.Ecs.Core.Game
{
    public class GameFeature : Feature
    {
        public GameFeature(Contexts contexts)
        {
            Add(new UpdateGameTotalKillsSystem(contexts));
            Add(new SaveFeature(contexts));
            Add(new GameOverFeature(contexts));
            Add(new ShowIdleIncomeMenuSystem(contexts));
        }
    }
}
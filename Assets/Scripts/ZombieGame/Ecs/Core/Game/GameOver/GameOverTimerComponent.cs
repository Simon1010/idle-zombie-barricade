using Entitas;
using IdleMineGenerator.CodeGeneration.Attributes;

namespace ZombieGame.Ecs.Core.Game.GameOver
{
    [Core, ListenerAny]
    public class GameOverTimerComponent : IComponent
    {
        public float value;
    }
}
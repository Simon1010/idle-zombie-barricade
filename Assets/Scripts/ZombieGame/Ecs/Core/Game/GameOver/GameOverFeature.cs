namespace ZombieGame.Ecs.Core.Game.GameOver
{
    public class GameOverFeature : Feature
    {
        public GameOverFeature(Contexts contexts)
        {
            Add(new CountdownGameOverTimerSystem(contexts));
            Add(new GameOverSystem(contexts));
        }
    }
}
using Entitas;
using UnityEngine;

namespace ZombieGame.Ecs.Core.Game.GameOver
{
    public class CountdownGameOverTimerSystem : IExecuteSystem
    {
        private readonly Contexts _contexts;
        private IGroup<CoreEntity> _group;

        public CountdownGameOverTimerSystem(Contexts contexts)
        {
            _contexts = contexts;
            _group = _contexts.core.GetGroup(CoreMatcher.AllOf(CoreMatcher.GameOver, CoreMatcher.GameOverTimer));
        }

        public void Execute()
        {
            foreach (var coreEntity in _group.GetEntities())
            {
                var timer = coreEntity.gameOverTimer.value;
                timer -= Time.deltaTime;
                coreEntity.ReplaceGameOverTimer(timer);
            }
        }
    }
}
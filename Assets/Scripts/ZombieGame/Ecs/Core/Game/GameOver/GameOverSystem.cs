using System;
using System.Collections.Generic;
using Entitas;

namespace ZombieGame.Ecs.Core.Game
{
    public class GameOverSystem : ReactiveSystem<CoreEntity>
    {
        private readonly Contexts _contexts;

        public GameOverSystem(Contexts contexts) : base(contexts.core)
        {
            _contexts = contexts;
        }

        protected override ICollector<CoreEntity> GetTrigger(IContext<CoreEntity> context)
        {
            return context.CreateCollector(CoreMatcher.AllOf(CoreMatcher.Dead, CoreMatcher.Barricade));
        }

        protected override bool Filter(CoreEntity entity)
        {
            return entity.isBarricade && entity.isDead;
        }

        protected override void Execute(List<CoreEntity> entities)
        {
            foreach (var coreEntity in entities)
            {
                if (_contexts.core.gameEntity.isGameOver == false)
                {
                    _contexts.core.gameEntity.isGameOver = true;
                    _contexts.core.gameEntity.AddGameOverDate(DateTime.Now);
                    _contexts.core.gameEntity.AddGameOverTimer(20);
                }
            }
        }
    }
}
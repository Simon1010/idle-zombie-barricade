using Entitas;
using IdleMineGenerator.CodeGeneration.Attributes;

namespace ZombieGame.Ecs.Core.IdleIncome
{
    [Core, ListenerAny]
    public class IdleIncomeDataComponent : IComponent
    {
        public GameIdleIncomeManager.IdleStats value;
    }
}
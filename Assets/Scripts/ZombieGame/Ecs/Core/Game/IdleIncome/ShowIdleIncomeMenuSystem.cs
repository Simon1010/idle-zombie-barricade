using Entitas;
using Utilities.Menu;

namespace ZombieGame.Ecs.Core.IdleIncome
{
    public class ShowIdleIncomeMenuSystem : IExecuteSystem
    {
        private readonly Contexts _contexts;
        private IGroup<CoreEntity> _group;

        public ShowIdleIncomeMenuSystem(Contexts contexts)
        {
            _contexts = contexts;
            _group = _contexts.core.GetGroup(CoreMatcher.IdleIncome);
        }

        public void Execute()
        {
            if (_contexts.core.gameEntity.isGameOver)
            {
                return;
            }

            foreach (var coreEntity in _group.GetEntities())
            {
                var idleStats = coreEntity.idleIncomeData.value;
                _contexts.core.playerEntity.ReplacePlayerMoney(
                    _contexts.core.playerEntity.playerMoney.value + idleStats.IncomeProduced);
                _contexts.core.barricadeEntity.ReplaceCurrentHealth(
                    _contexts.core.barricadeEntity.currentHealth.value - idleStats.DamageTaken);
                _contexts.core.gameEntity.ReplaceGameTotalKills(
                    _contexts.core.gameEntity.gameTotalKills.value + idleStats.EnemiesKilled);

                _contexts.core.gameEntity.ReplaceGameCurrentDay(
                    _contexts.core.gameEntity.gameCurrentDay.value + idleStats.DaysPassed);

                GameMenuManager.Instance.CreateMenu(GameEnums.MenuId.IdleIncome);

                coreEntity.isDestroyed = true;
            }
        }
    }
}
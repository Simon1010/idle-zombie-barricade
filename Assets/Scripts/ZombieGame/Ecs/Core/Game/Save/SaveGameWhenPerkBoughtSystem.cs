using System.Collections.Generic;
using Entitas;

namespace ZombieGame.Ecs.Core.Game.Save
{
    public class SaveGameWhenPerkBoughtSystem : ReactiveSystem<CoreEntity>
    {
        private readonly Contexts _contexts;

        public SaveGameWhenPerkBoughtSystem(Contexts contexts) : base(contexts.core)
        {
            _contexts = contexts;
        }

        protected override ICollector<CoreEntity> GetTrigger(IContext<CoreEntity> context)
        {
            return context.CreateCollector(CoreMatcher.AllOf(CoreMatcher.Perk, CoreMatcher.PerkOwned));
        }

        protected override bool Filter(CoreEntity entity)
        {
            return entity.isPerkOwned;
        }

        protected override void Execute(List<CoreEntity> entities)
        {
            foreach (var coreEntity in entities)
            {
                GameSaveManager.SaveGame(_contexts);
            }
        }
    }
}
namespace ZombieGame.Ecs.Core.Game.Save
{
    public class SaveFeature : Feature
    {
        public SaveFeature(Contexts contexts)
        {
            Add(new AutoSaveSystem(contexts));    
            Add(new SaveGameWhenPerkBoughtSystem(contexts));    
            Add(new SaveGameWhenWeaponUpgradedSystem(contexts));    
        }
    }
}
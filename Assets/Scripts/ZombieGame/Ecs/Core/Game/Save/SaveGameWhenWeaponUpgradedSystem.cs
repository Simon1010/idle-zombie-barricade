using System.Collections.Generic;
using Entitas;
using UnityEngine;

namespace ZombieGame.Ecs.Core.Game.Save
{
    public class SaveGameWhenWeaponUpgradedSystem : ReactiveSystem<CoreEntity>
    {
        private readonly Contexts _contexts;

        public SaveGameWhenWeaponUpgradedSystem(Contexts contexts) : base(contexts.core)
        {
            _contexts = contexts;
        }

        protected override ICollector<CoreEntity> GetTrigger(IContext<CoreEntity> context)
        {
            return context.CreateCollector(CoreMatcher.AllOf(CoreMatcher.Weapon, CoreMatcher.Level));
        }

        protected override bool Filter(CoreEntity entity)
        {
            return entity.isWeapon;
        }

        protected override void Execute(List<CoreEntity> entities)
        {
            foreach (var coreEntity in entities)
            {
                GameSaveManager.SaveGame(_contexts);
            }
        }
    }
}
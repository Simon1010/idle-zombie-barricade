using Entitas;
using Entitas.CodeGeneration.Attributes;

namespace ZombieGame.Ecs.Core.Game.Modifiers
{
    [Core]
    public class ModifierStatIdComponent : IComponent
    {
        [EntityIndex] public GameEnums.ModifierStatId value;
    }
}
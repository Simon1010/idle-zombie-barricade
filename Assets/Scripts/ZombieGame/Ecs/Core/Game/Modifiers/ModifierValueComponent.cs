using Entitas;

namespace ZombieGame.Ecs.Core.Game.Modifiers
{
    [Core]
    public class ModifierValueComponent : IComponent
    {
        public float value;
    }
}
using Entitas;
using Entitas.CodeGeneration.Attributes;

namespace ZombieGame.Ecs.Core.Game.Modifiers
{
    [Core]
    public class ModifierTargetEntityTypeComponent : IComponent
    {
        [EntityIndex] public GameEnums.ModifierTargetEntityType value;
    }
}
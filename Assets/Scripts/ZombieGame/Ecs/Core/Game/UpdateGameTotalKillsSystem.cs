using System.Collections.Generic;
using Entitas;

namespace ZombieGame.Ecs.Core.Game
{
    public class UpdateGameTotalKillsSystem : ReactiveSystem<CoreEntity>
    {
        private readonly Contexts _contexts;

        public UpdateGameTotalKillsSystem(Contexts contexts) : base(contexts.core)
        {
            _contexts = contexts;
        }

        protected override ICollector<CoreEntity> GetTrigger(IContext<CoreEntity> context)
        {
            return context.CreateCollector(CoreMatcher.AllOf(CoreMatcher.Dead, CoreMatcher.Enemy));
        }

        protected override bool Filter(CoreEntity entity)
        {
            return entity.isDead && entity.isEnemy;
        }

        protected override void Execute(List<CoreEntity> entities)
        {
            foreach (var coreEntity in entities)
            {
                _contexts.core.gameEntity.ReplaceGameTotalKills(++_contexts.core.gameEntity.gameTotalKills.value);
            }
        }
    }
}
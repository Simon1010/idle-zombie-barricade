using Entitas;
using IdleMineGenerator.CodeGeneration.Attributes;

namespace ZombieGame.Ecs.Core.Game
{
    [Core, ListenerAny]
    public class GameTotalKillsComponent : IComponent
    {
        public int value;
    }
}
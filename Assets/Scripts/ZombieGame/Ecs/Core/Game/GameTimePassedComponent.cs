using Entitas;

namespace ZombieGame.Ecs.Core.Game
{
    [Core]
    public class GameTimePassedComponent : IComponent
    {
        public long value;
    }
}
using Entitas;
using IdleMineGenerator.CodeGeneration.Attributes;

namespace ZombieGame.Ecs.Core
{
    [Core, ListenerSelf]
    public class LevelComponent : IComponent
    {
        public int value;
    }
}
using Entitas;

namespace ZombieGame.Ecs.Core.Tick
{
    [Core]
    public class TickCurrentTimeComponent : IComponent
    {
        public float value;
    }
}
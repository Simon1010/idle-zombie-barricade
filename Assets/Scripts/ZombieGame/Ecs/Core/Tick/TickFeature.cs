namespace ZombieGame.Ecs.Core.Tick
{
    public class TickFeature : Feature
    {
        public TickFeature(Contexts contexts)
        {
            Add(new ResetTickSystem(contexts));
            Add(new CountdownTickSystem(contexts));
        }
    }
}
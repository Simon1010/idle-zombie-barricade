using Entitas;

namespace ZombieGame.Ecs.Core.Tick
{
    public class CountdownTickSystem : IExecuteSystem
    {
        private readonly Contexts _contexts;
        private IGroup<CoreEntity> _ticks;

        public CountdownTickSystem(Contexts contexts)
        {
            _contexts = contexts;
            _ticks = _contexts.core.GetGroup(CoreMatcher.AllOf(CoreMatcher.Tick).NoneOf(CoreMatcher.TickDisabled));
        }

        public void Execute()
        {
            foreach (var coreEntity in _ticks.GetEntities())
            {
                var current = coreEntity.tickCurrentTime.value;
                current -= UnityEngine.Time.deltaTime;
                coreEntity.ReplaceTickCurrentTime(current);

                if (current <= 0)
                {
                    coreEntity.isTickComplete = true;
                }
            }
        }
    }
}
using Entitas;

namespace ZombieGame.Ecs.Core.Tick
{
    [Core]
    public class TickTotalTimeComponent : IComponent
    {
        public float value;
    }
}
using System.Collections.Generic;
using Entitas;

namespace ZombieGame.Ecs.Core.Collision
{
    public class RemoveCollisionSystem : ReactiveSystem<CoreEntity>
    {
        private readonly Contexts _contexts;

        public RemoveCollisionSystem(Contexts contexts) : base(contexts.core)
        {
            _contexts = contexts;
        }

        protected override ICollector<CoreEntity> GetTrigger(IContext<CoreEntity> context)
        {
            return context.CreateCollector(CoreMatcher.Collision);
        }

        protected override bool Filter(CoreEntity entity)
        {
            return entity.hasCollision;
        }

        protected override void Execute(List<CoreEntity> entities)
        {
            foreach (var coreEntity in entities)
            {
                coreEntity.isDestroyed = true;
            }
        }
    }
}
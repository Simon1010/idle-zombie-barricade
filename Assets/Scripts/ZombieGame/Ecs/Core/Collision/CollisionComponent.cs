using Entitas;

namespace ZombieGame.Ecs.Core.Collision
{
    [Core]
    public class CollisionComponent : IComponent
    {
        public CoreEntityId entityA;
        public CoreEntityId entityB;
    }
}
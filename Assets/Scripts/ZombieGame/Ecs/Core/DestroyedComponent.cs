using Entitas;
using IdleMineGenerator.CodeGeneration.Attributes;

namespace Ecs.Core
{
    [Core, Command, InputAction, ListenerAny, ListenerSelf]
    public class DestroyedComponent : IComponent
    {
    }
}
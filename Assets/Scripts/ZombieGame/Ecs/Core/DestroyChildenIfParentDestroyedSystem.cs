using Entitas;

namespace ZombieGame.Ecs.Core
{
    public class DestroyChildenIfParentDestroyedSystem : IInitializeSystem, ITearDownSystem
    {
        private readonly Contexts _contexts;
        private readonly IGroup<CoreEntity> _group;

        public DestroyChildenIfParentDestroyedSystem(Contexts contexts)
        {
            _contexts = contexts;
            _group = this._contexts.core.GetGroup(CoreMatcher.Destroyed);
        }

        public void Initialize()
        {
            _group.OnEntityAdded += FlagDestroyDependency;
        }

        public void TearDown()
        {
            _group.OnEntityAdded -= FlagDestroyDependency;
        }

        private void FlagDestroyDependency(IGroup<CoreEntity> group, CoreEntity entity, int index, IComponent component)
        {
            var dependencies = _contexts.core.GetEntitiesWithParent(entity.coreId.value);
            foreach (var dependency in dependencies)
            {
                dependency.isDestroyed = true;
            }
        }
    }
}
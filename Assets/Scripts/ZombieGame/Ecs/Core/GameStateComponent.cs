using Entitas;

namespace ZombieGame.Ecs.Core
{
    [Core]
    public class GameStateComponent : IComponent
    {
        public GameEnums.GameState value;
    }
}
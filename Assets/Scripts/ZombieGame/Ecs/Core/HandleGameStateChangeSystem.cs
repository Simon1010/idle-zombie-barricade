using System;
using System.Collections.Generic;
using Entitas;
using UnityEngine;

namespace ZombieGame.Ecs.Core
{
    public class HandleGameStateChangeSystem : ReactiveSystem<CoreEntity>
    {
        private readonly Contexts _contexts;

        public HandleGameStateChangeSystem(Contexts contexts) : base(contexts.core)
        {
            _contexts = contexts;
        }

        protected override ICollector<CoreEntity> GetTrigger(IContext<CoreEntity> context)
        {
            return context.CreateCollector(CoreMatcher.GameState);
        }

        protected override bool Filter(CoreEntity entity)
        {
            return entity.hasGameState;
        }

        protected override void Execute(List<CoreEntity> entities)
        {
            foreach (var coreEntity in entities)
            {
                switch (coreEntity.gameState.value)
                {
                    case GameEnums.GameState.Menu:
                        DisableGameplay();
                        break;
                    case GameEnums.GameState.MainMenu:
                        DisableGameplay();
                        break;
                    case GameEnums.GameState.Gameplay:
                        EnableGameplay();
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        private void DisableGameplay()
        {
            _contexts.core.gameplayTickEntity.isTickDisabled = true;
            _contexts.core.enemySpawnTickEntity.isTickDisabled = true;
        }

        private void EnableGameplay()
        {
            _contexts.core.gameplayTickEntity.isTickDisabled = false;
            _contexts.core.enemySpawnTickEntity.isTickDisabled = false;
        }
    }
}
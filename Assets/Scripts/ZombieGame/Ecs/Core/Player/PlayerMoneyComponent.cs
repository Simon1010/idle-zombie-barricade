using Entitas;
using IdleMineGenerator.CodeGeneration.Attributes;

namespace ZombieGame.Ecs.Core.Player
{
    [Core, ListenerSelf, ListenerAny]
    public class PlayerMoneyComponent : IComponent
    {
        public int value;
    }
}
using Entitas;
using Entitas.CodeGeneration.Attributes;
using IdleMineGenerator.CodeGeneration.Attributes;

namespace ZombieGame.Ecs.Core.Player
{
    [Core, Unique, ListenerAny]
    public class PlayerComponent : IComponent
    {
    }
}
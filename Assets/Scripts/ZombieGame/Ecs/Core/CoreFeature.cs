using ZombieGame.Ecs.Core.Collision;
using ZombieGame.Ecs.Core.Combat;
using ZombieGame.Ecs.Core.Game;
using ZombieGame.Ecs.Core.Gameplay;
using ZombieGame.Ecs.Core.Map;
using ZombieGame.Ecs.Core.Movement;
using ZombieGame.Ecs.Core.Movement.MimicPosition;
using ZombieGame.Ecs.Core.Perk;
using ZombieGame.Ecs.Core.Spawning;
using ZombieGame.Ecs.Core.Tick;
using ZombieGame.Ecs.Core.Units;

namespace ZombieGame.Ecs.Core
{
    public class CoreFeature : Feature
    {
        public CoreFeature(Contexts contexts)
        {
            Add(new HandleGameStateChangeSystem(contexts));
            Add(new GameFeature(contexts));
            Add(new PerkFeature(contexts));
            Add(new TickFeature(contexts));
            Add(new MapFeature(contexts));
            Add(new UnitFeature(contexts));
            Add(new SpawningFeature(contexts));
            Add(new CombatFeature(contexts));
            Add(new MimicPositionSystem(contexts));

            Add(new VelocityFeature(contexts));
            Add(new MovementFeature(contexts));
            Add(new RemoveCollisionSystem(contexts));

            Add(new DestroyChildenIfParentDestroyedSystem(contexts));


            Add(new ListenersEventSystem(contexts));

            Add(new DestroyEntitiesSystem(contexts));
        }
    }
}
using System.Collections.Generic;
using Entitas;

namespace ZombieGame.Ecs.Core
{
    public class DestroyEntitiesSystem : MultiReactiveSystem<IDestroyEntity, Contexts>
    {
        private readonly Contexts _contexts;

        public DestroyEntitiesSystem(Contexts contexts) : base(contexts)
        {
            _contexts = contexts;
        }

        protected override ICollector[] GetTrigger(Contexts contexts)
        {
            return new ICollector[]
            {
                contexts.command.CreateCollector(CommandMatcher.Destroyed),
                contexts.inputAction.CreateCollector(InputActionMatcher.Destroyed),
                contexts.core.CreateCollector(CoreMatcher.Destroyed),
            };
        }

        protected override bool Filter(IDestroyEntity entity)
        {
            return entity.isDestroyed;
        }

        protected override void Execute(List<IDestroyEntity> entities)
        {
            foreach (var entity in entities)
            {
                entity.Destroy();
            }
        }
    }

}


public interface IDestroyEntity : IEntity, IDestroyedEntity
{
}

public partial class CoreEntity : IDestroyEntity
{
}

public partial class InputActionEntity : IDestroyEntity
{
}

public partial class CommandEntity : IDestroyEntity
{
}
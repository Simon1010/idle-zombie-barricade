using Entitas;

namespace ZombieGame.Ecs.Core.Weapon
{
    [Core]
    public class WeaponReloadTimeComponent : IComponent
    {
        public float value;
    }
}
using Entitas;
using Entitas.CodeGeneration.Attributes;
using IdleMineGenerator.CodeGeneration.Attributes;

namespace ZombieGame.Ecs.Core.Weapon
{
    [Core, ListenerSelf]
    public class WeaponIdComponent : IComponent
    {
        [PrimaryEntityIndex] public GameEnums.WeaponId value;
    }
}
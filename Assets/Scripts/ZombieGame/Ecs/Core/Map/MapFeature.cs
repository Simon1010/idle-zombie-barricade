namespace ZombieGame.Ecs.Core.Map
{
    public class MapFeature : Feature
    {
        public MapFeature(Contexts contexts)
        {
            Add(new SetupMapSystem(contexts));
        }
    }
}
using Entitas;
using Entitas.CodeGeneration.Attributes;
using IdleMineGenerator.CodeGeneration.Attributes;

namespace ZombieGame.Ecs.Core.Map.Barricade
{
    [Core, ListenerAny, Unique]
    public class BarricadeComponent : IComponent
    {
    }
}
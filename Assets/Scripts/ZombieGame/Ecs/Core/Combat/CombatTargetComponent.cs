using Entitas;
using Entitas.CodeGeneration.Attributes;

namespace ZombieGame.Ecs.Core.Combat
{
    [Core]
    public class AttackTargetComponent : IComponent
    {
        [EntityIndex] public CoreEntityId value;
    }
}
using ZombieGame.Ecs.Core.Combat.ApplyDamage;
using ZombieGame.Ecs.Core.Combat.AwardMoneyOnDeath;
using ZombieGame.Ecs.Core.Combat.Bullet;
using ZombieGame.Ecs.Core.Combat.DealAOEDamageOnDeath;
using ZombieGame.Ecs.Core.Combat.Health;
using ZombieGame.Ecs.Core.Combat.Tap;

namespace ZombieGame.Ecs.Core.Combat
{
    public class CombatFeature : Feature
    {
        public CombatFeature(Contexts contexts)
        {
            Add(new TapFeature((contexts)));
            Add(new BulletFeature(contexts));
            Add(new DamageFeature(contexts));
            Add(new HealthFeature(contexts));

            Add(new AwardMoneyOnDeathSystem(contexts));
            Add(new DealAOEDamageOnDeathSystem(contexts));
        }
    }
}
using System.Collections.Generic;
using Entitas;
using ZombieGame.Data;

namespace ZombieGame.Ecs.Core.Combat.DealAOEDamageOnDeath
{
    public class DealAOEDamageOnDeathSystem : ReactiveSystem<CoreEntity>
    {
        private readonly Contexts _contexts;

        public DealAOEDamageOnDeathSystem(Contexts contexts) : base(contexts.core)
        {
            _contexts = contexts;
        }

        protected override ICollector<CoreEntity> GetTrigger(IContext<CoreEntity> context)
        {
            return context.CreateCollector(CoreMatcher.AllOf(CoreMatcher.Dead, CoreMatcher.DealAOEDamageOnDeath));
        }

        protected override bool Filter(CoreEntity entity)
        {
            return entity.isDead && entity.isDealAOEDamageOnDeath;
        }

        protected override void Execute(List<CoreEntity> entities)
        {
            foreach (var coreEntity in entities)
            {
                var enemyData = GameData.GetEnemyData(coreEntity.unitId.value);
                var damage = GameCalculations.Calculate(enemyData.Damage, coreEntity);
                var explosionRange = GameCalculations.Calculate(enemyData.DeathAOERange, coreEntity);

                GameFactory.CreateApplyDamage(_contexts, coreEntity.coreId.value, coreEntity.position.value,
                    explosionRange,
                    damage);
            }
        }
    }
}
namespace ZombieGame.Ecs.Core.Combat.ApplyDamage
{
    public class DamageFeature : Feature
    {
        public DamageFeature(Contexts contexts)
        {
            Add(new ApplyDamageSystem(contexts));
        }
    }
}
using Entitas;

namespace ZombieGame.Ecs.Core.Combat.ApplyDamage
{
    [Core]
    public class ApplyDamageTargetComponent : IComponent
    {
        public CoreEntityId value;
    }
}
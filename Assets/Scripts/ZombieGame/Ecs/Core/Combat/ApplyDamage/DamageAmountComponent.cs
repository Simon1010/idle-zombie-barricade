using Entitas;

namespace ZombieGame.Ecs.Core.Combat.ApplyDamage
{
    [Core]
    public class DamageAmountComponent : IComponent
    {
        public float value;
    }
}
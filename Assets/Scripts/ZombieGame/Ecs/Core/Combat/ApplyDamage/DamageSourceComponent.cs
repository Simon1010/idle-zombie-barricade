using Entitas;

namespace ZombieGame.Ecs.Core.Combat.ApplyDamage
{
    [Core]
    public class DamageSourceComponent : IComponent
    {
        public CoreEntityId value;
    }
}
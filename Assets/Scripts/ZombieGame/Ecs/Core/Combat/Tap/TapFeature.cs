namespace ZombieGame.Ecs.Core.Combat.Tap
{
    public class TapFeature : Feature
    {
        public TapFeature(Contexts contexts)
        {
            Add(new DestroyLingeringTapsSystem(contexts));
            Add(new HandleTapDamageSystem(contexts));
        }
    }
}
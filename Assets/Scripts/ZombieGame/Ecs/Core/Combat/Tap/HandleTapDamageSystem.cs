using System.Collections.Generic;
using System.Linq;
using Entitas;
using UnityEngine;
using Vector2 = System.Numerics.Vector2;

namespace ZombieGame.Ecs.Core.Combat.Tap
{
    public class HandleTapDamageSystem : ReactiveSystem<CoreEntity>
    {
        private readonly Contexts _contexts;

        public HandleTapDamageSystem(Contexts contexts) : base(contexts.core)
        {
            _contexts = contexts;
        }

        protected override ICollector<CoreEntity> GetTrigger(IContext<CoreEntity> context)
        {
            return context.CreateCollector(CoreMatcher.Tap);
        }

        protected override bool Filter(CoreEntity entity)
        {
            return entity.isTap;
        }

        protected override void Execute(List<CoreEntity> entities)
        {
            if (_contexts.core.gameEntity.gameState.value != GameEnums.GameState.Gameplay)
            {
                return;
            }

            if (_contexts.core.gameEntity.isGameOver)
            {
                return;
            }

            foreach (var coreEntity in entities)
            {
                var position = coreEntity.position.value;
                var playerData = GameData.GetPlayerData();
                var tapRange = GameCalculations.Calculate(playerData.TapRange, _contexts.core.playerEntity);
                var tapDamage = GameCalculations.Calculate(playerData.TapDamage, _contexts.core.playerEntity);

                var closestEnemies = _contexts.core
                    .GetEntities(CoreMatcher.AllOf(CoreMatcher.Enemy).NoneOf(CoreMatcher.Dead))
                    .Where(e => Vector2.Distance(e.position.value, position) <= tapRange).ToArray();

                if (closestEnemies.Any())
                {
                    var closestEnemy = closestEnemies.OrderBy(e => Vector2.Distance(e.position.value, position))
                        .FirstOrDefault();
                    if (closestEnemy != null)
                    {
                        GameFactory.CreateApplyDamage(_contexts, _contexts.core.playerEntity.coreId.value,
                            closestEnemy.coreId.value, tapDamage);
                    }
                }
            }
        }
    }
}
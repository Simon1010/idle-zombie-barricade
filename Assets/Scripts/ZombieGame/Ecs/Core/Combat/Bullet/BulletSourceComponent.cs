using Entitas;

namespace ZombieGame.Ecs.Core.Combat.Bullet
{
    [Core]
    public class BulletSourceComponent : IComponent
    {
        public CoreEntityId value;
    }
}
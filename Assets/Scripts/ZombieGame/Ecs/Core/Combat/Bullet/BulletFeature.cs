namespace ZombieGame.Ecs.Core.Combat.Bullet
{
    public class BulletFeature : Feature
    {
        public BulletFeature(Contexts contexts)
        {
            Add(new HandleBulletCollisionSystem(contexts));
        }
    }
}
using System.Collections.Generic;
using Entitas;
using ZombieGame.Data;

namespace ZombieGame.Ecs.Core.Combat.Bullet
{
    public class HandleBulletCollisionSystem : ReactiveSystem<CoreEntity>
    {
        private readonly Contexts _contexts;

        public HandleBulletCollisionSystem(Contexts contexts) : base(contexts.core)
        {
            _contexts = contexts;
        }

        protected override ICollector<CoreEntity> GetTrigger(IContext<CoreEntity> context)
        {
            return context.CreateCollector(CoreMatcher.Collision);
        }

        protected override bool Filter(CoreEntity entity)
        {
            return entity.hasCollision;
        }

        protected override void Execute(List<CoreEntity> entities)
        {
            foreach (var coreEntity in entities)
            {
                CoreEntity bullet = null;
                CoreEntity enemy = null;

                coreEntity.collision.GetCollisionEntities(_contexts, CoreComponentsLookup.Bullet,
                    CoreComponentsLookup.Enemy, out bullet, out enemy);

                if (bullet != null && enemy != null)
                {
                    if (enemy.isDead == false)
                    {
                        var bulletSource = _contexts.core.GetEntityWithCoreId(bullet.bulletSource.value);
                        var weaponEntity = _contexts.core.GetEntityWithCoreId(bulletSource.allyWeaponEquipped.value);
                        if (weaponEntity.weaponId.value == GameEnums.WeaponId.Bazooka)
                        {
                            var weaponData = GameData.GetWeaponData<BazookaData>(GameEnums.WeaponId.Bazooka);
                            var explosionRange = GameCalculations.Calculate(weaponData.ExplosionRange, weaponEntity);
                            GameFactory.CreateApplyDamage(_contexts, bullet.coreId.value, bullet.position.value,
                                explosionRange,
                                bullet.bulletDamage.value);
                        }
                        else
                        {
                            GameFactory.CreateApplyDamage(_contexts, bullet.coreId.value, enemy.coreId.value,
                                bullet.bulletDamage.value);
                        }

                        bullet.isDestroyed = true;
                    }
                }
            }
        }
    }
}
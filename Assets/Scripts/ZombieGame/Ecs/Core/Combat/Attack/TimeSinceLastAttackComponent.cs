using Entitas;

namespace ZombieGame.Ecs.Core.Combat.Attack
{
    [Core]
    public class TimeSinceLastAttackComponent : IComponent
    {
        public int value;
    }
}
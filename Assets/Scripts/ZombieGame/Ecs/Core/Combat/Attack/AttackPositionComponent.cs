using System.Numerics;
using Entitas;

namespace ZombieGame.Ecs.Core.Combat.Attack
{
    [Core]
    public class AttackPositionComponent : IComponent
    {
        public Vector2 value;
    }
}
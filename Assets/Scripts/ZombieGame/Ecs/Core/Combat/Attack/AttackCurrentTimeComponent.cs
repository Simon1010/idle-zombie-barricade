using Entitas;

namespace ZombieGame.Ecs.Core.Combat.Attack
{
    [Core]
    public class AttackCurrentTimeComponent : IComponent
    {
        public int value;
    }
}
using Entitas;

namespace ZombieGame.Ecs.Core.Combat.Attack
{
    [Core]
    public class AttackTotalTimeComponent : IComponent
    {
        public int value;
    }
}
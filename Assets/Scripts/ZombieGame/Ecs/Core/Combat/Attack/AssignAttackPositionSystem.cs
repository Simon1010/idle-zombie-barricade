using System.Collections.Generic;
using Entitas;

namespace ZombieGame.Ecs.Core.Combat.Attack
{
    public class AssignAttackPositionSystem : ReactiveSystem<CoreEntity>
    {
        private readonly Contexts _contexts;

        public AssignAttackPositionSystem(Contexts contexts) : base(contexts.core)
        {
            _contexts = contexts;
        }

        protected override ICollector<CoreEntity> GetTrigger(IContext<CoreEntity> context)
        {
            return context.CreateCollector(CoreMatcher.AttackTarget);
        }

        protected override bool Filter(CoreEntity entity)
        {
            return entity.hasAttackTarget;
        }

        protected override void Execute(List<CoreEntity> entities)
        {
            foreach (var coreEntity in entities)
            {
                continue;
                
                var attackTarget = _contexts.core.GetEntityWithCoreId(coreEntity.attackTarget.value);
                var assignNewPosition = false;

                //Targeting each other
                if (attackTarget.hasAttackTarget && attackTarget.attackTarget.value == coreEntity.coreId.value)
                {
                    if (coreEntity.hasAttackPosition && attackTarget.hasAttackPosition)
                    {
                        assignNewPosition = coreEntity.attackPosition.value != attackTarget.attackPosition.value;
                    }
                    else
                    {
                        assignNewPosition = true;
                    }

                    if (assignNewPosition)
                    {
                        var distance = attackTarget.position.value - coreEntity.position.value;
                        var halfDistance = distance / 2f;
                        var attackPosition = coreEntity.position.value + halfDistance;

                        if (attackTarget.isEnemy)
                        {
                            attackTarget.ReplaceAttackPosition(attackPosition + GameSettings.EnemyAttackOffset);
                            coreEntity.ReplaceAttackPosition(attackPosition + GameSettings.AllyAttackOffset);
                        }
                        else
                        {
                            attackTarget.ReplaceAttackPosition(attackPosition + GameSettings.AllyAttackOffset);
                            coreEntity.ReplaceAttackPosition(attackPosition + GameSettings.EnemyAttackOffset);
                        }
                    }
                }
            }
        }
    }
}
using ZombieGame.Ecs.Core.Combat.Attack.PerformAttacks;

namespace ZombieGame.Ecs.Core.Combat.Attack
{
    public class AttackFeature : Feature
    {
        public AttackFeature(Contexts contexts)
        {
            Add(new FinishAttackSystem(contexts));
            Add(new AssignAttackPositionSystem(contexts));
            Add(new CancelAttackIfTargetRemovedSystem(contexts));
            Add(new ProgressAttackTimeSystem(contexts));
            Add(new PerformAttacksFeature(contexts));
            Add(new ProgressTimeSinceLastAttackSystem(contexts));
        }
    }
}
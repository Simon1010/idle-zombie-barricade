using Entitas;
using IdleMineGenerator.CodeGeneration.Attributes;

namespace ZombieGame.Ecs.Core.Combat.Attack
{
    [Core, ListenerSelf]
    public class AttackIdComponent : IComponent
    {
        public GameEnums.AttackId value;
    }
}
using System.Collections.Generic;
using Entitas;

namespace ZombieGame.Ecs.Core.Combat.Attack.PerformAttacks
{
    public class SetCanPerformAttackSystem : ReactiveSystem<CoreEntity>, ICleanupSystem
    {
        private readonly Contexts _contexts;
        private IGroup<CoreEntity> _group;

        public SetCanPerformAttackSystem(Contexts contexts) : base(contexts.core)
        {
            _contexts = contexts;
            _group = contexts.core.GetGroup(CoreMatcher.CanPerformAttack);
        }

        protected override ICollector<CoreEntity> GetTrigger(IContext<CoreEntity> context)
        {
            return context.CreateCollector(CoreMatcher.AttackCurrentTime);
        }

        protected override bool Filter(CoreEntity entity)
        {
            return entity.hasAttackCurrentTime;
        }

        protected override void Execute(List<CoreEntity> entities)
        {
            foreach (var coreEntity in entities)
            {
                var attackData = GameData.GetAttackData(coreEntity.attackId.value);

                if (attackData.IsFrame(coreEntity.attackCurrentTime.value))
                {
                    coreEntity.isCanPerformAttack = true;
                }
            }
        }

        public void Cleanup()
        {
            foreach (var coreEntity in _group.GetEntities())
            {
                coreEntity.isCanPerformAttack = false;
            }
        }
    }
}
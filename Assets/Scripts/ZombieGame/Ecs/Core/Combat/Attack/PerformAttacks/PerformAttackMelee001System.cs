using System.Collections.Generic;
using Entitas;
using ZombieGame.Data;

namespace ZombieGame.Ecs.Core.Combat.Attack.PerformAttacks
{
    public class PerformAttackMelee001System : ReactiveSystem<CoreEntity>
    {
        private readonly Contexts _contexts;

        public PerformAttackMelee001System(Contexts contexts) : base(contexts.core)
        {
            _contexts = contexts;
        }

        protected override ICollector<CoreEntity> GetTrigger(IContext<CoreEntity> context)
        {
            return context.CreateCollector(CoreMatcher.CanPerformAttack);
        }

        protected override bool Filter(CoreEntity entity)
        {
            return entity.isCanPerformAttack;
        }

        protected override void Execute(List<CoreEntity> entities)
        {
            foreach (var coreEntity in entities)
            {
                var attackData = GameData.GetAttackData(coreEntity.attackId.value);
                if (attackData.attackId != GameEnums.AttackId.Melee_001) continue;

                if (attackData.IsFrame(coreEntity.attackCurrentTime.value))
                {
                    var enemyData = GameData.GetEnemyData(coreEntity.unitId.value);
                    GameFactory.CreateApplyDamage(_contexts, coreEntity.coreId.value, coreEntity.attackTarget.value,
                        GameCalculations.Calculate(enemyData.Damage, coreEntity));
                }
            }
        }
    }
}
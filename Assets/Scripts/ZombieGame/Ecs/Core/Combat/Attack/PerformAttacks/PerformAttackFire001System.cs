using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using Entitas;
using Utilities;

namespace ZombieGame.Ecs.Core.Combat.Attack.PerformAttacks
{
    public class PerformAttackFire001System : ReactiveSystem<CoreEntity>
    {
        private readonly Contexts _contexts;

        public PerformAttackFire001System(Contexts contexts) : base(contexts.core)
        {
            _contexts = contexts;
        }

        protected override ICollector<CoreEntity> GetTrigger(IContext<CoreEntity> context)
        {
            return context.CreateCollector(CoreMatcher.CanPerformAttack);
        }

        protected override bool Filter(CoreEntity entity)
        {
            return entity.isCanPerformAttack;
        }

        protected override void Execute(List<CoreEntity> entities)
        {
            foreach (var coreEntity in entities)
            {
                var attackData = GameData.GetAttackData(coreEntity.attackId.value);
                if (attackData.attackId != GameEnums.AttackId.Fire_001) continue;

                if (attackData.IsFrame(coreEntity.attackCurrentTime.value))
                {
                    var weaponEntity = _contexts.core.GetEntityWithCoreId(coreEntity.allyWeaponEquipped.value);
                    var weapon =
                        GameResourceManager.Instance.GetPrefab<Utilities.Weapon>(weaponEntity.weaponId.value
                            .ToString());

                    var weaponData = GameData.GetWeaponData(weaponEntity.weaponId.value);

                    weaponEntity.ReplaceWeaponAmmo(--weaponEntity.weaponAmmo.value);


                    var bulletSpawnPos = coreEntity.position.value + weapon.MuzzlePosition.localPosition.ToVector2();
                    var direction = new Vector2(0, -1);

                    var target = _contexts.core.GetEntityWithCoreId(coreEntity.attackTarget.value);
                    var frontOfTarget = target.position.value;

                    if (target.hasVelocity)
                    {
                        frontOfTarget += target.velocity.value;
                    }

                    direction = Vector2.Normalize(frontOfTarget - bulletSpawnPos);

                    var weaponDamage = GameCalculations.Calculate(weaponData.Damage, weaponEntity);

                    weaponDamage = GameCalculations.ApplyModifiers(_contexts, weaponDamage,
                        GameEnums.ModifierTargetEntityType.Ally,
                        GameEnums.ModifierStatId.Damage);

                    if (weaponEntity.weaponId.value == GameEnums.WeaponId.ShotgunSawnOff ||
                        weaponEntity.weaponId.value == GameEnums.WeaponId.Shotgun)
                    {
                        var shots = 6;
                        var spread = 80;

                        if (weaponEntity.weaponId.value == GameEnums.WeaponId.ShotgunSawnOff)
                        {
                            shots = 15;
                            spread = 130;
                        }

                        var random = new Random();
                        for (int i = 0; i < shots; i++)
                        {
                            var randomSpread = new Vector2(random.Next(0, spread), random.Next(0, spread));

                            if (random.Next(0, 2) == 1)
                            {
                                randomSpread.X = -randomSpread.X;
                            }

                            if (random.Next(0, 2) == 1)
                            {
                                randomSpread.Y = -randomSpread.Y;
                            }


                            direction = Vector2.Normalize(frontOfTarget - bulletSpawnPos + randomSpread);

                            GameFactory.CreateBullet(_contexts,
                                bulletSpawnPos, coreEntity.coreId.value, direction, GameSettings.BulletSpeed,
                                weaponDamage);
                        }
                    }
                    else
                    {
                        GameFactory.CreateBullet(_contexts,
                            bulletSpawnPos, coreEntity.coreId.value, direction, GameSettings.BulletSpeed, weaponDamage);
                    }
                }
            }
        }
    }
}
namespace ZombieGame.Ecs.Core.Combat.Attack.PerformAttacks
{
    public class PerformAttacksFeature : Feature
    {
        public PerformAttacksFeature(Contexts contexts)
        {
            Add(new SetCanPerformAttackSystem(contexts));
            Add(new PerformAttackMelee001System(contexts));
            Add(new PerformAttackFire001System(contexts));
            
        }
    }
}
using System.Collections.Generic;
using Entitas;

namespace ZombieGame.Ecs.Core.Combat.Attack
{
    public class CancelAttackIfTargetRemovedSystem : ReactiveSystem<CoreEntity>
    {
        private readonly Contexts _contexts;

        public CancelAttackIfTargetRemovedSystem(Contexts contexts) : base(contexts.core)
        {
            _contexts = contexts;
        }

        protected override ICollector<CoreEntity> GetTrigger(IContext<CoreEntity> context)
        {
            return context.CreateCollector(CoreMatcher.AttackTarget.Removed());
        }

        protected override bool Filter(CoreEntity entity)
        {
            return entity.hasAttackTarget == false && entity.hasAttackId;
        }

        protected override void Execute(List<CoreEntity> entities)
        {
            foreach (var coreEntity in entities)
            {
                GameAiHelpers.EndAttack(coreEntity);
            }
        }
    }
}
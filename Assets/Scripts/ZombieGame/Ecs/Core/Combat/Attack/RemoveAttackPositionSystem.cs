using System.Collections.Generic;
using Entitas;

namespace ZombieGame.Ecs.Core.Combat.Attack
{
    public class RemoveAttackPositionSystem : ReactiveSystem<CoreEntity>
    {
        private readonly Contexts _contexts;

        public RemoveAttackPositionSystem(Contexts contexts) : base(contexts.core)
        {
            _contexts = contexts;
        }

        protected override ICollector<CoreEntity> GetTrigger(IContext<CoreEntity> context)
        {
            return context.CreateCollector(CoreMatcher.AllOf(CoreMatcher.AttackPosition)
                .NoneOf(CoreMatcher.AttackTarget));
        }

        protected override bool Filter(CoreEntity entity)
        {
            return entity.hasAttackTarget == false && entity.hasAttackPosition;
        }

        protected override void Execute(List<CoreEntity> entities)
        {
            foreach (var coreEntity in entities)
            {
                coreEntity.RemoveAttackPosition();
            }
        }
    }
}
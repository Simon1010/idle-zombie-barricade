using Entitas;
using IdleMineGenerator.CodeGeneration.Attributes;

namespace ZombieGame.Ecs.Core.Combat.Health
{
    [Core, ListenerSelf]
    public class DeadComponent : IComponent
    {
    }
}
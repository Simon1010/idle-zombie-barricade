namespace ZombieGame.Ecs.Core.Combat.Health
{
    public class HealthFeature : Feature
    {
        public HealthFeature(Contexts contexts)
        {
            Add(new DestroyDeadEntitiesSystem(contexts));
            Add(new SetEntitiesWithEmptyHealthAsDeadSystem(contexts));
        }
    }
}
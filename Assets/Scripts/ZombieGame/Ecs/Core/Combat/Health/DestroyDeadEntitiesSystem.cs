using System.Collections.Generic;
using Entitas;

namespace ZombieGame.Ecs.Core.Combat.Health
{
    public class DestroyDeadEntitiesSystem : ReactiveSystem<CoreEntity>
    {
        private readonly Contexts _contexts;

        public DestroyDeadEntitiesSystem(Contexts contexts) : base(contexts.core)
        {
            _contexts = contexts;
        }

        protected override ICollector<CoreEntity> GetTrigger(IContext<CoreEntity> context)
        {
            return context.CreateCollector(CoreMatcher.AllOf(CoreMatcher.Dead)
                .AnyOf(CoreMatcher.DestroyOnDeadTime, CoreMatcher.DestroyOnDead));
        }

        protected override bool Filter(CoreEntity entity)
        {
            return entity.isDead;
        }

        protected override void Execute(List<CoreEntity> entities)
        {
            foreach (var coreEntity in entities)
            {
                if (coreEntity.hasDestroyOnDeadTime)
                {
                    var timeLeft = coreEntity.destroyOnDeadTime.value;
                    timeLeft--;
                    coreEntity.ReplaceDestroyOnDeadTime(timeLeft);
                    if (timeLeft > 0)
                    {
                        continue;
                    }
                }

                coreEntity.isDestroyed = true;
            }
        }
    }
}
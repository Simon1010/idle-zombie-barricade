using Entitas;

namespace ZombieGame.Ecs.Core.Combat.Health
{
    [Core]
    public class DestroyOnDeadTimeComponent : IComponent
    {
        public float value;
    }
}
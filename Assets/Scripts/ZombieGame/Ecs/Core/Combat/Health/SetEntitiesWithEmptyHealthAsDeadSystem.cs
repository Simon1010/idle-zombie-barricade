using System.Collections.Generic;
using Entitas;

namespace ZombieGame.Ecs.Core.Combat.Health
{
    public class SetEntitiesWithEmptyHealthAsDeadSystem : ReactiveSystem<CoreEntity>
    {
        private readonly Contexts _contexts;

        public SetEntitiesWithEmptyHealthAsDeadSystem(Contexts contexts) : base(contexts.core)
        {
            _contexts = contexts;
        }

        protected override ICollector<CoreEntity> GetTrigger(IContext<CoreEntity> context)
        {
            return context.CreateCollector(CoreMatcher.AllOf(CoreMatcher.CurrentHealth).NoneOf(CoreMatcher.Dead));
        }

        protected override bool Filter(CoreEntity entity)
        {
            return entity.hasCurrentHealth && entity.isDead == false;
        }

        protected override void Execute(List<CoreEntity> entities)
        {
            foreach (var coreEntity in entities)
            {
                if (coreEntity.currentHealth.value <= 0)
                {
                    coreEntity.isDead = true;
                }
            }
        }
    }
}
using System.Collections.Generic;
using Entitas;
using ZombieGame.Data;

namespace ZombieGame.Ecs.Core.Combat.AwardMoneyOnDeath
{
    public class
        AwardMoneyOnDeathSystem : ReactiveSystem<CoreEntity>
    {
        private readonly Contexts _contexts;

        public AwardMoneyOnDeathSystem(Contexts contexts) : base(contexts.core)
        {
            _contexts = contexts;
        }

        protected override ICollector<CoreEntity> GetTrigger(IContext<CoreEntity> context)
        {
            return context.CreateCollector(CoreMatcher.AllOf(CoreMatcher.Dead, CoreMatcher.AwardMoneyOnDeath));
        }

        protected override bool Filter(CoreEntity entity)
        {
            return entity.isDead && entity.isAwardMoneyOnDeath;
        }

        protected override void Execute(List<CoreEntity> entities)
        {
            foreach (var coreEntity in entities)
            {
                var enemyData = GameData.GetEnemyData(coreEntity.unitId.value);
                var reward = GameCalculations.CalculateInt(enemyData.KillReward, coreEntity);
                _contexts.core.playerEntity.ReplacePlayerMoney(
                    _contexts.core.playerEntity.playerMoney.value + reward);
            }
        }
    }
}
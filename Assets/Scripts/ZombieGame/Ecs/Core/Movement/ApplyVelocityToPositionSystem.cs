using System.Collections.Generic;
using Entitas;

namespace ZombieGame.Ecs.Core.Movement
{
    public class ApplyVelocityToPositionSystem : ReactiveSystem<CoreEntity>
    {
        private readonly Contexts _contexts;
        private IGroup<CoreEntity> _velocityGroup;

        public ApplyVelocityToPositionSystem(Contexts contexts) : base(contexts.core)
        {
            _contexts = contexts;
            _velocityGroup =
                _contexts.core.GetGroup(CoreMatcher.Velocity);
        }

        protected override ICollector<CoreEntity> GetTrigger(IContext<CoreEntity> context)
        {
            return context.CreateCollector(CoreMatcher.AllOf(CoreMatcher.GameplayTick, CoreMatcher.TickComplete));
        }

        protected override bool Filter(CoreEntity entity)
        {
            return entity.isGameplayTick && entity.isTickComplete;
        }

        protected override void Execute(List<CoreEntity> entities)
        {
            foreach (var coreEntity in _velocityGroup.GetEntities())
            {
                coreEntity.ReplacePosition(coreEntity.position.value + coreEntity.velocity.value);
            }
        }
    }
}
using System.Numerics;
using Entitas;

namespace ZombieGame.Ecs.Core.Movement
{
    [Core]
    public class MoveToComponent : IComponent
    {
        public Vector2 value;
    }
}
using System.Numerics;
using Entitas;

namespace ZombieGame.Ecs.Core.Movement
{
    [Core]
    public class VelocityComponent : IComponent
    {
        public Vector2 value;
    }
}
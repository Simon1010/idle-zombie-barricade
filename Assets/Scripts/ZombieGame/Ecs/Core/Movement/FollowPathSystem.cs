using System.Collections.Generic;
using Entitas;

namespace ZombieGame.Ecs.Core.Movement
{
    public class FollowPathSystem : ReactiveSystem<CoreEntity>
    {
        private readonly Contexts _contexts;

        public FollowPathSystem(Contexts contexts) : base(contexts.core)
        {
            _contexts = contexts;
        }

        protected override ICollector<CoreEntity> GetTrigger(IContext<CoreEntity> context)
        {
            return context.CreateCollector(CoreMatcher.AnyOf(CoreMatcher.MoveToComplete));
        }

        protected override bool Filter(CoreEntity entity)
        {
            return entity.isMoveToComplete && entity.hasMovementPath;
        }

        protected override void Execute(List<CoreEntity> entities)
        {
            foreach (var coreEntity in entities)
            {
                var nodeId = coreEntity.movementPathCurrentNodeId.value;
                nodeId++;

                if (coreEntity.movementPath.value.Length == nodeId)
                {
                    GameAiHelpers.CancelPath(coreEntity);
                    coreEntity.isFollowPathComplete = true;
                }
                else
                {
                    coreEntity.ReplaceMovementPathCurrentNodeId(nodeId);
                    coreEntity.ReplaceMoveTo(coreEntity.movementPath.value[nodeId]);
                }
            }
        }
    }
}
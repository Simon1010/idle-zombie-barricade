namespace ZombieGame.Ecs.Core.Movement
{
    public class VelocityFeature : Feature
    {
        public VelocityFeature(Contexts contexts)
        {
            Add(new ApplyVelocityToPositionSystem(contexts));
            Add(new ApplyGravityToVelocitySystem(contexts));
            Add(new ApplyFrictionToVelocitySystem(contexts));
            
        }
    }
}
using Entitas;

namespace ZombieGame.Ecs.Core.Movement
{
    [Core]
    public class MoveToArriveDistanceComponent : IComponent
    {
        public float value;
    }
}
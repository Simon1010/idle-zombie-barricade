using Entitas;

namespace ZombieGame.Ecs.Core.Movement
{
    [Core]
    public class MovementPathCurrentNodeIdComponent : IComponent
    {
        public int value;
    }
}
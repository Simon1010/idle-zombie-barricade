namespace ZombieGame.Ecs.Core.Movement
{
    public class MovementFeature : Feature
    {
        public MovementFeature(Contexts contexts)
        {
            Add(new CompleteFollowPathSystem(contexts));
            Add(new MoveToSystem(contexts));
            Add(new StartFollowPathSystem(contexts));
            Add(new FollowPathSystem(contexts));
        }
    }
}
using Entitas;

namespace ZombieGame.Ecs.Core.Movement.MimicPosition
{
    public class MimicPositionSystem : IExecuteSystem
    {
        private readonly Contexts _contexts;
        private IGroup<CoreEntity> _group;

        public MimicPositionSystem(Contexts contexts)
        {
            _contexts = contexts;
            _group = contexts.core.GetGroup(CoreMatcher.MimicPosition);
        }

        public void Execute()
        {
            foreach (var coreEntity in _group.GetEntities())
            {
                var target = _contexts.core.GetEntityWithCoreId(coreEntity.mimicPosition.value);

                if (target.hasPosition)
                {
                    var newPosition = target.position.value;

                    if (coreEntity.hasMimicPositionOffset)
                    {
                        newPosition += coreEntity.mimicPositionOffset.value;
                    }

                    coreEntity.ReplacePosition(newPosition);
                }
            }
        }
    }
}
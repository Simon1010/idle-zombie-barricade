using System.Numerics;
using Entitas;
using IdleMineGenerator.CodeGeneration.Attributes;

namespace ZombieGame.Ecs.Core.Movement
{
    [Core, ListenerSelf()]
    public class PositionComponent : IComponent
    {
        public Vector2 value;
    }
}
using Entitas;

namespace ZombieGame.Ecs.Core.Movement
{
    [Core]
    public class MoveSpeedComponent : IComponent
    {
        public float value;
    }
}
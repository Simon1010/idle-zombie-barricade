namespace ZombieGame.Ecs.Core.Gameplay.Map
{
    public class MapFeature : Feature
    {
        public MapFeature(Contexts contexts)
        {
            Add(new SetupMapSystem(contexts));
        }
    }
}
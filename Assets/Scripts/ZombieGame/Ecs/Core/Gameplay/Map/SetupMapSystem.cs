using System.Collections.Generic;
using System.Numerics;
using Entitas;
using Utilities;

namespace ZombieGame.Ecs.Core.Gameplay.Map
{
    public class SetupMapSystem : ReactiveSystem<CoreEntity>
    {
        private readonly Contexts _contexts;

        public SetupMapSystem(Contexts contexts) : base(contexts.core)
        {
            _contexts = contexts;
        }

        protected override ICollector<CoreEntity> GetTrigger(IContext<CoreEntity> context)
        {
            return context.CreateCollector(CoreMatcher.Map);
        }

        protected override bool Filter(CoreEntity entity)
        {
            return entity.isMap;
        }

        protected override void Execute(List<CoreEntity> entities)
        {
            foreach (var coreEntity in entities)
            {
                var map = GameResourceManager.Instance.GetPrefab<Utilities.Map>("Map");
                var barricade = GameFactory.CreateBarricade(_contexts, new Vector2(0, 475), 100);

                var safehouseEntrance = _contexts.core.CreateEntity();
                safehouseEntrance.isSafehouseEntrance = true;
                safehouseEntrance.ReplacePosition(barricade.position.value +
                                                  map.SafehousePosition.localPosition.ToVector2());
            }
        }
    }
}
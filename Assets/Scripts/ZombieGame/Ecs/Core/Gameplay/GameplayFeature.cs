using ZombieGame.Ecs.Core.Collision;
using ZombieGame.Ecs.Core.Combat;
using ZombieGame.Ecs.Core.Map;
using ZombieGame.Ecs.Core.Movement;
using ZombieGame.Ecs.Core.Movement.MimicPosition;
using ZombieGame.Ecs.Core.Spawning;
using ZombieGame.Ecs.Core.Tick;
using ZombieGame.Ecs.Core.Units;
using ZombieGame.Ecs.Core.Units.Ai;

namespace ZombieGame.Ecs.Core.Gameplay
{
    public class GameplayFeature : Feature
    {
        public GameplayFeature(Contexts contexts)
        {

        }
    }
}
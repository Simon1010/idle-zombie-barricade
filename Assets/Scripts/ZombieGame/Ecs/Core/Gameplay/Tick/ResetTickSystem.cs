using Entitas;

namespace ZombieGame.Ecs.Core.Gameplay.Tick
{
    public class ResetTickSystem : IExecuteSystem
    {
        private readonly Contexts _contexts;
        private IGroup<CoreEntity> _ticks;

        public ResetTickSystem(Contexts contexts)
        {
            _contexts = contexts;
            _ticks = _contexts.core.GetGroup(CoreMatcher.AllOf(CoreMatcher.Tick, CoreMatcher.TickComplete));
        }

        public void Execute()
        {
            foreach (var coreEntity in _ticks.GetEntities())
            {
                coreEntity.ReplaceTickCurrentTime(coreEntity.tickTotalTime.value);
                coreEntity.isTickComplete = false;
            }
        }
    }
}
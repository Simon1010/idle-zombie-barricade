using Entitas;

namespace ZombieGame.Ecs.Core.Gameplay.Tick
{
    [Core]
    public class TickTotalTimeComponent : IComponent
    {
        public float value;
    }
}
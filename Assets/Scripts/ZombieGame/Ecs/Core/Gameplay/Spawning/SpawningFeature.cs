namespace ZombieGame.Ecs.Core.Gameplay.Spawning
{
    public class SpawningFeature : Feature
    {
        public SpawningFeature(Contexts contexts)
        {
            Add(new SpawnAllySystem(contexts));
            Add(new SpawnEnemiesSystem(contexts));
        }
    }
}
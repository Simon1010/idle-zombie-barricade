using System.Collections.Generic;
using Entitas;

namespace ZombieGame.Ecs.Core.Gameplay.Combat.Health
{
    public class DestroyEntitiesWithEmptyHealthSystem : ReactiveSystem<CoreEntity>
    {
        private readonly Contexts _contexts;

        public DestroyEntitiesWithEmptyHealthSystem(Contexts contexts) : base(contexts.core)
        {
            _contexts = contexts;
        }

        protected override ICollector<CoreEntity> GetTrigger(IContext<CoreEntity> context)
        {
            return context.CreateCollector(CoreMatcher.CurrentHealth);
        }

        protected override bool Filter(CoreEntity entity)
        {
            return entity.hasCurrentHealth;
        }

        protected override void Execute(List<CoreEntity> entities)
        {
            foreach (var coreEntity in entities)
            {
                if (coreEntity.currentHealth.value <= 0)
                {
                    coreEntity.isDestroyed = true;
                }
            }
        }
    }
}
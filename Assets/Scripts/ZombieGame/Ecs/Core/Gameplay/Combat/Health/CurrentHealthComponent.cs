using Entitas;
using IdleMineGenerator.CodeGeneration.Attributes;

namespace ZombieGame.Ecs.Core.Gameplay.Combat.Health
{
    [Core, ListenerSelf, ListenerAny]
    public class CurrentHealthComponent : IComponent
    {
        public float value;
    }
}
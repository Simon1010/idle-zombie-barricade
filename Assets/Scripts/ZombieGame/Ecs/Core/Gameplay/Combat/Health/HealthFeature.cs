namespace ZombieGame.Ecs.Core.Gameplay.Combat.Health
{
    public class HealthFeature : Feature
    {
        public HealthFeature(Contexts contexts)
        {
            Add(new DestroyEntitiesWithEmptyHealthSystem(contexts));
        }
    }
}
using Entitas;
using IdleMineGenerator.CodeGeneration.Attributes;

namespace ZombieGame.Ecs.Core.Gameplay.Combat.Health
{
    [Core, ListenerSelf]
    public class TotalHealthComponent : IComponent
    {
        public float value;
    }
}
using ZombieGame.Ecs.Core.Gameplay.Combat.ApplyDamage;
using ZombieGame.Ecs.Core.Gameplay.Combat.Bullet;
using ZombieGame.Ecs.Core.Gameplay.Combat.Health;

namespace ZombieGame.Ecs.Core.Gameplay.Combat
{
    public class CombatFeature : Feature
    {
        public CombatFeature(Contexts contexts)
        {
            Add(new BulletFeature(contexts));
            Add(new DamageFeature(contexts));
            Add(new HealthFeature(contexts));
        }
    }
}
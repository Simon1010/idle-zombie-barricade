using Entitas;

namespace ZombieGame.Ecs.Core.Gameplay.Combat.Bullet
{
    [Core]
    public class BulletDamageComponent : IComponent
    {
        public float value;
    }
}
using Entitas;

namespace ZombieGame.Ecs.Core.Gameplay.Combat.Bullet
{
    [Core]
    public class BulletSourceComponent : IComponent
    {
        public CoreEntityId value;
    }
}
namespace ZombieGame.Ecs.Core.Gameplay.Combat.Bullet
{
    public class BulletFeature : Feature
    {
        public BulletFeature(Contexts contexts)
        {
            Add(new HandleBulletCollisionSystem(contexts));
        }
    }
}
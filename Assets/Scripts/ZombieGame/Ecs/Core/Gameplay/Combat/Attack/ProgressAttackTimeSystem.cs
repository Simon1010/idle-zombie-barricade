using Entitas;

namespace ZombieGame.Ecs.Core.Gameplay.Combat.Attack
{
    public class ProgressAttackTimeSystem : IExecuteSystem
    {
        private readonly Contexts _contexts;
        private IGroup<CoreEntity> _group;

        public ProgressAttackTimeSystem(Contexts contexts)
        {
            _contexts = contexts;
            _group = _contexts.core.GetGroup(CoreMatcher.AllOf(CoreMatcher.AttackCurrentTime));
        }

        public void Execute()
        {
            foreach (var coreEntity in _group.GetEntities())
            {
                var currentTime = coreEntity.attackCurrentTime.value;
                currentTime += 1;
                coreEntity.ReplaceAttackCurrentTime(currentTime);

                if (currentTime >= coreEntity.attackTotalTime.value)
                {
                    coreEntity.isAttackComplete = true;
                }
            }
        }
    }
}
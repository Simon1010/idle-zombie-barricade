using System.Numerics;
using Entitas;

namespace ZombieGame.Ecs.Core.Gameplay.Combat.Attack
{
    [Core]
    public class AttackPositionComponent : IComponent
    {
        public Vector2 value;
    }
}
using Entitas;
using IdleMineGenerator.CodeGeneration.Attributes;

namespace ZombieGame.Ecs.Core.Gameplay.Combat.Attack
{
    [Core, ListenerSelf]
    public class AttackCompleteComponent : IComponent
    {
    }
}
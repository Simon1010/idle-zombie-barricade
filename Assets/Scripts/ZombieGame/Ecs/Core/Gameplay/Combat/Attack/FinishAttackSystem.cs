using Entitas;

namespace ZombieGame.Ecs.Core.Gameplay.Combat.Attack
{
    public class FinishAttackSystem : IExecuteSystem
    {
        private readonly Contexts _contexts;
        private IGroup<CoreEntity> _group;

        public FinishAttackSystem(Contexts contexts)
        {
            _contexts = contexts;
            _group = _contexts.core.GetGroup(CoreMatcher.AllOf(CoreMatcher.AttackComplete));
        }

        public void Execute()
        {
            foreach (var coreEntity in _group.GetEntities())
            {
                GameAiHelpers.EndAttack(coreEntity);
                coreEntity.isAttackComplete = false;
                coreEntity.ReplaceTimeSinceLastAttack(0);
            }
        }
    }
}
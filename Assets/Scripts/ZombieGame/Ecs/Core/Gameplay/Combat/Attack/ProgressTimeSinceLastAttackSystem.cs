using Entitas;

namespace ZombieGame.Ecs.Core.Gameplay.Combat.Attack
{
    public class ProgressTimeSinceLastAttackSystem : IExecuteSystem
    {
        private readonly Contexts _contexts;
        private IGroup<CoreEntity> _group;

        public ProgressTimeSinceLastAttackSystem(Contexts contexts)
        {
            _contexts = contexts;
            _group = _contexts.core.GetGroup(CoreMatcher.TimeSinceLastAttack);
        }

        public void Execute()
        {
            foreach (var coreEntity in _group.GetEntities())
            {
                coreEntity.ReplaceTimeSinceLastAttack(++coreEntity.timeSinceLastAttack.value);
            }
        }
    }
}
using Entitas;

namespace ZombieGame.Ecs.Core.Gameplay.Combat.Attack
{
    [Core]
    public class AttackTotalTimeComponent : IComponent
    {
        public int value;
    }
}
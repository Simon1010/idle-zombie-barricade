using System.Collections.Generic;
using System.Numerics;
using Entitas;
using Utilities;

namespace ZombieGame.Ecs.Core.Gameplay.Combat.Attack.PerformAttacks
{
    public class PerformAttackFire001System : ReactiveSystem<CoreEntity>
    {
        private readonly Contexts _contexts;

        public PerformAttackFire001System(Contexts contexts) : base(contexts.core)
        {
            _contexts = contexts;
        }

        protected override ICollector<CoreEntity> GetTrigger(IContext<CoreEntity> context)
        {
            return context.CreateCollector(CoreMatcher.CanPerformAttack);
        }

        protected override bool Filter(CoreEntity entity)
        {
            return entity.isCanPerformAttack;
        }

        protected override void Execute(List<CoreEntity> entities)
        {
            foreach (var coreEntity in entities)
            {
                var attackData = GameData.GetAttackData(coreEntity.attackId.value);
                if (attackData.attackId != GameEnums.AttackId.Fire_001) continue;

                if (attackData.IsFrame(coreEntity.attackCurrentTime.value))
                {
                    var weaponEntity = _contexts.core.GetEntityWithCoreId(coreEntity.allyWeaponEquipped.value);
                    var weapon =
                        GameResourceManager.Instance.GetPrefab<Utilities.Weapon>(weaponEntity.weaponId.value
                            .ToString());


                    var bulletSpawnPos = coreEntity.position.value + weapon.MuzzlePosition.localPosition.ToVector2();
                    var direction = new Vector2(0, -1);

                    if (coreEntity.hasAttackTarget)
                    {
                        var target = _contexts.core.GetEntityWithCoreId(coreEntity.attackTarget.value);
                        direction = Vector2.Normalize(target.position.value - bulletSpawnPos);
                    }

                    GameFactory.CreateBullet(_contexts,
                        bulletSpawnPos,
                        coreEntity.coreId.value, direction, 10f, 1f);
                }
            }
        }
    }
}
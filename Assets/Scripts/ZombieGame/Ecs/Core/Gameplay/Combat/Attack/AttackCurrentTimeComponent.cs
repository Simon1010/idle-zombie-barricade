using Entitas;

namespace ZombieGame.Ecs.Core.Gameplay.Combat.Attack
{
    [Core]
    public class AttackCurrentTimeComponent : IComponent
    {
        public int value;
    }
}
using Entitas;

namespace ZombieGame.Ecs.Core.Gameplay.Combat.Attack
{
    [Core]
    public class TimeSinceLastAttackComponent : IComponent
    {
        public int value;
    }
}
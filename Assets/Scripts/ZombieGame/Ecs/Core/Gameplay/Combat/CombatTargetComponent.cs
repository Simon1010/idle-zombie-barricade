using Entitas;
using Entitas.CodeGeneration.Attributes;

namespace ZombieGame.Ecs.Core.Gameplay.Combat
{
    [Core]
    public class AttackTargetComponent : IComponent
    {
        [EntityIndex] public CoreEntityId value;
    }
}
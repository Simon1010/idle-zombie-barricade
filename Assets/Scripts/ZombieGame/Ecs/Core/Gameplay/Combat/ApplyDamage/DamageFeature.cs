namespace ZombieGame.Ecs.Core.Gameplay.Combat.ApplyDamage
{
    public class DamageFeature : Feature
    {
        public DamageFeature(Contexts contexts)
        {
            Add(new ApplyDamageSystem(contexts));
        }
    }
}
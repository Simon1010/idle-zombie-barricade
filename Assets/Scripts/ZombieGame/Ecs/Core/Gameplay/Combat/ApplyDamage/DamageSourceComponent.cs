using Entitas;

namespace ZombieGame.Ecs.Core.Gameplay.Combat.ApplyDamage
{
    [Core]
    public class DamageSourceComponent : IComponent
    {
        public CoreEntityId value;
    }
}
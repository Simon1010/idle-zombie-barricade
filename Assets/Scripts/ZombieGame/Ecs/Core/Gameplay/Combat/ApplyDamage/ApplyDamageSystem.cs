using System.Collections.Generic;
using Entitas;

namespace ZombieGame.Ecs.Core.Gameplay.Combat.ApplyDamage
{
    public class ApplyDamageSystem : ReactiveSystem<CoreEntity>, ICleanupSystem
    {
        private readonly Contexts _contexts;
        private IGroup<CoreEntity> _group;

        public ApplyDamageSystem(Contexts contexts) : base(contexts.core)
        {
            _contexts = contexts;
            _group = contexts.core.GetGroup(CoreMatcher.ApplyDamage);
        }

        protected override ICollector<CoreEntity> GetTrigger(IContext<CoreEntity> context)
        {
            return context.CreateCollector(CoreMatcher.ApplyDamage);
        }

        protected override bool Filter(CoreEntity entity)
        {
            return entity.isApplyDamage && entity.hasApplyDamageTarget;
        }

        protected override void Execute(List<CoreEntity> entities)
        {
            foreach (var coreEntity in entities)
            {
                var attackTarget = _contexts.core.GetEntityWithCoreId(coreEntity.applyDamageTarget.value);

                if (attackTarget != null)
                {
                    if (attackTarget.hasCurrentHealth)
                    {
                        var currentHealth = attackTarget.currentHealth.value;
                        currentHealth -= coreEntity.damageAmount.value;
                        attackTarget.ReplaceCurrentHealth(currentHealth);
                    }
                }
            }
        }

        public void Cleanup()
        {
            foreach (var coreEntity in _group.GetEntities())
            {
                coreEntity.isDestroyed = true;
            }
        }
    }
}
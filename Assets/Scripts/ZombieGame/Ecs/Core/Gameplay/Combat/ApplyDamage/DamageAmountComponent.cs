using Entitas;

namespace ZombieGame.Ecs.Core.Gameplay.Combat.ApplyDamage
{
    [Core]
    public class DamageAmountComponent : IComponent
    {
        public float value;
    }
}
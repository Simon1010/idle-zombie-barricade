using System.Collections.Generic;
using Entitas;

namespace ZombieGame.Ecs.Core.Gameplay.Units.Ai
{
    public class AllyBehaviourSystem : ReactiveSystem<CoreEntity>
    {
        private readonly Contexts _contexts;
        private IGroup<CoreEntity> _enemyGrunts;

        public AllyBehaviourSystem(Contexts contexts) : base(contexts.core)
        {
            _contexts = contexts;
            _enemyGrunts =
                contexts.core.GetGroup(CoreMatcher.AllOf(CoreMatcher.AllyBehaviour));
        }

        protected override ICollector<CoreEntity> GetTrigger(IContext<CoreEntity> context)
        {
            return context.CreateCollector(CoreMatcher.AllOf(CoreMatcher.TickComplete, CoreMatcher.GameplayTick));
        }

        protected override bool Filter(CoreEntity entity)
        {
            return entity.isGameplayTick && entity.isTickComplete;
        }

        protected override void Execute(List<CoreEntity> entities)
        {
            foreach (var coreEntity in _enemyGrunts.GetEntities())
            {
                if (coreEntity.hasUnitCurrentState == false)
                {
                    coreEntity.AddUnitCurrentState(GameEnums.UnitState.Idle);
                }

                if (coreEntity.unitCurrentState.value == GameEnums.UnitState.Idle)
                {
                    if (coreEntity.hasAttackTarget)
                    {
                        coreEntity.ReplaceUnitCurrentState(GameEnums.UnitState.Attacking);
                    }
                }

                if (coreEntity.unitCurrentState.value == GameEnums.UnitState.Attacking)
                {
                    if (coreEntity.hasAttackTarget)
                    {
                        GameAiHelpers.DoAttack(coreEntity, GameEnums.AttackId.Fire_001);
                    }
                    else
                    {
                        coreEntity.ReplaceUnitCurrentState(GameEnums.UnitState.Idle);
                    }
                }
            }
        }
    }
}
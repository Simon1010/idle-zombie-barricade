using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using Entitas;

namespace ZombieGame.Ecs.Core.Gameplay.Units.Ai
{
    public class AllyTargetingSystem : ReactiveSystem<CoreEntity>
    {
        private readonly Contexts _contexts;
        private IGroup<CoreEntity> _enemyGrunts;

        public AllyTargetingSystem(Contexts contexts) : base(contexts.core)
        {
            _contexts = contexts;
            _enemyGrunts =
                contexts.core.GetGroup(CoreMatcher.AllOf(CoreMatcher.AllyBehaviour));
        }

        protected override ICollector<CoreEntity> GetTrigger(IContext<CoreEntity> context)
        {
            return context.CreateCollector(CoreMatcher.AllOf(CoreMatcher.TickComplete, CoreMatcher.GameplayTick));
        }

        protected override bool Filter(CoreEntity entity)
        {
            return entity.isGameplayTick && entity.isTickComplete;
        }

        protected override void Execute(List<CoreEntity> entities)
        {
            foreach (var coreEntity in _enemyGrunts.GetEntities())
            {
                var potentialTargets = _contexts.core.GetGroup(CoreMatcher.Enemy).GetEntities();

                var weapon = _contexts.core.GetEntityWithCoreId(coreEntity.allyWeaponEquipped.value);
                
                var closeTargets = potentialTargets.Where(e =>
                        Vector2.Distance(e.position.value, coreEntity.position.value) < weapon.weaponRange.value)
                    .OrderBy(e => Vector2.Distance(e.position.value, coreEntity.position.value));


                if (coreEntity.hasAttackTarget)
                {
                    var attackTarget = _contexts.core.GetEntityWithCoreId(coreEntity.attackTarget.value);
                    if (attackTarget == null || attackTarget.isDestroyed)
                    {
                        coreEntity.RemoveAttackTarget();
                        if (coreEntity.hasAttackPosition)
                        {
                            coreEntity.RemoveAttackPosition();
                        }
                    }
                }

                if (closeTargets.Any())
                {
                    var canTarget = false;
                    if (coreEntity.hasAttackTarget)
                    {
                        var attackTarget = _contexts.core.GetEntityWithCoreId(coreEntity.attackTarget.value);
                        if (attackTarget.isUnit == false)
                        {
                            canTarget = true;
                        }
                    }
                    else
                    {
                        canTarget = true;
                    }

                    if (canTarget)
                    {
                        var closestTarget = closeTargets.FirstOrDefault();
                        coreEntity.ReplaceAttackTarget(closestTarget.coreId.value);
                        closestTarget.ReplaceAttackTarget(coreEntity.coreId.value);
                    }
                }
            }
        }
    }
}
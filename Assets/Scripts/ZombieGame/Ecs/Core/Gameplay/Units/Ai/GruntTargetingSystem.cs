using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using Entitas;

namespace ZombieGame.Ecs.Core.Gameplay.Units.Ai
{
    public class GruntTargetingSystem : ReactiveSystem<CoreEntity>
    {
        private readonly Contexts _contexts;
        private IGroup<CoreEntity> _enemyGrunts;

        public GruntTargetingSystem(Contexts contexts) : base(contexts.core)
        {
            _contexts = contexts;
            _enemyGrunts =
                contexts.core.GetGroup(CoreMatcher.AllOf(CoreMatcher.GruntBehaviour));
        }

        protected override ICollector<CoreEntity> GetTrigger(IContext<CoreEntity> context)
        {
            return context.CreateCollector(CoreMatcher.AllOf(CoreMatcher.TickComplete, CoreMatcher.GameplayTick));
        }

        protected override bool Filter(CoreEntity entity)
        {
            return entity.isGameplayTick && entity.isTickComplete;
        }

        protected override void Execute(List<CoreEntity> entities)
        {
            foreach (var coreEntity in _enemyGrunts.GetEntities())
            {
//                var potentialTargets = _contexts.core.GetGroup(CoreMatcher.Enemy).GetEntities();
//
//                if (coreEntity.isEnemy)
//                {
//                    potentialTargets = _contexts.core.GetGroup(CoreMatcher.Ally).GetEntities();
//                }
//
//                var nonTargetedTargets = potentialTargets.Where(e =>
//                    _contexts.core.GetEntitiesWithAttackTarget(e.coreId.value).Any() == false);
//
//                var closeTargets = nonTargetedTargets.Where(e =>
//                        Vector2.Distance(e.position.value, coreEntity.position.value) < 350f)
//                    .OrderBy(e => Vector2.Distance(e.position.value, coreEntity.position.value));
//
//
//                if (coreEntity.hasAttackTarget)
//                {
//                    var attackTarget = _contexts.core.GetEntityWithCoreId(coreEntity.attackTarget.value);
//                    if (attackTarget == null || attackTarget.isDestroyed)
//                    {
//                        coreEntity.RemoveAttackTarget();
//                        if (coreEntity.hasAttackPosition)
//                        {
//                            coreEntity.RemoveAttackPosition();
//                        }
//                    }
//                }
//
//
//                if (closeTargets.Any())
//                {
//                    var canTarget = false;
//                    if (coreEntity.hasAttackTarget)
//                    {
//                        var attackTarget = _contexts.core.GetEntityWithCoreId(coreEntity.attackTarget.value);
//                        if (attackTarget.isUnit == false)
//                        {
//                            canTarget = true;
//                        }
//                    }
//                    else
//                    {
//                        canTarget = true;
//                    }
//
//                    if (canTarget)
//                    {
//                        var closestTarget = closeTargets.FirstOrDefault();
//                        coreEntity.ReplaceAttackTarget(closestTarget.coreId.value);
//                        closestTarget.ReplaceAttackTarget(coreEntity.coreId.value);
//                    }
//                }


//                if (closeTargets.Any() == false && coreEntity.hasAttackTarget == false)
//                {
                    if (coreEntity.isEnemy)
                    {
                        if (_contexts.core.barricadeEntity == null)
                        {
                            coreEntity.ReplaceAttackTarget(_contexts.core.safehouseEntranceEntity.coreId.value);
                        }
                        else
                        {
                            coreEntity.ReplaceAttackTarget(_contexts.core.barricadeEntity.coreId.value);
                        }
                    }
//                }
            }
        }
    }
}
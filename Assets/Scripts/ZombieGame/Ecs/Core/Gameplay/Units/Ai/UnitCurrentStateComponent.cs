using Entitas;
using IdleMineGenerator.CodeGeneration.Attributes;

namespace ZombieGame.Ecs.Core.Gameplay.Units.Ai
{
    [Core, ListenerSelf]
    public class UnitCurrentStateComponent : IComponent
    {
        public GameEnums.UnitState value;
    }
}
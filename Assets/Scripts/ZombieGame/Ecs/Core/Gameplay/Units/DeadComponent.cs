using Entitas;
using IdleMineGenerator.CodeGeneration.Attributes;

namespace ZombieGame.Ecs.Core.Gameplay.Units
{
    [Core, ListenerSelf]
    public class DeadComponent : IComponent
    {
    }
}
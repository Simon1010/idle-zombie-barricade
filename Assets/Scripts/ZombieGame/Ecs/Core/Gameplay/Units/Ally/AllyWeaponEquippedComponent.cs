using Entitas;
using IdleMineGenerator.CodeGeneration.Attributes;

namespace ZombieGame.Ecs.Core.Gameplay.Units.Ally
{
    [Core, ListenerSelf]
    public class AllyWeaponEquippedComponent : IComponent
    {
        public CoreEntityId value;
    }
}
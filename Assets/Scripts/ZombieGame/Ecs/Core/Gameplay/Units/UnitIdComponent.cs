using Entitas;
using IdleMineGenerator.CodeGeneration.Attributes;

namespace ZombieGame.Ecs.Core.Gameplay.Units
{
    [Core, ListenerAny]
    public class UnitIdComponent : IComponent
    {
        public GameEnums.UnitId value;
    }
}
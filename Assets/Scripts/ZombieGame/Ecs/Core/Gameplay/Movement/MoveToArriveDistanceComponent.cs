using Entitas;

namespace ZombieGame.Ecs.Core.Gameplay.Movement
{
    [Core]
    public class MoveToArriveDistanceComponent : IComponent
    {
        public float value;
    }
}
using Entitas;

namespace ZombieGame.Ecs.Core.Gameplay.Movement
{
    public class ApplyGravityToVelocitySystem : IExecuteSystem
    {
        private readonly Contexts _contexts;
        private IGroup<CoreEntity> _velocityGroup;

        public ApplyGravityToVelocitySystem(Contexts contexts)
        {
            _contexts = contexts;
            _velocityGroup =
                _contexts.core.GetGroup(CoreMatcher.AllOf(CoreMatcher.Velocity, CoreMatcher.AffectedByGravity,
                    CoreMatcher.Falling));
        }

        public void Execute()
        {
            foreach (var coreEntity in _velocityGroup.GetEntities())
            {
                var velocity = coreEntity.velocity.value;
                velocity.Y -= GameSettings.Gravity;
                coreEntity.ReplaceVelocity(velocity);
            }
        }
    }
}
using Entitas;

namespace ZombieGame.Ecs.Core.Gameplay.Movement
{
    [Core]
    public class MovementPathCurrentNodeIdComponent : IComponent
    {
        public int value;
    }
}
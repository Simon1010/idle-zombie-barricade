using System.Collections.Generic;
using Entitas;

namespace ZombieGame.Ecs.Core.Gameplay.Movement
{
    public class CompleteFollowPathSystem : ReactiveSystem<CoreEntity>
    {
        private readonly Contexts _contexts;

        public CompleteFollowPathSystem(Contexts contexts) : base(contexts.core)
        {
            _contexts = contexts;
        }

        protected override ICollector<CoreEntity> GetTrigger(IContext<CoreEntity> context)
        {
            return context.CreateCollector(CoreMatcher.FollowPathComplete);
        }

        protected override bool Filter(CoreEntity entity)
        {
            return entity.isFollowPathComplete;
        }

        protected override void Execute(List<CoreEntity> entities)
        {
            foreach (var coreEntity in entities)
            {
                coreEntity.isFollowPathComplete = false;
            }
        }
    }
}
using Entitas;

namespace ZombieGame.Ecs.Core.Gameplay.Movement
{
    public class ApplyFrictionToVelocitySystem : IExecuteSystem
    {
        private readonly Contexts _contexts;
        private IGroup<CoreEntity> _velocityGroup;

        public ApplyFrictionToVelocitySystem(Contexts contexts)
        {
            _contexts = contexts;
            _velocityGroup =
                _contexts.core.GetGroup(CoreMatcher.AllOf(CoreMatcher.Velocity).NoneOf(CoreMatcher.Falling, CoreMatcher.IgnoreFriction));
        }

        public void Execute()
        {
            foreach (var coreEntity in _velocityGroup.GetEntities())
            {
                var velocity = coreEntity.velocity.value;
                velocity *= GameSettings.Friction;
                coreEntity.ReplaceVelocity(velocity);
            }
        }
    }
}
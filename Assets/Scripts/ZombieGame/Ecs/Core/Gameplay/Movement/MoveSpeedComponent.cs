using Entitas;

namespace ZombieGame.Ecs.Core.Gameplay.Movement
{
    [Core]
    public class MoveSpeedComponent : IComponent
    {
        public float value;
    }
}
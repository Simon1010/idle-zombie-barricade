using Entitas;

namespace ZombieGame.Ecs.Core.Gameplay.Movement.MimicPosition
{
    [Core]
    public class MimicPositionComponent : IComponent
    {
        public CoreEntityId value;
    }
}
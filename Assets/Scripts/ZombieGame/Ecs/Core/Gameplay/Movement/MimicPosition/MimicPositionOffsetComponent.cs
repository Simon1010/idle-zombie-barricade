using System.Numerics;
using Entitas;

namespace ZombieGame.Ecs.Core.Gameplay.Movement.MimicPosition
{
    [Core]
    public class MimicPositionOffsetComponent : IComponent
    {
        public Vector2 value;
    }
}
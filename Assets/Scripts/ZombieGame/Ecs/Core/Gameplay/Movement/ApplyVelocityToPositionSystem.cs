using Entitas;

namespace ZombieGame.Ecs.Core.Gameplay.Movement
{
    public class ApplyVelocityToPositionSystem : IExecuteSystem
    {
        private readonly Contexts _contexts;
        private IGroup<CoreEntity> _velocityGroup;

        public ApplyVelocityToPositionSystem(Contexts contexts)
        {
            _contexts = contexts;
            _velocityGroup =
                _contexts.core.GetGroup(CoreMatcher.Velocity);
        }

        public void Execute()
        {
            foreach (var coreEntity in _velocityGroup.GetEntities())
            {
                coreEntity.ReplacePosition(coreEntity.position.value + coreEntity.velocity.value);
            }
        }
    }
}
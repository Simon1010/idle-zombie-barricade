namespace ZombieGame.Ecs.Core.Gameplay.Movement
{
    public class VelocityFeature : Feature
    {
        public VelocityFeature(Contexts contexts)
        {
            Add(new ApplyVelocityToPositionSystem(contexts));
            Add(new ApplyGravityToVelocitySystem(contexts));
            Add(new ApplyFrictionToVelocitySystem(contexts));
            
        }
    }
}
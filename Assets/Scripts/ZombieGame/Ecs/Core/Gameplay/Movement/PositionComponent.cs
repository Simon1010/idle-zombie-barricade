using System.Numerics;
using Entitas;
using IdleMineGenerator.CodeGeneration.Attributes;

namespace ZombieGame.Ecs.Core.Gameplay.Movement
{
    [Core, ListenerSelf()]
    public class PositionComponent : IComponent
    {
        public Vector2 value;
    }
}
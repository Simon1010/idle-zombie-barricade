﻿using Entitas;
using Entitas.CodeGeneration.Attributes;

namespace ZombieGame.Ecs.Core
{
    [Core]
    public class CoreIdComponent : IComponent
    {
        [PrimaryEntityIndex] public CoreEntityId value;
    }
}

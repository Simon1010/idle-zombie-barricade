using Entitas;
using IdleMineGenerator.CodeGeneration.Attributes;

namespace ZombieGame.Ecs.Core.Units.Ally
{
    [Core, ListenerAny]
    public class AllyCurrentReloadTimeComponent : IComponent
    {
        public float value;
    }
}
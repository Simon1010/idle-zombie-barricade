using Entitas;

namespace ZombieGame.Ecs.Core.Units.Ally
{
    [Core]
    public class AllyCurrentFireCooldownComponent : IComponent
    {
        public float value;
    }
}
using Entitas;
using Entitas.CodeGeneration.Attributes;
using IdleMineGenerator.CodeGeneration.Attributes;

namespace ZombieGame.Ecs.Core.Units.Ally
{
    [Core, ListenerSelf]
    public class AllyWeaponEquippedComponent : IComponent
    {
        [EntityIndex] public CoreEntityId value;
    }
}
using System.Collections.Generic;
using Entitas;

namespace ZombieGame.Ecs.Core.Units.Ally
{
    public class SetupWeaponWhenAssignedSystem : ReactiveSystem<CoreEntity>
    {
        private readonly Contexts _contexts;

        public SetupWeaponWhenAssignedSystem(Contexts contexts) : base(contexts.core)
        {
            _contexts = contexts;
        }

        protected override ICollector<CoreEntity> GetTrigger(IContext<CoreEntity> context)
        {
            return context.CreateCollector(CoreMatcher.AllyWeaponEquipped);
        }

        protected override bool Filter(CoreEntity entity)
        {
            return entity.hasAllyWeaponEquipped;
        }

        protected override void Execute(List<CoreEntity> entities)
        {
            foreach (var coreEntity in entities)
            {
                var weapon = _contexts.core.GetEntityWithCoreId(coreEntity.allyWeaponEquipped.value);
                var weaponData = GameData.GetWeaponData(weapon.weaponId.value);
                var magazineSize = GameCalculations.CalculateInt(weaponData.MagazineSize, weapon);
                weapon.AddWeaponAmmo(magazineSize);
            }
        }
    }
}
namespace ZombieGame.Ecs.Core.Units.Ally
{
    public class AllyFeature : Feature
    {
        public AllyFeature(Contexts contexts)
        {
            Add(new SetupWeaponWhenAssignedSystem(contexts));    
        }
    }
}
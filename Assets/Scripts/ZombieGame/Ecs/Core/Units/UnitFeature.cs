using ZombieGame.Ecs.Core.Units.Ai;
using ZombieGame.Ecs.Core.Units.Ally;

namespace ZombieGame.Ecs.Core.Units
{
    public class UnitFeature : Feature
    {
        public UnitFeature(Contexts contexts)
        {
            Add(new AllyFeature(contexts));
            Add(new AiFeature(contexts));
        }
    }
}
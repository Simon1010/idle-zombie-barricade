using System.Collections.Generic;
using System.Linq;
using Entitas;

namespace ZombieGame.Ecs.Core.Units.Ai
{
    public class RemoveDestroyedTargetsSystem : ReactiveSystem<CoreEntity>
    {
        private readonly Contexts _contexts;

        public RemoveDestroyedTargetsSystem(Contexts contexts) : base(contexts.core)
        {
            _contexts = contexts;
        }

        protected override ICollector<CoreEntity> GetTrigger(IContext<CoreEntity> context)
        {
            return context.CreateCollector(CoreMatcher.AllOf(CoreMatcher.Destroyed));
        }

        protected override bool Filter(CoreEntity entity)
        {
            return entity.isDestroyed;
        }

        protected override void Execute(List<CoreEntity> entities)
        {
            foreach (var coreEntity in entities)
            {
                var targeters = _contexts.core.GetEntitiesWithAttackTarget(coreEntity.coreId.value).ToArray();

                for (var index = targeters.Length - 1; index >= 0; index--)
                {
                    var targeter = targeters[index];
                    targeter.RemoveAttackTarget();
                }
            }
        }
    }
}
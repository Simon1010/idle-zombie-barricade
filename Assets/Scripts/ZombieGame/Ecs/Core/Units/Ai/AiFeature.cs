using ZombieGame.Ecs.Core.Combat.Attack;

namespace ZombieGame.Ecs.Core.Units.Ai
{
    public class AiFeature : Feature
    {
        public AiFeature(Contexts contexts)
        {
            //Targeting
            Add(new RemoveDestroyedTargetsSystem(contexts));
            Add(new RemoveDeadTargetsSystem(contexts));
            
            Add(new RemoveAttackPositionSystem(contexts));
            Add(new GruntTargetingSystem(contexts));
            Add(new AllyTargetingSystem(contexts));

            Add(new AttackFeature(contexts));

            //Behaviour
            Add(new GruntBehaviourSystem(contexts));
            Add(new AllyBehaviourSystem(contexts));
        }
    }
}
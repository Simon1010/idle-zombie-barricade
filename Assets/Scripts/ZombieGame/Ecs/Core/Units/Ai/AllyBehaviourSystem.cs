using System.Collections.Generic;
using Entitas;
using UnityEngine;

namespace ZombieGame.Ecs.Core.Units.Ai
{
    public class AllyBehaviourSystem : ReactiveSystem<CoreEntity>
    {
        private readonly Contexts _contexts;
        private IGroup<CoreEntity> _enemyGrunts;

        public AllyBehaviourSystem(Contexts contexts) : base(contexts.core)
        {
            _contexts = contexts;
            _enemyGrunts =
                contexts.core.GetGroup(CoreMatcher.AllOf(CoreMatcher.AllyBehaviour));
        }

        protected override ICollector<CoreEntity> GetTrigger(IContext<CoreEntity> context)
        {
            return context.CreateCollector(CoreMatcher.AllOf(CoreMatcher.TickComplete, CoreMatcher.GameplayTick));
        }

        protected override bool Filter(CoreEntity entity)
        {
            return entity.isGameplayTick && entity.isTickComplete;
        }

        protected override void Execute(List<CoreEntity> entities)
        {
            foreach (var coreEntity in _enemyGrunts.GetEntities())
            {
                if (coreEntity.hasUnitCurrentState == false)
                {
                    coreEntity.AddUnitCurrentState(GameEnums.UnitState.Idle);
                }

                if (_contexts.core.barricadeEntity.isDead)
                {
                    coreEntity.ReplaceUnitCurrentState(GameEnums.UnitState.Flee);
                }

                if (coreEntity.unitCurrentState.value == GameEnums.UnitState.Flee)
                {
                    if (GameAiHelpers.Move(coreEntity, _contexts.core.safehouseEntranceEntity.position.value))
                    {
                        coreEntity.isDestroyed = true;
                    }
                }

                if (coreEntity.unitCurrentState.value != GameEnums.UnitState.Flee)
                {
                    var weapon = _contexts.core.GetEntityWithCoreId(coreEntity.allyWeaponEquipped.value);
                    if (weapon.weaponAmmo.value <= 0)
                    {
                        coreEntity.ReplaceUnitCurrentState(GameEnums.UnitState.Reloading);
                    }
                }

                if (coreEntity.unitCurrentState.value == GameEnums.UnitState.Reloading)
                {
                    if (coreEntity.hasAllyCurrentReloadTime)
                    {
                        var currentReloadTime = coreEntity.allyCurrentReloadTime.value;
                        currentReloadTime--;
                        if (currentReloadTime < 0)
                        {
                            coreEntity.RemoveAllyCurrentReloadTime();

                            var weapon = _contexts.core.GetEntityWithCoreId(coreEntity.allyWeaponEquipped.value);
                            var weaponData = GameData.GetWeaponData(weapon.weaponId.value);
                            var ammo = GameCalculations.CalculateInt(weaponData.MagazineSize, weapon);
                            weapon.ReplaceWeaponAmmo(ammo);

                            coreEntity.ReplaceUnitCurrentState(GameEnums.UnitState.Idle);

                            if (coreEntity.hasAllyCurrentFireCooldown)
                            {
                                coreEntity.RemoveAllyCurrentFireCooldown();
                            }
                        }
                        else
                        {
                            coreEntity.ReplaceAllyCurrentReloadTime(currentReloadTime);
                        }
                    }
                    else
                    {
                        var weapon = _contexts.core.GetEntityWithCoreId(coreEntity.allyWeaponEquipped.value);
                        var weaponData = GameData.GetWeaponData(weapon.weaponId.value);
                        var weaponReloadTime = GameCalculations.Calculate(weaponData.ReloadTime, weapon);
                        coreEntity.AddAllyCurrentReloadTime(weaponReloadTime);
                    }
                }

                if (coreEntity.unitCurrentState.value == GameEnums.UnitState.Idle)
                {
                    if (coreEntity.hasAttackTarget)
                    {
                        coreEntity.ReplaceUnitCurrentState(GameEnums.UnitState.Attacking);
                    }
                }

                if (coreEntity.unitCurrentState.value == GameEnums.UnitState.Attacking)
                {
                    if (coreEntity.hasAttackTarget)
                    {
                        if (coreEntity.hasAllyCurrentFireCooldown)
                        {
                            var cooldown = coreEntity.allyCurrentFireCooldown.value;
                            cooldown--;
                            if (cooldown <= 0)
                            {
                                coreEntity.RemoveAllyCurrentFireCooldown();
                            }
                            else
                            {
                                coreEntity.ReplaceAllyCurrentFireCooldown(cooldown);
                            }
                        }

                        if (coreEntity.hasAllyCurrentFireCooldown == false)
                        {
                            GameAiHelpers.DoAttack(coreEntity, GameEnums.AttackId.Fire_001);
                            var weapon = _contexts.core.GetEntityWithCoreId(coreEntity.allyWeaponEquipped.value);
                            var weaponData = GameData.GetWeaponData(weapon.weaponId.value);
                            var weaponFireRate = GameCalculations.Calculate(weaponData.FireRate, weapon);
                            coreEntity.ReplaceAllyCurrentFireCooldown(weaponFireRate);
                        }
                    }
                    else
                    {
                        coreEntity.ReplaceUnitCurrentState(GameEnums.UnitState.Idle);
                    }
                }
            }
        }
    }
}
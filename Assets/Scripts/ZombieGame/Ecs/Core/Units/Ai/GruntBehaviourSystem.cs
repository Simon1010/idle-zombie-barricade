using System.Collections.Generic;
using Entitas;

namespace ZombieGame.Ecs.Core.Units.Ai
{
    public class GruntBehaviourSystem : ReactiveSystem<CoreEntity>
    {
        private readonly Contexts _contexts;
        private IGroup<CoreEntity> _enemyGrunts;

        public GruntBehaviourSystem(Contexts contexts) : base(contexts.core)
        {
            _contexts = contexts;
            _enemyGrunts =
                contexts.core.GetGroup(CoreMatcher.AllOf(CoreMatcher.GruntBehaviour));
        }

        protected override ICollector<CoreEntity> GetTrigger(IContext<CoreEntity> context)
        {
            return context.CreateCollector(CoreMatcher.AllOf(CoreMatcher.TickComplete, CoreMatcher.GameplayTick));
        }

        protected override bool Filter(CoreEntity entity)
        {
            return entity.isGameplayTick && entity.isTickComplete;
        }

        protected override void Execute(List<CoreEntity> entities)
        {
            foreach (var coreEntity in _enemyGrunts.GetEntities())
            {
                if (coreEntity.hasUnitCurrentState == false)
                {
                    coreEntity.AddUnitCurrentState(GameEnums.UnitState.Idle);
                }

                if (coreEntity.isDead)
                {
                    coreEntity.ReplaceUnitCurrentState(GameEnums.UnitState.Dead);
                }

                if (coreEntity.unitCurrentState.value == GameEnums.UnitState.Dead)
                {
                    if (GameAiHelpers.IsMoving(coreEntity))
                    {
                        GameAiHelpers.StopMoving(coreEntity);
                    }
                }

                if (coreEntity.unitCurrentState.value == GameEnums.UnitState.Idle)
                {
                    if (coreEntity.hasAttackTarget)
                    {
                        coreEntity.ReplaceUnitCurrentState(GameEnums.UnitState.Advancing);
                    }
                }


                if (coreEntity.unitCurrentState.value == GameEnums.UnitState.Advancing)
                {
                    if (coreEntity.hasAttackTarget)
                    {
                        var attackTarget = _contexts.core.GetEntityWithCoreId(coreEntity.attackTarget.value);
                        var targetPosition = attackTarget.position.value;

                        if (attackTarget.isBarricade)
                        {
                            targetPosition.X = coreEntity.position.value.X;
                        }

                        if (coreEntity.hasAttackPosition)
                        {
                            targetPosition = coreEntity.attackPosition.value;
                        }

                        if (GameAiHelpers.Move(coreEntity, targetPosition))
                        {
                            coreEntity.ReplaceUnitCurrentState(GameEnums.UnitState.Attacking);
                        }
                    }
                    else
                    {
                        coreEntity.ReplaceUnitCurrentState(GameEnums.UnitState.Idle);
                    }
                }

                if (coreEntity.unitCurrentState.value == GameEnums.UnitState.Attacking)
                {
                    if (coreEntity.hasAttackTarget)
                    {
                        var attackTarget = _contexts.core.GetEntityWithCoreId(coreEntity.attackTarget.value);
                        var targetPosition = attackTarget.position.value;

                        if (coreEntity.hasAttackPosition)
                        {
                            targetPosition = coreEntity.attackPosition.value;
                        }

                        if (attackTarget.isBarricade)
                        {
                            targetPosition.X = coreEntity.position.value.X;
                        }

                        if (GameAiHelpers.IsAt(coreEntity, targetPosition))
                        {
                            GameAiHelpers.DoAttack(coreEntity, GameEnums.AttackId.Melee_001);
                        }
                        else
                        {
                            coreEntity.ReplaceUnitCurrentState(GameEnums.UnitState.Idle);
                        }
                    }
                    else
                    {
                        coreEntity.ReplaceUnitCurrentState(GameEnums.UnitState.Idle);
                    }
                }
            }
        }
    }
}
using Entitas;
using IdleMineGenerator.CodeGeneration.Attributes;

namespace ZombieGame.Ecs.Core.Units
{
    [Core, ListenerAny]
    public class UnitIdComponent : IComponent
    {
        public GameEnums.UnitId value;
    }
}
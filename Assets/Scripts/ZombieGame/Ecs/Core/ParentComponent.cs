using Entitas;
using Entitas.CodeGeneration.Attributes;
using IdleMineGenerator.CodeGeneration.Attributes;

namespace ZombieGame.Ecs.Core
{
    [Core, ListenerAny]
    public class ParentComponent : IComponent
    {
        [EntityIndex] public CoreEntityId value;
    }
}
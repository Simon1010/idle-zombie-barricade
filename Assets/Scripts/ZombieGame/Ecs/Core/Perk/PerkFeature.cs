namespace ZombieGame.Ecs.Core.Perk
{
    public class PerkFeature : Feature
    {
        public PerkFeature(Contexts contexts)
        {
            Add(new HandlePerkModifiersSystem(contexts));
        }
    }
}
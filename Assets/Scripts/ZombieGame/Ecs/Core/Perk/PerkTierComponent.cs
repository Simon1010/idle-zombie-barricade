using Entitas;

namespace ZombieGame.Ecs.Core.Perk
{
    [Core]
    public class PerkTierComponent : IComponent
    {
        public GameEnums.PerkTier value;
    }
}
using Entitas;
using IdleMineGenerator.CodeGeneration.Attributes;

namespace ZombieGame.Ecs.Core.Perk
{
    [Core, ListenerSelf, ListenerAny]
    public class PerkOwnedComponent : IComponent
    {
    }
}
using System;
using System.Collections.Generic;
using Entitas;

namespace ZombieGame.Ecs.Core.Perk
{
    public class HandlePerkModifiersSystem : ReactiveSystem<CoreEntity>
    {
        private readonly Contexts _contexts;

        public HandlePerkModifiersSystem(Contexts contexts) : base(contexts.core)
        {
            _contexts = contexts;
        }

        protected override ICollector<CoreEntity> GetTrigger(IContext<CoreEntity> context)
        {
            return context.CreateCollector(CoreMatcher.AllOf(CoreMatcher.Perk, CoreMatcher.PerkOwned));
        }

        protected override bool Filter(CoreEntity entity)
        {
            return entity.isPerk && entity.isPerkOwned;
        }

        protected override void Execute(List<CoreEntity> entities)
        {
            foreach (var coreEntity in entities)
            {
                var perkData = GameData.GetPerkData(coreEntity.perkId.value, coreEntity.perkTier.value);

                var modifier = _contexts.core.CreateEntity();
                modifier.isModifier = true;
                modifier.AddModifierStatId(perkData.ModifierStatId);
                modifier.AddModifierTargetEntityType(perkData.ModifierTargetEntityType);
                modifier.AddModifierValue(perkData.StatModifier);
            }
        }
    }
}
using Entitas;
using Entitas.CodeGeneration.Attributes;
using IdleMineGenerator.CodeGeneration.Attributes;

namespace ZombieGame.Ecs.Core.Perk
{
    [Core, ListenerSelf]
    public class PerkIdComponent : IComponent
    {
        [EntityIndex] public GameEnums.PerkId value;
    }
}
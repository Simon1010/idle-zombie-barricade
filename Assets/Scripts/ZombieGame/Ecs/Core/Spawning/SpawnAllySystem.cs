using System.Collections.Generic;
using Entitas;

namespace ZombieGame.Ecs.Core.Spawning
{
    public class SpawnAllySystem : ReactiveSystem<CoreEntity>
    {
        private readonly Contexts _contexts;

        public SpawnAllySystem(Contexts contexts) : base(contexts.core)
        {
            _contexts = contexts;
        }

        protected override ICollector<CoreEntity> GetTrigger(IContext<CoreEntity> context)
        {
            return context.CreateCollector(CoreMatcher.AllOf(CoreMatcher.TickComplete, CoreMatcher.AllySpawnTick));
        }

        protected override bool Filter(CoreEntity entity)
        {
            return entity.isAllySpawnTick && entity.isTickComplete;
        }

        protected override void Execute(List<CoreEntity> entities)
        {
            foreach (var coreEntity in entities)
            {
            }
        }
    }
}
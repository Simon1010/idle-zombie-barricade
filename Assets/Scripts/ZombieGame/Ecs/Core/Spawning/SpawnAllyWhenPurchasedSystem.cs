using System.Collections.Generic;
using System.Linq;
using Entitas;
using Utilities;

namespace ZombieGame.Ecs.Core.Spawning
{
    public class SpawnAllyWhenPurchasedSystem : ReactiveSystem<CoreEntity>
    {
        private readonly Contexts _contexts;

        public SpawnAllyWhenPurchasedSystem(Contexts contexts) : base(contexts.core)
        {
            _contexts = contexts;
        }

        protected override ICollector<CoreEntity> GetTrigger(IContext<CoreEntity> context)
        {
            return context.CreateCollector(CoreMatcher.Level);
        }

        protected override bool Filter(CoreEntity entity)
        {
            return entity.hasLevel && entity.isWeapon;
        }

        protected override void Execute(List<CoreEntity> entities)
        {
            foreach (var coreEntity in entities)
            {
                var ally = _contexts.core.GetEntitiesWithAllyWeaponEquipped(coreEntity.coreId.value).SingleOrDefault();
                if (coreEntity.level.value > 0)
                {
                    if (ally == null)
                    {
                        var weaponId = coreEntity.weaponId.value;
                        var barricade = GameResourceManager.Instance.GetPrefab<Barricade>("Barricade");
                        ally = GameFactory.CreateAlly(_contexts,
                            _contexts.core.barricadeEntity.position.value +
                            barricade.GetAllyPositions()[(int) weaponId],
                            GameEnums.UnitId.Civilian_Grunt_001);

                        var weapon = _contexts.core.GetEntityWithWeaponId(weaponId);
                        ally.AddAllyWeaponEquipped(weapon.coreId.value);
                    }
                }
            }
        }
    }
}
using System;
using System.Collections.Generic;
using Entitas;
using Vector2 = System.Numerics.Vector2;

namespace ZombieGame.Ecs.Core.Spawning
{
    public class SpawnEnemiesSystem : ReactiveSystem<CoreEntity>
    {
        private readonly Contexts _contexts;

        public SpawnEnemiesSystem(Contexts contexts) : base(contexts.core)
        {
            _contexts = contexts;
        }

        protected override ICollector<CoreEntity> GetTrigger(IContext<CoreEntity> context)
        {
            return context.CreateCollector(CoreMatcher.AllOf(CoreMatcher.TickComplete, CoreMatcher.EnemySpawnTick));
        }

        protected override bool Filter(CoreEntity entity)
        {
            return entity.isEnemySpawnTick && entity.isTickComplete;
        }

        protected override void Execute(List<CoreEntity> entities)
        {
            foreach (var coreEntity in entities)
            {
                var random = new Random();
                var randomX = random.Next(-200, 200);
                var enemyValues = Enum.GetValues(typeof(GameEnums.UnitId));
                var randomEnemy = random.Next(1, enemyValues.Length);
                GameFactory.CreateEnemy(_contexts, new Vector2(randomX,  GameSettings.EnemySpawnY), (GameEnums.UnitId) randomEnemy);
            }
        }
    }
}
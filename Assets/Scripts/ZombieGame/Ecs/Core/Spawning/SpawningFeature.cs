namespace ZombieGame.Ecs.Core.Spawning
{
    public class SpawningFeature : Feature
    {
        public SpawningFeature(Contexts contexts)
        {
            Add(new SpawnEnemiesSystem(contexts));
            Add(new SpawnAllyWhenPurchasedSystem(contexts));
        }
    }
}
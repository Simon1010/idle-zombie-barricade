﻿using System;
using Ecs.Core;
using Entitas;
using ZombieGame.Ecs.Core;

namespace ZombieGame.Ecs
{
    public static class ContextsIdExtensions
    {
        public static void SubscribeId(this Contexts contexts)
        {
            foreach (var context in contexts.allContexts)
            {
                if (Array.FindIndex(context.contextInfo.componentTypes, v => v == typeof(CoreIdComponent)) >= 0)
                {
                    context.OnEntityCreated += AddId;
                }
            }
        }

        public static void AddId(IContext context, IEntity entity)
        {
            if (entity as CoreEntity != null)
            {
                (entity as CoreEntity).ReplaceCoreId(new CoreEntityId(entity.creationIndex));
            }
        }
    }
}
using System;
using System.Diagnostics;
using System.Linq;
using Entitas;
using ZombieGame.Data;
using Vector2 = System.Numerics.Vector2;

namespace ZombieGame.Ecs
{
    public static class GameFactory
    {
        public static void CreateCollision(Contexts contexts, CoreEntityId entityA, CoreEntityId entityB)
        {
            var collision = contexts.core.CreateEntity();
            collision.AddCollision(entityA, entityB);
        }

        public static CoreEntity CreateTick(Contexts contexts, float total, float start = 0)
        {
            var tick = contexts.core.CreateEntity();
            tick.isTick = true;
            tick.AddTickCurrentTime(start);
            tick.AddTickTotalTime(total);
            return tick;
        }

        public static CoreEntity CreateMap(Contexts contexts)
        {
            var map = contexts.core.CreateEntity();
            map.isMap = true;
            return map;
        }

        public static void CreateApplyDamage(Contexts contexts, CoreEntityId damageSource,
            Vector2 position, float range, float damageAmount)
        {
            var enemiesInRange = contexts.core
                .GetEntities(CoreMatcher.AllOf(CoreMatcher.Enemy, CoreMatcher.Position).NoneOf(CoreMatcher.Dead))
                .Where(e => Vector2.Distance(e.position.value, position) <= range);

            var damageFallOff = GameSettings.AOEDamageFallOff;

            foreach (var coreEntity in enemiesInRange)
            {
                var yDistance = Math.Abs(coreEntity.position.value.Y - position.Y);
                var distancePercent = 1 - (yDistance / range);
                var damage = distancePercent * damageAmount;

                if (distancePercent <= 0.30f)
                {
                    damage *= (damageFallOff * distancePercent);
                }

                CreateApplyDamage(contexts, damageSource, coreEntity.coreId.value, distancePercent * damage);
            }
        }

        public static CoreEntity CreateApplyDamage(Contexts contexts, CoreEntityId damageSource,
            CoreEntityId attackTarget, float damageAmount)
        {
            var attack = contexts.core.CreateEntity();
            attack.isApplyDamage = true;
            attack.AddApplyDamageTarget(attackTarget);
            attack.AddDamageSource(damageSource);
            attack.AddDamageAmount(damageAmount);
            return attack;
        }

        public static CoreEntity CreateBarricade(Contexts contexts, Vector2 position, float health)
        {
            var barricade = contexts.core.CreateEntity();
            barricade.isBarricade = true;
            barricade.AddPosition(position);
            barricade.AddCurrentHealth(health);
            barricade.AddTotalHealth(health);
            return barricade;
        }

        public static CoreEntity CreateEnemy(Contexts contexts, Vector2 position, GameEnums.UnitId unitId)
        {
            var entity = CreateUnit(contexts, position, unitId);
            entity.isEnemy = true;
            entity.isGruntBehaviour = true;
            entity.isAwardMoneyOnDeath = true;
            entity.AddLevel(1);

            var unitData = GameData.GetEnemyData(unitId);
            var health = GameCalculations.Calculate(unitData.Health, entity);
            entity.AddCurrentHealth(health);
            entity.AddTotalHealth(health);
            var speed = GameCalculations.Calculate(unitData.Speed, entity);
            entity.AddMoveSpeed(speed);

            if (unitData.DeathAOERange != null)
            {
                entity.isDealAOEDamageOnDeath = true;
            }


            entity.AddDestroyOnDeadTime(3000);
            return entity;
        }

        public static CoreEntity CreateAlly(Contexts contexts, Vector2 position, GameEnums.UnitId unitId)
        {
            var entity = CreateUnit(contexts, position, unitId);
            entity.isAlly = true;
            entity.isAllyBehaviour = true;
            entity.AddMoveSpeed(20);
            return entity;
        }

        public static CoreEntity CreateWeapon(Contexts contexts, GameEnums.WeaponId weaponId)
        {
            var entity = contexts.core.CreateEntity();
            entity.AddWeaponId(weaponId);
            entity.isWeapon = true;
            entity.AddLevel(0);

            return entity;
        }


        public static CoreEntity CreateUnit(Contexts contexts, Vector2 position, GameEnums.UnitId unitId)
        {
            var entity = contexts.core.CreateEntity();
            entity.AddPosition(position);
            entity.AddUnitId(unitId);
            entity.isUnit = true;
            return entity;
        }

        public static CoreEntity CreateBullet(Contexts contexts, Vector2 position, CoreEntityId bulletSource,
            Vector2 direction, float speed, float damage)
        {
            var entity = contexts.core.CreateEntity();
            entity.AddPosition(position);
            entity.isBullet = true;
            entity.AddBulletDamage(damage);
            entity.AddBulletSource(bulletSource);
            entity.AddVelocity(direction * speed);
            entity.isIgnoreFriction = true;

            entity.isDead = true;
            entity.AddDestroyOnDeadTime(500);

            return entity;
        }

        public static void CreatePlayer(Contexts contexts)
        {
            var entity = contexts.core.CreateEntity();
            entity.isPlayer = true;
            entity.AddPlayerMoney(0);
            entity.AddLevel(55);
        }

        public static void CreateTap(Contexts contexts, Vector2 position)
        {
            var entity = contexts.core.CreateEntity();
            entity.isTap = true;
            entity.AddPosition(position);
        }

        public static void CreateGame(Contexts contexts)
        {
            var entity = contexts.core.CreateEntity();
            entity.isGame = true;
            entity.AddGameState(GameEnums.GameState.Gameplay);
            entity.AddGameTotalKills(0);
            entity.AddGameCurrentDay(1);
            entity.AddGameTimePassed(100);
        }

        public static void CreatePerk(Contexts contexts, GameEnums.PerkId perkId)
        {
            var perkData = GameData.GetPerkTierData(perkId);

            if (perkData == null)
            {
                UnityEngine.Debug.Log("Perk Data not yet created " + perkId);
                return;
            }

            foreach (var data in perkData.PerkData)
            {
                var entity = contexts.core.CreateEntity();
                entity.isPerk = true;
                entity.AddPerkId(perkId);
                entity.AddPerkTier(data.PerkTier);
            }
        }
    }
}
using System.Collections.Generic;
using ZombieGame.Data;
using ZombieGame.Ecs.Core.Collision;

namespace ZombieGame.Ecs
{
    public static class GameExtensions
    {
        public static UnityEngine.Vector2 ToUnityVector2(this System.Numerics.Vector2 value)
        {
            return new UnityEngine.Vector2(value.X, value.Y);
        }

        public static UnityEngine.Vector3 ToUnityVector3(this System.Numerics.Vector3 value)
        {
            return new UnityEngine.Vector3(value.X, value.Y, value.Z);
        }

        public static System.Numerics.Vector2 ToVector2(this UnityEngine.Vector3 value)
        {
            return new System.Numerics.Vector2(value.x, value.y);
        }

        public static System.Numerics.Vector2 ToVector2(this UnityEngine.Vector2 value)
        {
            return new System.Numerics.Vector2(value.x, value.y);
        }

        public static System.Numerics.Vector3 ToVector3(this UnityEngine.Vector3 value)
        {
            return new System.Numerics.Vector3(value.x, value.y, value.z);
        }

        public static List<T> ShiftLeft<T>(this List<T> list, int shiftBy)
        {
            if (list.Count <= shiftBy)
            {
                return list;
            }

            var result = list.GetRange(shiftBy, list.Count - shiftBy);
            result.AddRange(list.GetRange(0, shiftBy));
            return new List<T>(result);
        }

        public static List<T> ShiftRight<T>(this List<T> list, int shiftBy)
        {
            if (list.Count <= shiftBy)
            {
                return list;
            }

            var result = list.GetRange(list.Count - shiftBy, shiftBy);
            result.AddRange(list.GetRange(0, list.Count - shiftBy));
            return result;
        }

        public static void GetCollisionEntities(this CollisionComponent component, Contexts contexts,
            int aComponentIndex,
            int bComponentIndex, out CoreEntity a, out CoreEntity b)
        {
            a = null;
            b = null;

            var entityA = contexts.core.GetEntityWithCoreId(component.entityA);
            var entityB = contexts.core.GetEntityWithCoreId(component.entityB);

            if (entityA.HasComponent(aComponentIndex) && entityB.HasComponent(bComponentIndex))
            {
                a = contexts.core.GetEntityWithCoreId(component.entityA);
                b = contexts.core.GetEntityWithCoreId(component.entityB);
            }

            if (entityB.HasComponent(aComponentIndex) && entityA.HasComponent(bComponentIndex))
            {
                b = contexts.core.GetEntityWithCoreId(component.entityA);
                a = contexts.core.GetEntityWithCoreId(component.entityB);
            }
        }


        public static bool IsLocked(this PerkData perkData, Contexts contexts)
        {
            foreach (var perkDataRequirement in perkData.Requirements)
            {
                if (perkDataRequirement.IsValid(contexts) == false)
                {
                    return true;
                }
            }

            return false;
        }

        public static string ToGameNumberFormat(this float f)
        {
            return f.ToString("N0");
        }

        public static string ToGameNumberFormat(this int f)
        {
            return f.ToString("N0");
        }
    }
}
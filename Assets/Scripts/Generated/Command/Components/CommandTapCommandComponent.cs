//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentEntityApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class CommandEntity {

    public ZombieGame.Ecs.Commands.Tap.TapCommandComponent tapCommand { get { return (ZombieGame.Ecs.Commands.Tap.TapCommandComponent)GetComponent(CommandComponentsLookup.TapCommand); } }
    public bool hasTapCommand { get { return HasComponent(CommandComponentsLookup.TapCommand); } }

    public void AddTapCommand(System.Numerics.Vector2 newPosition) {
        var index = CommandComponentsLookup.TapCommand;
        var component = (ZombieGame.Ecs.Commands.Tap.TapCommandComponent)CreateComponent(index, typeof(ZombieGame.Ecs.Commands.Tap.TapCommandComponent));
        component.position = newPosition;
        AddComponent(index, component);
    }

    public void ReplaceTapCommand(System.Numerics.Vector2 newPosition) {
        var index = CommandComponentsLookup.TapCommand;
        var component = (ZombieGame.Ecs.Commands.Tap.TapCommandComponent)CreateComponent(index, typeof(ZombieGame.Ecs.Commands.Tap.TapCommandComponent));
        component.position = newPosition;
        ReplaceComponent(index, component);
    }

    public void RemoveTapCommand() {
        RemoveComponent(CommandComponentsLookup.TapCommand);
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentMatcherApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public sealed partial class CommandMatcher {

    static Entitas.IMatcher<CommandEntity> _matcherTapCommand;

    public static Entitas.IMatcher<CommandEntity> TapCommand {
        get {
            if (_matcherTapCommand == null) {
                var matcher = (Entitas.Matcher<CommandEntity>)Entitas.Matcher<CommandEntity>.AllOf(CommandComponentsLookup.TapCommand);
                matcher.componentNames = CommandComponentsLookup.componentNames;
                _matcherTapCommand = matcher;
            }

            return _matcherTapCommand;
        }
    }
}

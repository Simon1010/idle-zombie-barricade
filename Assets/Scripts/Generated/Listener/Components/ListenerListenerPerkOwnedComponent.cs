//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentEntityApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class ListenerEntity {

    public ListenerPerkOwnedComponent listenerPerkOwned { get { return (ListenerPerkOwnedComponent)GetComponent(ListenerComponentsLookup.ListenerPerkOwned); } }
    public bool hasListenerPerkOwned { get { return HasComponent(ListenerComponentsLookup.ListenerPerkOwned); } }

    public void AddListenerPerkOwned(IListenerPerkOwned newValue) {
        var index = ListenerComponentsLookup.ListenerPerkOwned;
        var component = (ListenerPerkOwnedComponent)CreateComponent(index, typeof(ListenerPerkOwnedComponent));
        component.value = newValue;
        AddComponent(index, component);
    }

    public void ReplaceListenerPerkOwned(IListenerPerkOwned newValue) {
        var index = ListenerComponentsLookup.ListenerPerkOwned;
        var component = (ListenerPerkOwnedComponent)CreateComponent(index, typeof(ListenerPerkOwnedComponent));
        component.value = newValue;
        ReplaceComponent(index, component);
    }

    public void RemoveListenerPerkOwned() {
        RemoveComponent(ListenerComponentsLookup.ListenerPerkOwned);
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentMatcherApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public sealed partial class ListenerMatcher {

    static Entitas.IMatcher<ListenerEntity> _matcherListenerPerkOwned;

    public static Entitas.IMatcher<ListenerEntity> ListenerPerkOwned {
        get {
            if (_matcherListenerPerkOwned == null) {
                var matcher = (Entitas.Matcher<ListenerEntity>)Entitas.Matcher<ListenerEntity>.AllOf(ListenerComponentsLookup.ListenerPerkOwned);
                matcher.componentNames = ListenerComponentsLookup.componentNames;
                _matcherListenerPerkOwned = matcher;
            }

            return _matcherListenerPerkOwned;
        }
    }
}

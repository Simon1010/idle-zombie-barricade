//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentEntityApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class ListenerEntity {

    public ListenerAnyGameOverTimerComponent listenerAnyGameOverTimer { get { return (ListenerAnyGameOverTimerComponent)GetComponent(ListenerComponentsLookup.ListenerAnyGameOverTimer); } }
    public bool hasListenerAnyGameOverTimer { get { return HasComponent(ListenerComponentsLookup.ListenerAnyGameOverTimer); } }

    public void AddListenerAnyGameOverTimer(IListenerAnyGameOverTimer newValue) {
        var index = ListenerComponentsLookup.ListenerAnyGameOverTimer;
        var component = (ListenerAnyGameOverTimerComponent)CreateComponent(index, typeof(ListenerAnyGameOverTimerComponent));
        component.value = newValue;
        AddComponent(index, component);
    }

    public void ReplaceListenerAnyGameOverTimer(IListenerAnyGameOverTimer newValue) {
        var index = ListenerComponentsLookup.ListenerAnyGameOverTimer;
        var component = (ListenerAnyGameOverTimerComponent)CreateComponent(index, typeof(ListenerAnyGameOverTimerComponent));
        component.value = newValue;
        ReplaceComponent(index, component);
    }

    public void RemoveListenerAnyGameOverTimer() {
        RemoveComponent(ListenerComponentsLookup.ListenerAnyGameOverTimer);
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentMatcherApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public sealed partial class ListenerMatcher {

    static Entitas.IMatcher<ListenerEntity> _matcherListenerAnyGameOverTimer;

    public static Entitas.IMatcher<ListenerEntity> ListenerAnyGameOverTimer {
        get {
            if (_matcherListenerAnyGameOverTimer == null) {
                var matcher = (Entitas.Matcher<ListenerEntity>)Entitas.Matcher<ListenerEntity>.AllOf(ListenerComponentsLookup.ListenerAnyGameOverTimer);
                matcher.componentNames = ListenerComponentsLookup.componentNames;
                _matcherListenerAnyGameOverTimer = matcher;
            }

            return _matcherListenerAnyGameOverTimer;
        }
    }
}

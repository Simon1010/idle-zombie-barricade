//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentEntityApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class ListenerEntity {

    public ListenerAnyApplyDamageComponent listenerAnyApplyDamage { get { return (ListenerAnyApplyDamageComponent)GetComponent(ListenerComponentsLookup.ListenerAnyApplyDamage); } }
    public bool hasListenerAnyApplyDamage { get { return HasComponent(ListenerComponentsLookup.ListenerAnyApplyDamage); } }

    public void AddListenerAnyApplyDamage(IListenerAnyApplyDamage newValue) {
        var index = ListenerComponentsLookup.ListenerAnyApplyDamage;
        var component = (ListenerAnyApplyDamageComponent)CreateComponent(index, typeof(ListenerAnyApplyDamageComponent));
        component.value = newValue;
        AddComponent(index, component);
    }

    public void ReplaceListenerAnyApplyDamage(IListenerAnyApplyDamage newValue) {
        var index = ListenerComponentsLookup.ListenerAnyApplyDamage;
        var component = (ListenerAnyApplyDamageComponent)CreateComponent(index, typeof(ListenerAnyApplyDamageComponent));
        component.value = newValue;
        ReplaceComponent(index, component);
    }

    public void RemoveListenerAnyApplyDamage() {
        RemoveComponent(ListenerComponentsLookup.ListenerAnyApplyDamage);
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentMatcherApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public sealed partial class ListenerMatcher {

    static Entitas.IMatcher<ListenerEntity> _matcherListenerAnyApplyDamage;

    public static Entitas.IMatcher<ListenerEntity> ListenerAnyApplyDamage {
        get {
            if (_matcherListenerAnyApplyDamage == null) {
                var matcher = (Entitas.Matcher<ListenerEntity>)Entitas.Matcher<ListenerEntity>.AllOf(ListenerComponentsLookup.ListenerAnyApplyDamage);
                matcher.componentNames = ListenerComponentsLookup.componentNames;
                _matcherListenerAnyApplyDamage = matcher;
            }

            return _matcherListenerAnyApplyDamage;
        }
    }
}

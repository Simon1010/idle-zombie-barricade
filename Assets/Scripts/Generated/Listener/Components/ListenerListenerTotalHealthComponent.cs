//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentEntityApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class ListenerEntity {

    public ListenerTotalHealthComponent listenerTotalHealth { get { return (ListenerTotalHealthComponent)GetComponent(ListenerComponentsLookup.ListenerTotalHealth); } }
    public bool hasListenerTotalHealth { get { return HasComponent(ListenerComponentsLookup.ListenerTotalHealth); } }

    public void AddListenerTotalHealth(IListenerTotalHealth newValue) {
        var index = ListenerComponentsLookup.ListenerTotalHealth;
        var component = (ListenerTotalHealthComponent)CreateComponent(index, typeof(ListenerTotalHealthComponent));
        component.value = newValue;
        AddComponent(index, component);
    }

    public void ReplaceListenerTotalHealth(IListenerTotalHealth newValue) {
        var index = ListenerComponentsLookup.ListenerTotalHealth;
        var component = (ListenerTotalHealthComponent)CreateComponent(index, typeof(ListenerTotalHealthComponent));
        component.value = newValue;
        ReplaceComponent(index, component);
    }

    public void RemoveListenerTotalHealth() {
        RemoveComponent(ListenerComponentsLookup.ListenerTotalHealth);
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentMatcherApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public sealed partial class ListenerMatcher {

    static Entitas.IMatcher<ListenerEntity> _matcherListenerTotalHealth;

    public static Entitas.IMatcher<ListenerEntity> ListenerTotalHealth {
        get {
            if (_matcherListenerTotalHealth == null) {
                var matcher = (Entitas.Matcher<ListenerEntity>)Entitas.Matcher<ListenerEntity>.AllOf(ListenerComponentsLookup.ListenerTotalHealth);
                matcher.componentNames = ListenerComponentsLookup.componentNames;
                _matcherListenerTotalHealth = matcher;
            }

            return _matcherListenerTotalHealth;
        }
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentEntityApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class CoreEntity {

    static readonly ZombieGame.Ecs.Core.Map.MapComponent mapComponent = new ZombieGame.Ecs.Core.Map.MapComponent();

    public bool isMap {
        get { return HasComponent(CoreComponentsLookup.Map); }
        set {
            if (value != isMap) {
                var index = CoreComponentsLookup.Map;
                if (value) {
                    var componentPool = GetComponentPool(index);
                    var component = componentPool.Count > 0
                            ? componentPool.Pop()
                            : mapComponent;

                    AddComponent(index, component);
                } else {
                    RemoveComponent(index);
                }
            }
        }
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentMatcherApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public sealed partial class CoreMatcher {

    static Entitas.IMatcher<CoreEntity> _matcherMap;

    public static Entitas.IMatcher<CoreEntity> Map {
        get {
            if (_matcherMap == null) {
                var matcher = (Entitas.Matcher<CoreEntity>)Entitas.Matcher<CoreEntity>.AllOf(CoreComponentsLookup.Map);
                matcher.componentNames = CoreComponentsLookup.componentNames;
                _matcherMap = matcher;
            }

            return _matcherMap;
        }
    }
}

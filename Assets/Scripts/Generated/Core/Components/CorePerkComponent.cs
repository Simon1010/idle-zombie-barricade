//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentEntityApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class CoreEntity {

    static readonly ZombieGame.Ecs.Core.Perk.PerkComponent perkComponent = new ZombieGame.Ecs.Core.Perk.PerkComponent();

    public bool isPerk {
        get { return HasComponent(CoreComponentsLookup.Perk); }
        set {
            if (value != isPerk) {
                var index = CoreComponentsLookup.Perk;
                if (value) {
                    var componentPool = GetComponentPool(index);
                    var component = componentPool.Count > 0
                            ? componentPool.Pop()
                            : perkComponent;

                    AddComponent(index, component);
                } else {
                    RemoveComponent(index);
                }
            }
        }
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentMatcherApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public sealed partial class CoreMatcher {

    static Entitas.IMatcher<CoreEntity> _matcherPerk;

    public static Entitas.IMatcher<CoreEntity> Perk {
        get {
            if (_matcherPerk == null) {
                var matcher = (Entitas.Matcher<CoreEntity>)Entitas.Matcher<CoreEntity>.AllOf(CoreComponentsLookup.Perk);
                matcher.componentNames = CoreComponentsLookup.componentNames;
                _matcherPerk = matcher;
            }

            return _matcherPerk;
        }
    }
}

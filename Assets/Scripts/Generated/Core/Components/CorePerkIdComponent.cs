//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentEntityApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class CoreEntity {

    public ZombieGame.Ecs.Core.Perk.PerkIdComponent perkId { get { return (ZombieGame.Ecs.Core.Perk.PerkIdComponent)GetComponent(CoreComponentsLookup.PerkId); } }
    public bool hasPerkId { get { return HasComponent(CoreComponentsLookup.PerkId); } }

    public void AddPerkId(ZombieGame.GameEnums.PerkId newValue) {
        var index = CoreComponentsLookup.PerkId;
        var component = (ZombieGame.Ecs.Core.Perk.PerkIdComponent)CreateComponent(index, typeof(ZombieGame.Ecs.Core.Perk.PerkIdComponent));
        component.value = newValue;
        AddComponent(index, component);
    }

    public void ReplacePerkId(ZombieGame.GameEnums.PerkId newValue) {
        var index = CoreComponentsLookup.PerkId;
        var component = (ZombieGame.Ecs.Core.Perk.PerkIdComponent)CreateComponent(index, typeof(ZombieGame.Ecs.Core.Perk.PerkIdComponent));
        component.value = newValue;
        ReplaceComponent(index, component);
    }

    public void RemovePerkId() {
        RemoveComponent(CoreComponentsLookup.PerkId);
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentMatcherApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public sealed partial class CoreMatcher {

    static Entitas.IMatcher<CoreEntity> _matcherPerkId;

    public static Entitas.IMatcher<CoreEntity> PerkId {
        get {
            if (_matcherPerkId == null) {
                var matcher = (Entitas.Matcher<CoreEntity>)Entitas.Matcher<CoreEntity>.AllOf(CoreComponentsLookup.PerkId);
                matcher.componentNames = CoreComponentsLookup.componentNames;
                _matcherPerkId = matcher;
            }

            return _matcherPerkId;
        }
    }
}

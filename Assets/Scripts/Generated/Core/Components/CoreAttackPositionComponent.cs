//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentEntityApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class CoreEntity {

    public ZombieGame.Ecs.Core.Combat.Attack.AttackPositionComponent attackPosition { get { return (ZombieGame.Ecs.Core.Combat.Attack.AttackPositionComponent)GetComponent(CoreComponentsLookup.AttackPosition); } }
    public bool hasAttackPosition { get { return HasComponent(CoreComponentsLookup.AttackPosition); } }

    public void AddAttackPosition(System.Numerics.Vector2 newValue) {
        var index = CoreComponentsLookup.AttackPosition;
        var component = (ZombieGame.Ecs.Core.Combat.Attack.AttackPositionComponent)CreateComponent(index, typeof(ZombieGame.Ecs.Core.Combat.Attack.AttackPositionComponent));
        component.value = newValue;
        AddComponent(index, component);
    }

    public void ReplaceAttackPosition(System.Numerics.Vector2 newValue) {
        var index = CoreComponentsLookup.AttackPosition;
        var component = (ZombieGame.Ecs.Core.Combat.Attack.AttackPositionComponent)CreateComponent(index, typeof(ZombieGame.Ecs.Core.Combat.Attack.AttackPositionComponent));
        component.value = newValue;
        ReplaceComponent(index, component);
    }

    public void RemoveAttackPosition() {
        RemoveComponent(CoreComponentsLookup.AttackPosition);
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentMatcherApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public sealed partial class CoreMatcher {

    static Entitas.IMatcher<CoreEntity> _matcherAttackPosition;

    public static Entitas.IMatcher<CoreEntity> AttackPosition {
        get {
            if (_matcherAttackPosition == null) {
                var matcher = (Entitas.Matcher<CoreEntity>)Entitas.Matcher<CoreEntity>.AllOf(CoreComponentsLookup.AttackPosition);
                matcher.componentNames = CoreComponentsLookup.componentNames;
                _matcherAttackPosition = matcher;
            }

            return _matcherAttackPosition;
        }
    }
}

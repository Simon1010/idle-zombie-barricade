//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentEntityApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class CoreEntity {

    public ZombieGame.Ecs.Core.Movement.VelocityComponent velocity { get { return (ZombieGame.Ecs.Core.Movement.VelocityComponent)GetComponent(CoreComponentsLookup.Velocity); } }
    public bool hasVelocity { get { return HasComponent(CoreComponentsLookup.Velocity); } }

    public void AddVelocity(System.Numerics.Vector2 newValue) {
        var index = CoreComponentsLookup.Velocity;
        var component = (ZombieGame.Ecs.Core.Movement.VelocityComponent)CreateComponent(index, typeof(ZombieGame.Ecs.Core.Movement.VelocityComponent));
        component.value = newValue;
        AddComponent(index, component);
    }

    public void ReplaceVelocity(System.Numerics.Vector2 newValue) {
        var index = CoreComponentsLookup.Velocity;
        var component = (ZombieGame.Ecs.Core.Movement.VelocityComponent)CreateComponent(index, typeof(ZombieGame.Ecs.Core.Movement.VelocityComponent));
        component.value = newValue;
        ReplaceComponent(index, component);
    }

    public void RemoveVelocity() {
        RemoveComponent(CoreComponentsLookup.Velocity);
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentMatcherApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public sealed partial class CoreMatcher {

    static Entitas.IMatcher<CoreEntity> _matcherVelocity;

    public static Entitas.IMatcher<CoreEntity> Velocity {
        get {
            if (_matcherVelocity == null) {
                var matcher = (Entitas.Matcher<CoreEntity>)Entitas.Matcher<CoreEntity>.AllOf(CoreComponentsLookup.Velocity);
                matcher.componentNames = CoreComponentsLookup.componentNames;
                _matcherVelocity = matcher;
            }

            return _matcherVelocity;
        }
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentEntityApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class CoreEntity {

    public ZombieGame.Ecs.Core.Combat.Bullet.BulletDamageComponent bulletDamage { get { return (ZombieGame.Ecs.Core.Combat.Bullet.BulletDamageComponent)GetComponent(CoreComponentsLookup.BulletDamage); } }
    public bool hasBulletDamage { get { return HasComponent(CoreComponentsLookup.BulletDamage); } }

    public void AddBulletDamage(float newValue) {
        var index = CoreComponentsLookup.BulletDamage;
        var component = (ZombieGame.Ecs.Core.Combat.Bullet.BulletDamageComponent)CreateComponent(index, typeof(ZombieGame.Ecs.Core.Combat.Bullet.BulletDamageComponent));
        component.value = newValue;
        AddComponent(index, component);
    }

    public void ReplaceBulletDamage(float newValue) {
        var index = CoreComponentsLookup.BulletDamage;
        var component = (ZombieGame.Ecs.Core.Combat.Bullet.BulletDamageComponent)CreateComponent(index, typeof(ZombieGame.Ecs.Core.Combat.Bullet.BulletDamageComponent));
        component.value = newValue;
        ReplaceComponent(index, component);
    }

    public void RemoveBulletDamage() {
        RemoveComponent(CoreComponentsLookup.BulletDamage);
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentMatcherApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public sealed partial class CoreMatcher {

    static Entitas.IMatcher<CoreEntity> _matcherBulletDamage;

    public static Entitas.IMatcher<CoreEntity> BulletDamage {
        get {
            if (_matcherBulletDamage == null) {
                var matcher = (Entitas.Matcher<CoreEntity>)Entitas.Matcher<CoreEntity>.AllOf(CoreComponentsLookup.BulletDamage);
                matcher.componentNames = CoreComponentsLookup.componentNames;
                _matcherBulletDamage = matcher;
            }

            return _matcherBulletDamage;
        }
    }
}

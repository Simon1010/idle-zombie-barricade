//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentEntityApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class CoreEntity {

    static readonly ZombieGame.Ecs.Core.Game.Modifiers.ModifierComponent modifierComponent = new ZombieGame.Ecs.Core.Game.Modifiers.ModifierComponent();

    public bool isModifier {
        get { return HasComponent(CoreComponentsLookup.Modifier); }
        set {
            if (value != isModifier) {
                var index = CoreComponentsLookup.Modifier;
                if (value) {
                    var componentPool = GetComponentPool(index);
                    var component = componentPool.Count > 0
                            ? componentPool.Pop()
                            : modifierComponent;

                    AddComponent(index, component);
                } else {
                    RemoveComponent(index);
                }
            }
        }
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentMatcherApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public sealed partial class CoreMatcher {

    static Entitas.IMatcher<CoreEntity> _matcherModifier;

    public static Entitas.IMatcher<CoreEntity> Modifier {
        get {
            if (_matcherModifier == null) {
                var matcher = (Entitas.Matcher<CoreEntity>)Entitas.Matcher<CoreEntity>.AllOf(CoreComponentsLookup.Modifier);
                matcher.componentNames = CoreComponentsLookup.componentNames;
                _matcherModifier = matcher;
            }

            return _matcherModifier;
        }
    }
}

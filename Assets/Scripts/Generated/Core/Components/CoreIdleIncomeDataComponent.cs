//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentEntityApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class CoreEntity {

    public ZombieGame.Ecs.Core.IdleIncome.IdleIncomeDataComponent idleIncomeData { get { return (ZombieGame.Ecs.Core.IdleIncome.IdleIncomeDataComponent)GetComponent(CoreComponentsLookup.IdleIncomeData); } }
    public bool hasIdleIncomeData { get { return HasComponent(CoreComponentsLookup.IdleIncomeData); } }

    public void AddIdleIncomeData(ZombieGame.GameIdleIncomeManager.IdleStats newValue) {
        var index = CoreComponentsLookup.IdleIncomeData;
        var component = (ZombieGame.Ecs.Core.IdleIncome.IdleIncomeDataComponent)CreateComponent(index, typeof(ZombieGame.Ecs.Core.IdleIncome.IdleIncomeDataComponent));
        component.value = newValue;
        AddComponent(index, component);
    }

    public void ReplaceIdleIncomeData(ZombieGame.GameIdleIncomeManager.IdleStats newValue) {
        var index = CoreComponentsLookup.IdleIncomeData;
        var component = (ZombieGame.Ecs.Core.IdleIncome.IdleIncomeDataComponent)CreateComponent(index, typeof(ZombieGame.Ecs.Core.IdleIncome.IdleIncomeDataComponent));
        component.value = newValue;
        ReplaceComponent(index, component);
    }

    public void RemoveIdleIncomeData() {
        RemoveComponent(CoreComponentsLookup.IdleIncomeData);
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentMatcherApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public sealed partial class CoreMatcher {

    static Entitas.IMatcher<CoreEntity> _matcherIdleIncomeData;

    public static Entitas.IMatcher<CoreEntity> IdleIncomeData {
        get {
            if (_matcherIdleIncomeData == null) {
                var matcher = (Entitas.Matcher<CoreEntity>)Entitas.Matcher<CoreEntity>.AllOf(CoreComponentsLookup.IdleIncomeData);
                matcher.componentNames = CoreComponentsLookup.componentNames;
                _matcherIdleIncomeData = matcher;
            }

            return _matcherIdleIncomeData;
        }
    }
}

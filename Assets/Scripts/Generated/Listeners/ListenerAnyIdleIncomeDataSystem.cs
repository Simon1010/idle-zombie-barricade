//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by IdleMineGenerator.CodeGeneration.Plugins.ListenerGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System.Collections.Generic;
using Entitas;
    public class ListenerAnyIdleIncomeDataSystem : ReactiveSystem<CoreEntity>
    {
        private readonly Contexts contexts;

        public ListenerAnyIdleIncomeDataSystem(Contexts contexts) : base(contexts.core)
        {
            this.contexts = contexts;
        }

        protected override void Execute(List<CoreEntity> entities)
        {
            var listeners = contexts.listener.GetEntities(ListenerMatcher.ListenerAnyIdleIncomeData);
            foreach (var coreEntity in entities)
            {
                foreach (var listenerEntity in listeners)
                {
                        listenerEntity.listenerAnyIdleIncomeData.value.OnAnyIdleIncomeData(coreEntity,coreEntity.idleIncomeData.value);
                    }
                }
            }


        protected override bool Filter(CoreEntity entity)
        {
            return entity.hasIdleIncomeData;
        }

        protected override ICollector<CoreEntity> GetTrigger(IContext<CoreEntity> context)
        {
            return context.CreateCollector(CoreMatcher.IdleIncomeData);
        }
    }

    public interface IListenerAnyIdleIncomeData
    {
        void OnAnyIdleIncomeData(CoreEntity coreEntity, ZombieGame.GameIdleIncomeManager.IdleStats value);
    }

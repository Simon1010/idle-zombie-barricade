//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by IdleMineGenerator.CodeGeneration.Plugins.ListenerGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

    public class ListenersEventSystem : Feature
    {
        public ListenersEventSystem(Contexts contexts)
        {
            
Add(new ListenerAnyApplyDamageSystem(contexts));
Add(new ListenerAnyBulletSystem(contexts));
Add(new ListenerAnyCurrentHealthSystem(contexts));
Add(new ListenerAnyGameOverTimerSystem(contexts));
Add(new ListenerAnyGameCurrentDaySystem(contexts));
Add(new ListenerAnyGameOverSystem(contexts));
Add(new ListenerAnyGameOverDateSystem(contexts));
Add(new ListenerAnyGameTotalKillsSystem(contexts));
Add(new ListenerAnyIdleIncomeDataSystem(contexts));
Add(new ListenerAnyBarricadeSystem(contexts));
Add(new ListenerAnyMapSystem(contexts));
Add(new ListenerAnyPerkOwnedSystem(contexts));
Add(new ListenerAnyPlayerSystem(contexts));
Add(new ListenerAnyPlayerMoneySystem(contexts));
Add(new ListenerAnyAllySystem(contexts));
Add(new ListenerAnyAllyCurrentReloadTimeSystem(contexts));
Add(new ListenerAnyUnitIdSystem(contexts));
Add(new ListenerAnyParentSystem(contexts));
Add(new ListenerAnyDestroyedSystem(contexts));
Add(new ListenerApplyDamageSystem(contexts));
Add(new ListenerAttackCompleteSystem(contexts));
Add(new ListenerAttackIdSystem(contexts));
Add(new ListenerCurrentHealthSystem(contexts));
Add(new ListenerDeadSystem(contexts));
Add(new ListenerTotalHealthSystem(contexts));
Add(new ListenerPositionSystem(contexts));
Add(new ListenerPerkIdSystem(contexts));
Add(new ListenerPerkOwnedSystem(contexts));
Add(new ListenerPlayerMoneySystem(contexts));
Add(new ListenerUnitCurrentStateSystem(contexts));
Add(new ListenerAllyWeaponEquippedSystem(contexts));
Add(new ListenerWeaponIdSystem(contexts));
Add(new ListenerLevelSystem(contexts));
Add(new ListenerDestroyedSystem(contexts));
        }
}

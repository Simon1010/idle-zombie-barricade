//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by IdleMineGenerator.CodeGeneration.Plugins.ListenerGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System.Collections.Generic;
using Entitas;
    public class ListenerAnyMapSystem : ReactiveSystem<CoreEntity>
    {
        private readonly Contexts contexts;

        public ListenerAnyMapSystem(Contexts contexts) : base(contexts.core)
        {
            this.contexts = contexts;
        }

        protected override void Execute(List<CoreEntity> entities)
        {
            var listeners = contexts.listener.GetEntities(ListenerMatcher.ListenerAnyMap);
            foreach (var coreEntity in entities)
            {
                foreach (var listenerEntity in listeners)
                {
                        listenerEntity.listenerAnyMap.value.OnAnyMap(coreEntity,coreEntity.isMap);
                    }
                }
            }


        protected override bool Filter(CoreEntity entity)
        {
            return entity.isMap;
        }

        protected override ICollector<CoreEntity> GetTrigger(IContext<CoreEntity> context)
        {
            return context.CreateCollector(CoreMatcher.Map);
        }
    }

    public interface IListenerAnyMap
    {
        void OnAnyMap(CoreEntity coreEntity, bool isMap);
    }

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by IdleMineGenerator.CodeGeneration.Plugins.ListenerGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System.Collections.Generic;
using Entitas;
    public class ListenerAnyBulletSystem : ReactiveSystem<CoreEntity>
    {
        private readonly Contexts contexts;

        public ListenerAnyBulletSystem(Contexts contexts) : base(contexts.core)
        {
            this.contexts = contexts;
        }

        protected override void Execute(List<CoreEntity> entities)
        {
            var listeners = contexts.listener.GetEntities(ListenerMatcher.ListenerAnyBullet);
            foreach (var coreEntity in entities)
            {
                foreach (var listenerEntity in listeners)
                {
                        listenerEntity.listenerAnyBullet.value.OnAnyBullet(coreEntity,coreEntity.isBullet);
                    }
                }
            }


        protected override bool Filter(CoreEntity entity)
        {
            return entity.isBullet;
        }

        protected override ICollector<CoreEntity> GetTrigger(IContext<CoreEntity> context)
        {
            return context.CreateCollector(CoreMatcher.Bullet);
        }
    }

    public interface IListenerAnyBullet
    {
        void OnAnyBullet(CoreEntity coreEntity, bool isBullet);
    }

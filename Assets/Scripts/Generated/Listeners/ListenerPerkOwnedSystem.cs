//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by IdleMineGenerator.CodeGeneration.Plugins.ListenerGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System.Collections.Generic;
using Entitas;

    public class ListenerPerkOwnedSystem : ReactiveSystem<CoreEntity>
    {
        private readonly Contexts contexts;

        public ListenerPerkOwnedSystem(Contexts contexts) : base(contexts.core)
        {
            this.contexts = contexts;
        }

        protected override void Execute(List<CoreEntity> entities)
        {
            var listeners = contexts.listener.GetEntities(ListenerMatcher.ListenerPerkOwned);
            foreach (var coreEntity in entities)
            {
                foreach (var listenerEntity in listeners)
                {
                    if (listenerEntity.listenerCoreId.value == coreEntity.coreId.value)
                    {
                        listenerEntity.listenerPerkOwned.value.OnPerkOwned(coreEntity.isPerkOwned);
                    }
                }
            }
        }    

        protected override bool Filter(CoreEntity entity)
        {
            return entity.isPerkOwned;
        }

        protected override ICollector<CoreEntity> GetTrigger(IContext<CoreEntity> context)
        {
            return context.CreateCollector(CoreMatcher.PerkOwned);
        }

    }
    
    public interface IListenerPerkOwned
    {
        void OnPerkOwned(bool isPerkOwned);
    }
